{-# LANGUAGE CPP #-}
-- SMTSOlver.hs ---
--
-- Filename: SMTSOlver.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Mär 10 13:32:05 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mo Jun 16 22:15:08 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 184
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module SMTSolving.SMTSolver
    ( solveInterpreationSMT
    ) where

import Types.TypePolynomialInterpretation
import Types.TypeSExpression
import Types.TypeArgumentOptions

import Misc.HelperFunctions
import Exceptions.ExceptionHandler

import Control.Monad (unless, when, void)
import qualified System.Process as Cmd
import Data.Char (isNumber)
import System.IO
import qualified Data.Map.Strict as Map
import Text.ParserCombinators.Parsec
import Control.Exception (throw)


#ifdef DEBUG
import Debug.Trace (trace)
#endif

binary :: String
binary = "minismt"


-- | This function takes as input the left side of a rewrite rule as
-- a polynomial interpretation and the right side of a rewrite rule
-- as a polynomial interpretation and returns a Map which maps
-- variable names to it's integer values.
solveInterpreationSMT :: ArgumentOptions -> [SExpression] -> IO (Map.Map String Int)
solveInterpreationSMT opts sexpr = do
  -- write to tmp file
  let txt = intro ++ concatMap (\x -> "\n :extrafuns ((" ++ x ++ " Int))") vars ++
            concatMap (\x -> "\n :assumption " ++ show x) sexpr ++
            end

  -- create the temporary files
  (pName, pHandle) <- openTempFile tempDir "SMTP"
  (sName, sHandle) <- openTempFile tempDir "SMTS"

  -- write to the temporary file
  hPutStrLn pHandle txt

  -- close the files
  hClose pHandle
  hClose sHandle

  -- execute minismt and read in solution
  Cmd.system $ binary ++ " -v1 -m -simp 2 -neg " ++ pName ++ " > " ++ sName
  solStr <- readFile sName

#ifdef DEBUG
  when (getDebug opts) $ do
           putStrLn $ "--------------------------------------------------" ++
            "\n\nDEBUG OUTPUT - Linear Problem:\n"
           putStrLn txt
#endif


  -- parse solution
  let solution = case parse smtSolveResult sName solStr of
                   Left err -> throw $
                               FatalException ("minismt failed! Message:\n" ++ show err)
                   Right x -> x

#ifdef DEBUG
  when (getDebug opts) $ do
           putStrLn $ "--------------------------------------------------" ++
            "\n\nDEBUG OUTPUT:\n\n" ++ showListWithSep show solution "\n" ++ "\n"
           putStrLn "\n--------------------------------------------------\n"


#endif


  unless (getKeepFiles opts) $
         void (Cmd.system ("rm " ++ pName  ++ " "  ++ sName ))


  return $ Map.fromList solution

  where
    -- The directory to save the temporary files in.
    tempDir = getTempDir opts

    -- This function gets all variables in the SExpression list and
    -- removes all duplicates. It finally returns a list of strings.
    vars :: [String]
    vars = map fst $
           Map.toList $
           Map.fromList $ map (\a -> (a,a)) (concatMap getVars sexpr)

    intro :: String
    intro = "(benchmark none\n :logic QF_NIA"

    end :: String
    end = "\n :formula true)"


    getVars               :: SExpression -> [String]
    getVars (SLeaf v)     = if all isNumber v then [] else [v]
    getVars (SNode _ l r) =
            (case l of
               SNode{} -> getVars l
               SLeaf v -> if all isNumber v then [] else [v]) ++
            (case r of
               SNode{} -> getVars r
               SLeaf v -> if all isNumber v then [] else [v])


smtSolveResult :: Parser [(String, Int)]
smtSolveResult = try unkown
                 <|> try unsolveable
                 <|> solution

unkown :: Parser a
unkown = do
  _ <- string "unknown"
  throw $ FatalException "The smt solver returned 'unkown'."


unsolveable :: Parser a
unsolveable = do
  _ <- string "unsat"
  throw $ FatalException "The smt problem could not be solved (was unsat)."


solution :: Parser [(String, Int)]
solution = do
  _ <- string "sat" >> spaces
  _ <- manyTill anyChar (try newline) -- ignore time output
  _ <- spaces
  v <- many varMap
  _ <- eof
  return v

varMap :: Parser (String, Int)
varMap = do

  n <- name
  _ <- spaces
  _ <- char '='
  _ <- spaces
  v <- int
  _ <- spaces
  return (n, v)

name :: Parser String
name = do
  l <- letter
  r <- many (alphaNum <|> char '_' <|> char '\'')
  return $ l:r


int :: Parser Int
int = do
  ds <- (do
          nr <- char '-'
          d <- many digit
          return (nr:d)
        ) <|> many digit
  return (read ds :: Int)


--
-- SMTSOlver.hs ends here
