{-# LANGUAGE CPP #-}
-- Main.hs ---
--
-- Filename: Main.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 19:49:03 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Sa Mai 24 13:47:50 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 1333
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This is the main starting point of the application.
-- See README and Documentation/... for more information on
-- how to use the program.

-- Usage help text:
--     $ ./progName -h

-- GHCI argument passing:
--     *Main> :main -h -v filePath.trs


-- To enabl/disable DEBUG output:
-- compile/'start ghci' with -DDEBUG
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Main
    ( main )
    where

import           Types.TypeArgumentOptions
import           Types.TypeDatatype
import           Types.TypeGeneral
import           Types.TypePolynomialInterpretation
import           Types.TypeTypedTRS

import           Exceptions.ExceptionHandler
import           Misc.HelperFunctions
import           Parsing.Arguments.ArgumentOptionsParser
import           Parsing.Parser                             (parseTRSFromFile)
import           PolynomialInterpretations.Analyzer
import           PolynomialInterpretations.PolynomialSolver

#ifdef DEBUG
import Debug.Trace (trace)
#endif
import qualified Control.Exception                          as E
import           Control.Monad                              (when)
import qualified Data.Map.Strict                            as Map
import           Data.Maybe                                 (fromMaybe)
import Data.List (find)
import           Text.ParserCombinators.Parsec              (ParseError)
import System.Exit


-- | This is the starting point of the program.
main :: IO ()
main =
    E.catch
         (do                                                   -- do for IO Monad
           -- Read arguments
           maybeArgs <- parseArgOpts :: IO (Maybe ArgumentOptions)
           let args = fromMaybe (E.throw $ FatalException "args where empty") maybeArgs

#ifdef DEBUG
           -- print message if in debug mode
           putStrLn ("DEBUG: Parsed arguments:\n" ++ show args )
#endif

           -- parse file (file gets read in by parser) and convert to TypedTRS
           -- (some semantic checks are also done by the parser)
           tmp <- parseTRSFromFile args
           let trs0 = getTRS tmp

           putStrLn "Parsed Typed Term Rewrite System:\n"
           print trs0
           putStr "\n--------------------------------------------------\n"

           -- create polynomial interpretation for all rewrite rules
           let polyInterp = generatePolyInterpretations trs0
               polyMap = Map.fromList polyInterp

           when (getVerbose args || getDebug args) $ do
             putStrLn "\rPolynomial Interpretations (pX represtens the X'th parameter)\n"
             putStr $ showListWithSep id
                          (map (\a -> "\t[" ++ fst a ++ "] = " ++  show (snd a))
                               polyInterp) "\n"
             putStrLn "\n\n--------------------------------------------------\n\n"

           -- calculate costs with polynomial interpretations
           let trs1 = trs0 { getRewriteRules =
                             createPolyInterpretationsForRR trs0 (getRewriteRules trs0)
                             polyMap
                           }

           -- print the TRS
           -- print trs1

           -- p0 + p1 x > 0 where p0 = a c d + a d + b − c b − d and p1 = a c 2 − c a
           -- So instead of this, it is sufficient to demand p0 > 0 and p1 ≥ 0:

           -- solve polynomial interpretations
           result <- solvePolynomialInterpretations args trs1
           let interp = fillPolyInterp result polyInterp

           putStrLn "\nPolynomial Interpretations (pX represtens the X'th parameter)\n"
           putStr $ showListWithSep id
                      (map (\a -> "\t[" ++ fst a ++ "] = " ++  show (snd a))
                           interp) "\n"
           putStrLn "\n--------------------------------------------------\n"


           -- enter variables to TRS
           let typedTRStmp = putVarsIntoTRS trs1 result
               typedTRS = typedTRStmp { getConstructors = putVarsIntoCtrsType result $
                                        removeDatatypeFromCtrs (getConstructors typedTRStmp)}
           putStrLn "Analyzed Rules:                \n"
           -- putStrLn (showListWithSep (\x -> "\t" ++ getName x ++ " :: " ++ show (getSignature x))
           --                               (getRewriteRules typedTRS) "\n")

           print typedTRS

           -- print complexity
           putStrLn "\n--------------------------------------------------\n\nComplexity:\n"
           printComplexity typedTRS

         )
         (\e -> print (e :: ProgException) >> exitFailure)


-- | This function just unpacks a Either ParseErro SimpleTRS to the
-- appropriate SimpleTRS or an exception.
getTRS             :: Either ParseError TypedTRS -> TypedTRS
getTRS (Right trs) = trs
getTRS (Left err)  = E.throw $ ParseException $ show err


fillPolyInterp :: Map.Map String Int -> [(String, Interpretation)] ->
                  [(String, Interpretation)]
fillPolyInterp m = foldl (fillInterpretationTuple m) []


fillInterpretationTuple         :: Map.Map String Int ->  [(String, Interpretation)] ->
                                   (String, Interpretation) -> [(String, Interpretation)]
fillInterpretationTuple m acc e = acc ++ [(fst e, fillInterpretation m (snd e))]


fillInterpretation      :: Map.Map String Int -> Interpretation -> Interpretation
fillInterpretation m ip =
    case ip of
      InterpNode list v2 ->
          InterpNode (map (fillVars m) list) (map (map (fillMonom m)) v2)
      InterpLeaf mon -> InterpLeaf $ fillMonom m mon
      _ -> ip


fillVars             :: Map.Map String Int -> ([Monom], [Interpretation]) ->
                        ([Monom], [Interpretation])
fillVars m (ms, ips) = (map (fillMonom m) ms, map (fillInterpretation m) ips)


fillMonom                     :: Map.Map String Int -> Monom -> Monom
fillMonom m (Monom (a, b, c)) = case Map.lookup lookupName m of
                                  Nothing -> Monom (a, b, c)
                                  Just x -> Monom (a, b {getIVCosts = [CostInt x] }, c)
    where
      lookupName = case b of
                     InternalVariable v _ [] -> v
                     x -> showCost (head $ getIVCosts x)


putVarsIntoCtrsType   :: Map.Map String Int -> [ConstructorType] -> [ConstructorType]
putVarsIntoCtrsType m = map (\x -> x { getCConstructors =
                                       putVarsIntoCtrs m (getCConstructors x) } )

putVarsIntoCtrs   ::  Map.Map String Int -> [Constructor] -> [Constructor]
putVarsIntoCtrs m = map (putVarsIntoCtr m)

putVarsIntoCtr                     ::  Map.Map String Int -> Constructor -> Constructor
putVarsIntoCtr m (Constructor s c) = Constructor (putVarsIntoStmt s m) c

putVarsIntoTRS     :: TypedTRS -> Map.Map String Int -> TypedTRS
putVarsIntoTRS t m = t { getRewriteRules =
                             map (`putVarsIntoRR` m) (getRewriteRules t)  }

putVarsIntoRR     :: RewriteRule -> Map.Map String Int -> RewriteRule
putVarsIntoRR (RewriteRule n s r) m =
    RewriteRule n (putVarsIntoSig s m) (map (`putVarsIntoRelation` m) r)

putVarsIntoSig               :: Signature -> Map.Map String Int -> Signature
putVarsIntoSig (Signature par ret c p) m =
    Signature (map (`putVarsIntoIV` m) par) (putVarsIntoIV ret m)
              (putVarsIntoCost c m) (putVarsIntoCost p m)

putVarsIntoRelation                :: Relation -> Map.Map String Int -> Relation
putVarsIntoRelation (Relation l li r ri) m = Relation (putVarsIntoStmt l m) li (putVarsIntoStmt r m) ri

putVarsIntoIV :: InternalVariable -> Map.Map String Int -> InternalVariable
putVarsIntoIV (InternalVariable n d c) m = InternalVariable n d
                                           (map (`putVarsIntoCost` m) c)
                                           -- case Map.lookup (show c) m of
                                           --   Just x -> InternalVariable n d (CostInt x)
                                           --   Nothing -> InternalVariable n d CostEmpty


putVarsIntoCost               :: Cost -> Map.Map String Int -> Cost
putVarsIntoCost (CostVar n) m = case Map.lookup n m of
                                  Just x -> CostInt x
                                  Nothing -> CostInt 0
putVarsIntoCost x _           = x


putVarsIntoStmt                :: Statement -> Map.Map String Int -> Statement
putVarsIntoStmt (Function n c) m = Function n (map (`putVarsIntoStmt` m) c)
putVarsIntoStmt (Atom x) m       = Atom $ putVarsIntoIV x m


printComplexity     :: TypedTRS -> IO ()
printComplexity trs = do
  putStr $ "\tInfo: pX represents the X'th input parameter (starting with p0).\n" ++
           "\tWhereas T(Y,Z) represents the Y'th constructor and it's Z'th\n" ++
           "\tparameter of the datatype T.\n\n\t"

  putStrLn $ concatMap (\x -> complexityRR trs x ++ "\n\t") (getRewriteRules trs)


getParameters    :: RewriteRule -> [InternalVariable]
getParameters rr = map (\a ->  InternalVariable ('p' : show a) NIL [] )  [0..]

-- getParameters rr = map (\(a,b) -> b { getIVVariable = 'p' : show a })
--                    (zip [0..] $ (getParTypes . getSignature) rr)

complexityRR                     :: TypedTRS -> RewriteRule -> String
complexityRR trs (RewriteRule n s c) = "O(" ++ n ++  ") = " ++
                                   if null compl then "1" else compl
    where vars = getParameters (RewriteRule n s c)
          compl = complexitySig trs vars s


complexitySig :: TypedTRS -> [InternalVariable] -> Signature -> String
complexitySig trs vars (Signature par _ _ _) =
    params ++ (if null costs || null params then "" else " + ") ++ costs


        where params = showListWithSep complexityIV vs " + "
              costs = showListWithSep complexityIV (sumOfSameNames $ concatMap innerCosts par) " + "
              filterZeros = filter (\x -> case sum (getIVCosts x) of
                                   0 -> False
                                   _ -> True
                                   )

              vs = filterZeros $ map (\(x, y) -> x { getIVCosts = getIVCosts y })
                                                                  (zip vars par)

              sumOfSameNames :: [InternalVariable] -> [InternalVariable]
              sumOfSameNames = sum'
                  where
                    sum' []     = []
                    sum' (x':xs') = elem' : sum' newList
                        where
                        (elem', newList) = foldl fun (x',[]) xs'

                    fun :: (InternalVariable, [InternalVariable]) -> InternalVariable ->
                               (InternalVariable, [InternalVariable])
                    fun (acc, list) e =
                            if getIVVariable acc == getIVVariable e
                            then
                                (acc { getIVCosts = zipWith (+) (getIVCosts acc)
                                                    (getIVCosts e)
                                     }, list)
                            else (acc, list ++ [e])


              innerCosts :: InternalVariable -> [InternalVariable]
              innerCosts iv = filterZeros $
                              concatMap (\(a, b) ->
                                         map (\(c, d) ->
                                                  d { getIVVariable = " [from " ++
                                                      show (getIVDatatype iv) ++ "(" ++
                                                      tail (getIVVariable a) ++ "," ++
                                                      show c ++  ")]"
                                                    })
                                         (zip [0..] b))
                              (zip vars (subCosts $ getIVDatatype iv))

              subCosts :: Datatype -> [[InternalVariable]]
              subCosts dt = map fun $ constrs dt
                  where
                    fun :: Constructor -> [InternalVariable]
                    fun c = case getCStatement c of
                              Atom _ -> []
                              Function _ ch -> map getVariable ch   -- has to be an Atom


              constrs :: Datatype -> [Constructor]
              constrs dt = getCConstructors $ ctrtype dt
              ctrtype :: Datatype -> ConstructorType
              ctrtype dt = fromMaybe
                           (E.throw $ FatalException $ "Could not find datatype " ++
                             show dt ++".")
                           (find (\x -> dt == getCDatatype x) $ getConstructors trs)


complexityIV                             :: InternalVariable -> String
complexityIV iv = show (sum $ getIVCosts iv) ++
                  if take (length " [from ") (getIVVariable iv) == " [from "
                  then getIVVariable iv
                  else '⋅' : getIVVariable iv


removeDatatypeFromCtrs :: [ConstructorType] -> [ConstructorType]
removeDatatypeFromCtrs = map (\x -> x { getCConstructors = map wrapper (getCConstructors x) })
    where
      wrapper                                 :: Constructor -> Constructor
      wrapper (Constructor (Atom x) c)        = Constructor
                                                (Atom $ x { getIVDatatype = NIL }) c
      wrapper (Constructor (Function n ch) c) =
          Constructor (Function n (map wrapper' ch)) c

      wrapper' :: Statement -> Statement
      wrapper' (Atom x) = Atom $ x { getIVDatatype = NIL }
      wrapper' (Function n ch) = Function n (map wrapper' ch)


--
-- Main.hs ends here

