{-# LANGUAGE CPP #-}
-- PolynomialInterpretation.hs ---
--
-- Filename: Interpretation.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Di Feb 25 21:07:33 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:27:42 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 1412
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module PolynomialInterpretations.PolynomialInterpretation
    ( convertStmtToPolyInterpretationLhs
    , convertStmtToPolyInterpretationRhs
    , createPolyInterpretation
    , permuteLinear
    , permuteLinearWithoutZeros
    , negative
    , hasDatatype
    , isVarMonom
    , mergeSameMonom
    ) where

import           Types.TypeDatatype
import           Types.TypeGeneral
import           Types.TypePolynomialInterpretation
import           Types.TypeTypedTRS

import           Exceptions.ExceptionHandler

import           Control.Exception                  (throw)
import           Data.List                          (delete, find, sortBy)
import qualified Data.Map.Strict                    as Map
import           Data.Maybe                         (isJust, fromMaybe)
import           Data.Ord                           (comparing)

#ifdef DEBUG
import           Debug.Trace                        (trace)
#endif


type Permutation = Int -> [[Int]]


-- | This function takes as input a boolean variable, which states if
-- each variable child should get a variable, a statement and a
-- permutation function and returns the polynomial interpretation
-- composed of variables.
createPolyInterpretation :: TypedTRS -> Bool -> Bool -> Permutation -> Statement -> Interpretation
createPolyInterpretation trs wMonoms wCost permute stmt =
    case stmt of
      Atom x -> interp stmt (getDatatypeOfAtom x trs) 0
      _ -> interp stmt NIL 0
    where
      pre = (case stmt of
               Atom x -> getIVVariable x
               Function n _ -> n) ++ "_"

      interp :: Statement -> Datatype -> Int -> Interpretation
      interp s dt chldNr =
          case s of
            Atom x -> InterpParam chldNr dt
                      (case getIVCosts x of
                         [] -> if wCost
                               then CostVar ("ipvar_VAR_" ++ pre ++ show chldNr)
                               else CostEmpty
                         (CostVar _ : _) -> CostEmpty
                         y -> head y
                      )

            Function n [] ->
                InterpLeaf $ Monom (1, InternalVariable ("ipvar_const_" ++ n) dt [], 1)
            Function _ ch ->
                -- create Interpreation Node
                InterpNode (map (\(e, nr) ->
                                 ( [Monom (1, InternalVariable ("ipvar_" ++ pre ++ show nr)
                                            dt [], 1)
                                    | wMonoms], -- only if with Monoms enabled!
                                   concatMap (\(a, ip) ->
                                              if a == 0
                                              then []
                                              else [ip]
                                             )   $ zip e ips)
                                ) $ zip (snd exps)
                            [length (fst exps)..length (fst exps) +
                                    length (snd exps)-1]
                           )
                (map (\x -> [Monom (1, InternalVariable
                                    ("ipvar_" ++
                                     (if length (fst exps) > 1
                                      -- no number if only 1 value
                                      then pre ++ show x
                                      else take (length pre-1) pre ))
                                    NIL [], 1)])
                 [0..length (fst exps)-1])

                    where
                      ips = map (\(x, y, z) -> interp y x z )
                            (zip3 (getDatatypeChld s trs) ch [0..(length ch-1)])
                      exps = foldl breakUp ([],[]) $ permute (length ch)
                      breakUp (zeros, other) e =
                          if all (==0) e
                          then (zeros ++ [e], other)
                          else (zeros, other ++ [e])

-- | This function takes a statement a the corresponding typed TRS as
-- input and returns a list of data-types, which are the data-types of
-- the children of the root statement.
getDatatypeChld       :: Statement -> TypedTRS -> [Datatype]
getDatatypeChld stmt' trs = case stmt' of
                               Atom x -> getDatatype' (getIVVariable x)
                               Function x _ -> getDatatype' x

    where
      getDatatype'     :: String -> [Datatype]
      getDatatype' str =
          case findRRByName str trs of
            Just x -> -- (getIVDatatype $ getRetTypes sig,
                                    map getIVDatatype $ getParTypes sig
                where
                  sig = getSignature x
            Nothing ->
                case findConstructorTypeByName str trs of
                  Nothing -> throw $ FatalException $ "Could not find datatype for " ++ str ++ "!"
                  Just x -> case findConstructorInType str x of
                              Nothing -> throw $ FatalException $ "Could not find constructor " ++
                                         str ++ " in constructor type " ++ show x ++
                                                 ". Programming error :("
                              Just y -> buildCtrDatatypes (getCDatatype x) (getCStatement y)


      buildCtrDatatypes                    :: Datatype -> Statement -> [Datatype]
      buildCtrDatatypes dt (Atom x)        =
          if getIVVariable x == "X" then [dt] else [Datatype (getIVVariable x)]
      buildCtrDatatypes dt (Function _ ch) = concatMap (buildCtrDatatypes dt) ch


-- | This function takes as input a map of function names with their
-- interpretations, a permutations function and a statement returns
-- the a polynomial interpretation composed of variables.
convertStmtToPolyInterpretationLhs                   :: TypedTRS -> String -> Map.Map String Interpretation ->
                                                        Statement -> PolynomialInterpretation
convertStmtToPolyInterpretationLhs trs prefix m stmt =
    case stmt of
      Atom x -> throw $ FatalException $ "Every rewrite rule has to start with a function " ++
                "(rewrite rule name). For example: minus(x, y) -> ..."

                -- makePolyInterpretation $ createInterpretation stmt (getDatatypeOfAtom x trs)
                -- (CostVar $ "ipvar_VAR_" ++ prefix ++ getIVVariable x) True
      _ -> makePolyInterpretation $ createInterpretation stmt NIL (getCostOfRootStmt trs stmt) True
    where
      createInterpretation :: Statement -> Datatype -> [Cost] -> Bool -> Interpretation
      createInterpretation stmt' dt cst isRoot =
          case stmt' of
            Atom x -> case Map.lookup (getIVVariable x) m of
                        Nothing -> InterpLeaf $
                                   Monom (1, InternalVariable (getIVVariable x) dt cst , 1)
                        Just (InterpLeaf (Monom (1, b, 1))) ->
                            InterpLeaf $ Monom (1, b {getIVDatatype = dt}, 1)

            Function n [] -> case findRRByName n trs of
                               Just _ -> InterpLeaf $
                                         Monom (1, InternalVariable ("ipvar_" ++ n) NIL [], 1)
                               Nothing ->
                                   case findCtrByName n trs of
                                     Nothing -> throw $ FatalException $
                                                "Unknown function " ++ n ++ "."
                                     Just ctr ->
                                         case Map.lookup n m of
                                           Nothing -> throw $ FatalException $
                                                "Could not find interpretation of constructor: "
                                                ++ n ++ "."
                                           Just (InterpLeaf (Monom (1, b, 1))) ->
                                               InterpLeaf $ Monom (1, b {getIVDatatype = dt}, 1)


            Function n ch ->
                case Map.lookup n m of
                  Nothing -> throw $ FatalException $
                             "Could not find interpretation of function: " ++ n ++ "."
                  Just (InterpNode list vars) ->
                      if isRR && not isRoot
                      then throw $ FatalException  $
                               "Nested rewrite rules are not allowed on left-hand side." ++
                               " Name of nested function: " ++ n ++ "."
                      else
                          InterpNode
                          (map (\(a,b) ->
                                (a, map (\c ->
                                         case c of
                                           InterpParam nr _ _ ->
                                               createInterpretation (ch !! nr)
                                                (dtChld !! nr)
                                               (case costChld !! nr of
                                                  [] -> []
                                                  c' ->
                                                      if all (== CostEmpty) c'
                                                      then if dt == dtChld !! nr
                                                           then cst
                                                           else map (\x ->
                                                                     case x of
                                                                       CostVar v ->
                                                                           CostVar $ v ++ "_" ++ show nr
                                                                       CostInt x' -> CostInt x'
                                                                       CostEmpty ->
                                                                           throw $ FatalException
                                                                            "Empty cost occured.") cst
                                                      else c'
                                                          ) False
                                                   ) b)
                                          ) list) newVars


                          where
                            isRR = isJust (findRRByName n trs)
                            dtChld = getDatatypeChld stmt' trs
                            costChld = getCostOfChildren trs n (length ch) (length cst)
                            newVars = case findCtrByName n trs of
                                        Nothing -> vars
                                        Just x -> case getCCost x of
                                                    CostInt i -> [[Monom (1, InternalVariable
                                                                    (show i) NIL [], 1)]]
                                                    _ -> [ [ Monom (1, InternalVariable
                                                             (showCost $ cst !! ctrChldNr x)
                                                             NIL [], 1)
                                                           ]
                                                         ]
                            ctrChldNr     :: Constructor -> Int
                            ctrChldNr ctr = snd $ fromMaybe
                                            (throw $ FatalException "Programming error ctrChldNr") $
                                            find (\(x, _) -> getStmtName (getCStatement x) == name)
                                            (case findConstructorTypeByName n trs of
                                               Nothing -> throw $ FatalException $ "Cannot find " ++
                                                          "constructor " ++ name ++ "."
                                               Just x -> zip (getCConstructors x) [0..]
                                            )

                                where
                                  name = getStmtName (getCStatement ctr)

                            getStmtName                :: Statement -> String
                            getStmtName (Function n _) = n
                            getStmtName (Atom x)       = getIVVariable x


-- | Given a typed TRS and a statement, this function fetches the
-- costs of the statement.
getCostOfRootStmt                     :: TypedTRS -> Statement -> [Cost]
getCostOfRootStmt _ (Atom _)          = throw $ FatalException "LHS must start with a rewrite rule name"
getCostOfRootStmt trs (Function n _)  = case rr of
                                          Nothing -> throw $ FatalException
                                                     "LHS must start with a rewrite rule name"
                                          Just x -> (getIVCosts . getRetTypes . getSignature) x
    where
      rr = findRRByName n trs

-- | This function takes as input parameters a typed TRS, a
-- constructor or rewrite rule name, an integer for the length of the
-- function (constructor or rewrite rule) children and the number of
-- costs. It returns a list of lists of costs, where each list of
-- costs stands for one child.
getCostOfChildren              :: TypedTRS -> String -> Int -> Int -> [[Cost]]
getCostOfChildren trs n nrChld nrCost= case ctr of
                                   Nothing -> costFromRR
                                   Just x -> -- change Cost if given
                                             -- as integer
                                          foldl fun costFromRR
                                            (zip [0..] (getConstructorChildCosts $ getCStatement x))
                                       where
                                         fun               :: [[Cost]] -> (Int, [Cost]) -> [[Cost]]
                                         fun acc (_, [])   = acc
                                         fun acc (nr, cst) = take nr acc ++ [cst] ++ drop (nr + 1) acc

    where
      rr = findRRByName n trs
      ctr = findCtrByName n trs

      ctrType :: Datatype -> ConstructorType
      ctrType dt = fromMaybe (throw $ FatalException $ "Should not happen. Info: " ++ show dt)
                (find ((dt ==) . getCDatatype ) $ getConstructors trs)

      ctrCosts         :: [Cost] -> ConstructorType -> [Cost]
      ctrCosts csts ct = snd $ foldl fun (csts ++ replicate (length list) (CostInt 0), []) list
          where
            fun               :: ([Cost], [Cost]) -> Cost -> ([Cost], [Cost])
            fun (csts, acc) c = case c of
                                  CostVar _ -> (tail csts, acc ++ [head csts])
                                  _ -> (csts, acc ++ [c])

            list = map getCCost $ getCConstructors ct

      costFromRR :: [[Cost]]
      costFromRR = case rr of
                     Nothing -> replicate nrChld (replicate nrCost CostEmpty)
                     Just x -> map getIVCosts $ (getParTypes . getSignature) x

      getConstructorChildCosts          :: Statement -> [[Cost]]
      getConstructorChildCosts (Atom x) = [ if getIVVariable x == "X"
                                            then [CostEmpty]
                                            else ctrCosts (getIVCosts x) (ctrType $ getIVDatatype x)
                                          ]
      getConstructorChildCosts (Function _ ch) = concatMap getConstructorChildCosts ch


-- | This function gets the datatype of a Atom.
getDatatypeOfAtom        :: InternalVariable -> TypedTRS -> Datatype
getDatatypeOfAtom iv trs =
    case findRRByName (getIVVariable iv) trs of
      Just rr -> getIVDatatype $ getRetTypes $ getSignature rr
      Nothing -> case findConstructorTypeByName (getIVVariable iv) trs of
                   Nothing -> throw $ FatalException $ "Could not find datatype of " ++ show (getIVVariable iv) ++
                              "."
                   Just ctrt -> getCDatatype ctrt

-- | This function takes as input a map of function names with their
-- interpretations, a permutations function and a statement returns
-- the a polynomial interpretation composed of variables.
convertStmtToPolyInterpretationRhs                            :: TypedTRS -> String -> Map.Map String
                                                                 Interpretation -> Datatype -> [Cost] ->
                                                                 Statement -> PolynomialInterpretation
convertStmtToPolyInterpretationRhs trs prefix m datatype cost stmt =
    makePolyInterpretation $ createInterpretation stmt datatype cost
        where

          createInterpretation          :: Statement -> Datatype -> [Cost] -> Interpretation
          createInterpretation stmt' dt cst =
              case stmt' of
                Atom x -> case Map.lookup (getIVVariable x) m of
                            Nothing -> InterpLeaf $
                                       Monom (1, InternalVariable (getIVVariable x) dt cst , 1)
                            Just (InterpLeaf (Monom (1, b, 1))) ->
                                InterpLeaf $ Monom (1, b {getIVDatatype = dt}, 1)

                Function n [] -> case Map.lookup n m of
                                   Nothing -> throw $ FatalException $
                                              "Could not find interpretation of constructor: " ++ n ++ "."
                                   Just (InterpLeaf (Monom (1, b, 1))) ->
                                             InterpLeaf $ Monom (1, b {getIVDatatype = dt}, 1)

                Function n ch ->
                    case Map.lookup n m of
                      Nothing -> throw $ FatalException $
                                 "Could not find interpretation of function: " ++ n ++ "."
                      Just (InterpNode list vars) ->
                          InterpNode
                          (map (\(a,b) ->
                                (a, map (\c ->
                                         case c of
                                           InterpParam nr _ _ ->
                                               createInterpretation (ch !! nr)
                                                (dtChld !! nr)
                                               (case costChld !! nr of
                                                  [] -> [] -- throw $ FatalException
                                                        -- "Nil list should not happen. Error!"
                                                  c' ->
                                                      if all (== CostEmpty) c'
                                                      then if dt == dtChld !! nr
                                                           then cst
                                                           else map (\x ->
                                                                     case x of
                                                                       CostVar v ->
                                                                           CostVar $ v ++ "_" ++ show nr
                                                                       CostInt x -> CostInt x
                                                                       CostEmpty ->
                                                                           throw $ FatalException
                                                                            "Empty cost occured.") cst
                                                      else c'
                                               )
                                        ) b)
                               ) list) newVars

                          where
                            dtChld = getDatatypeChld stmt' trs
                            costChld = getCostOfChildren trs n (length ch) (length cst)
                            newVars = case findCtrByName n trs of
                                        Nothing -> vars
                                        Just x -> case getCCost x of
                                                    CostInt i -> [[Monom (1, InternalVariable
                                                                    (show i) NIL [], 1)]]
                                                    _ -> [[Monom (1, InternalVariable
                                                             (showCost $ cst !! ctrChldNr x)
                                                             NIL [], 1)
                                                           ]
                                                         ]
                            ctrChldNr     :: Constructor -> Int
                            ctrChldNr ctr = snd $ fromMaybe (undefined) $

                                            find (\(x, _) -> getStmtName (getCStatement x) == name)
                                            (case findConstructorTypeByName n trs of
                                               Nothing -> throw $ FatalException $ "Cannot find " ++
                                                          "constructor " ++ name ++ "."
                                               Just x -> zip (getCConstructors x) [0..]
                                            )

                                where
                                  name = getStmtName (getCStatement ctr)


                            getStmtName                :: Statement -> String
                            getStmtName (Function n _) = n
                            getStmtName (Atom x)       = getIVVariable x


-- | This function negates all the input Monom-lists and returns the
-- result.
negative :: [[Monom]] -> [[Monom]]
negative = map (\x -> case head x of
                        (Monom (a, b, c)) -> Monom ((-1) * a, b, c)
                                            : tail x)


-- | This function merges monoms which are equal in it's datatype. It
-- returns the resulting list.
mergeSameMonom :: [[Monom]] -> ([[[Monom]]], [[Monom]])
mergeSameMonom = mergeSameMonom' ([],[])
    where
      mergeSameMonom'                  :: ([[[Monom]]], [[Monom]]) -> [[Monom]] ->
                                          ([[[Monom]]], [[Monom]])
      mergeSameMonom' res        []    = res
      mergeSameMonom' (acc,cst) (m:ms) =
          case mergeElement m of
            Nothing -> mergeSameMonom' (acc, m:cst) ms
            Just x -> mergeSameMonom' (acc ++ [m : allM], cst) newMs


                where
                  allM :: [[Monom]]
                  allM = map snd sameMonoms

                  newMs :: [[Monom]]
                  newMs = -- trace ("x: " ++ show x ++ " same: " ++ show sameMonoms) $
                          foldl fun ms sameMonoms

                  fun             :: [[Monom]] -> (Int, [Monom]) -> [[Monom]]
                  fun acc' (nr, _) = take nr acc' ++ drop (nr + 1) acc'

                  sameMonoms :: [(Int, [Monom])]
                  sameMonoms = sortBy (flip (comparing fst)) $
                        filter (\(_,b) -> case mergeElement b of
                                            Nothing -> False
                                            Just y ->
                                                getIVVariable (sndMonom x) == getIVVariable (sndMonom y) &&
                                                getIVDatatype (sndMonom x) == getIVDatatype (sndMonom y))
                                   (zip [0..] ms)


      mergeElement :: [Monom] -> Maybe Monom
      mergeElement = find hasDatatype


      sndMonom           :: Monom -> InternalVariable
      sndMonom (Monom (_, b, _)) = b

-- | This function checks if the given Monom has a datatype set.
hasDatatype                    :: Monom -> Bool
hasDatatype (Monom (_, iv, _)) =
    case iv of
      InternalVariable _ NIL _ -> False
      _ -> True

-- | Given a monom this methods returns true if the given monom is a
-- variable, as given in the input file.
isVarMonom   :: Monom -> Bool
isVarMonom m = all (uncurry (==)) (zip (getIVVariable $ name m) "ipvar_VAR")
    where
      name (Monom (_, n', _)) = n'


-- | This function calculates creates a list with length of the input
-- value with all permutations of this list having one element to 1
-- and one element having all values to 0.
--
-- Example: permuts 2 evaluates to: [[0,0],[1,0],[0,1]]
permuteLinear    :: Int -> [[Int]]
permuteLinear nr = foldl fun [start] [0..nr-1]
    where
      start :: [Int]
      start = replicate nr 0

      fun       :: [[Int]] -> Int -> [[Int]]
      fun acc n = acc ++ [replicate n 0 ++ [1] ++ replicate (nr-n-1) 0]

-- | This function calculates creates a list with length of the input
-- value with all permutations of this list having one element to 1
-- and one element having all values to 0.
--
-- Example: permuts 2 evaluates to: [[1,0],[0,1]]
permuteLinearWithoutZeros    :: Int -> [[Int]]
permuteLinearWithoutZeros nr = foldl fun [] [0..nr-1]
    where
      fun       :: [[Int]] -> Int -> [[Int]]
      fun acc n = acc ++ [replicate n 0 ++ [1] ++ replicate (nr-n-1) 0]


-- | This function converts a interpretation into a polynomial
-- interpretation by first simplifying the interpretation to an
-- interpretation without any further reductions possible. Afterwards
-- it convert the outcome by stepping over the list of interpretations
-- created by simplifyInterpretation and summing up the
-- multiplications.
makePolyInterpretation    :: Interpretation -> PolynomialInterpretation
makePolyInterpretation (InterpLeaf x) = PolyInterp [[x]] []
makePolyInterpretation ip =  -- trace ("\tDEBUG      : " ++ show ip) $
    uncurry PolyInterp $ foldl fun ([], []) newIp
    where
      newIp = -- trace ("DEBUG newIP: " ++ show (simplifyInterpretation ip) ++ "\n") $
              simplifyInterpretation ip
      fun :: ([[Monom]], [[Monom]]) -> Interpretation -> ([[Monom]], [[Monom]])
      fun (acc1, acc2) (InterpNode list vars) =
          (acc1 ++ map (\(v, ips) -> v ++ map (\(InterpLeaf x) -> x) ips) list,
          acc2 ++ vars)


-- | This function simplifies a given interpretation to a
-- non-reducible list of Interpretations, where each element can be
-- seen as a summation (addend).
--
-- NOTE: This function is not working properly with the multiplication
-- function yet, when used any other permute function than linear.
simplifyInterpretation                :: Interpretation -> [Interpretation]
simplifyInterpretation (InterpLeaf x)        = [InterpLeaf x]
simplifyInterpretation (InterpNode list vs2) = -- trace (show (InterpNode list vs2)) $
    if all (all (\ip -> case ip of
                          InterpLeaf _ -> True
                          _ -> False
                ) . snd) list
    then [InterpNode list vs2]
    else concatMap simplifyInterpretation (res2 (head res) : tail res)
    where
      res = concatMap (\(x,y) -> map (putMonomsIntoInterp x) y) list
      -- add var to the first element in the list
      res2 (InterpNode l r) = InterpNode l (r++vs2)


-- | This function takes as input a Interpretation element split up in
-- its basics. This means one element of an Interpretation ((vars,
-- (ip:ips)):rest) v2 is the tuple (var, ip). It will then create a
-- new Interpretation out of the given values multiplying vars into
-- the ip data structure. The [[Monom]] section cannot be filled in this
-- function, due to the reason that it will be called in a map
-- function call, which would duplicate the [[Monom]] section for each
-- child.
putMonomsIntoInterp       :: [Monom] -> Interpretation  -> Interpretation
putMonomsIntoInterp vs1 ip =
    case ip of
      InterpLeaf _ -> InterpNode [(vs1, [ip])] []
      InterpNode tuples vars ->
          InterpNode (map (\(x,y) -> (createExp $ vs1 ++ x, y)) tuples)
                         (addFront vs1 vars)
      InterpParam _ _ _ -> throw $
                       FatalException $ "Parameter node occured where it shouldn't." ++
                                          " Programming error!"


-- | This function adds a list to the beggining of a list of lists.
addFront    :: [a] -> [[a]] -> [[a]]
addFront _ []     = []
addFront x (y:ys) = (x ++ y) : addFront x ys


-- | This function checks a list of Monom(iables) if some of the
-- variables can be merged together using a multiplier or a exponent.
--
-- NOTE: This function is not working properly with the multiplication
-- function yet.
createExp    :: [Monom] -> [Monom]
createExp = foldl createExp' []
    where
      createExp'       :: [Monom] -> Monom -> [Monom]
      createExp' acc (Monom (a,b,c)) =
          case find (\(Monom (_,x,_)) -> x == b) acc of
            Nothing -> acc ++ [Monom (a,b,c)]
            Just (Monom (d,e,f)) -> delete (Monom (d,e,f)) acc ++ [Monom (d*a, b, f+c)]


--
-- Interpretation.hs ends here


