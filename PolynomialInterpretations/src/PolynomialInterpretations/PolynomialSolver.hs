{-# LANGUAGE CPP #-}
-- PolynomialSolver.hs ---
--
-- Filename: PolynomialSolver.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Mär 10 13:37:14 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:31:59 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 1249
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module PolynomialInterpretations.PolynomialSolver
    ( solvePolynomialInterpretations
    ) where

-- #define EMPTY_DT
#define USE_GEQ

import           Types.TypeArgumentOptions
import           Types.TypeGeneral
import           Types.TypePolynomialInterpretation
import           Types.TypeSExpression
import           Types.TypeTypedTRS

import           Exceptions.ExceptionHandler
import           PolynomialInterpretations.PolynomialInterpretation

import           LinearProblem.LPSolve
import           SMTSolving.SMTSolver

import           Control.Exception                                  (throw)
import           Data.Char                                          (isNumber)
import           Data.List
import qualified Data.Map.Strict                                    as Map
import           Data.Maybe                                         (fromMaybe)

#ifdef DEBUG
import           Debug.Trace                                        (trace)
#endif


-- | This function takes as input parameters the argument options and
-- the typed TRS and returns a Map which connects variables with
-- integer values.
solvePolynomialInterpretations                 :: ArgumentOptions ->
                                                  TypedTRS -> IO (Map.Map String Int)
solvePolynomialInterpretations args trs =
    if getSMTSolver args
    then solveInterpreationSMT args sexpr
    else solveInterpreationLP args sexpr

        where
          rrs :: [RewriteRule]
          rrs = getRewriteRules trs

          sexpr :: [SExpression]
          sexpr = concatMap (\x -> convertRRtoSExpr args x trs) rrs


-- | This function converts a rewrite rule to it's corresponding
-- SExpressions.
convertRRtoSExpr             :: ArgumentOptions -> RewriteRule -> TypedTRS -> [SExpression]
convertRRtoSExpr args rr trs = concatMap removeNumOnly $
                               concatMap (\x -> convertRelationToSExpr x trs $
                                                getIVCosts $ getRetTypes $ getSignature rr)
                                             (getRelations rr) ++
                               (if getCostConstraints args
                               then []
                                else costSExpressions rr)

-- | This function removes all constraints, that do not contain a
-- variable and are true (e.g. 1 = 1 or 0 = 0 + 0).
removeNumOnly           :: SExpression -> [SExpression]
removeNumOnly (SLeaf _) = throw $ FatalException "Root operator node must be of SNode."
removeNumOnly (SNode op l r)
    | not (numbersOnly (SNode op l r)) = [SNode op l r]
    | otherwise                        =
        if operatorCmp op (getValue l) (getValue r)
        then []
        else  throw $ UnsolveableException $ "The following constraint cannot be solved: '" ++
              showSExpressionAsLP (SNode op l r) ++ "'."


    where
      operatorCmp op' = case op' of
                          Eq ->  (==)
                          Gt ->  (>)
                          Geq -> (>=)
                          Lt ->  (<)
                          Leq -> (<=)
                          _ ->   throw $ FatalException "Root operator node has to be comparative."

      operatorCalc op' = case op' of
                           Times -> (*)
                           Plus  -> (+)
                           Minus -> (-)
                           Exp   -> (^)
                           _ ->   throw $ FatalException "Non root operator node must not be comparative."

      -- This function gets the value by combining all possible
      -- values from the SExpressions.
      getValue                   :: SExpression -> Integer
      getValue (SLeaf x)         = read x
      getValue (SNode op' l' r') = operatorCalc op' (getValue l') (getValue r')

      -- This function checks if there are numbers only in the
      -- SExpressions.
      numbersOnly                 :: SExpression -> Bool
      numbersOnly (SLeaf x)       = all isNumber x
      numbersOnly (SNode _ l' r') = numbersOnly l' && numbersOnly r'

-- | This function takes as input parameter a rewrite rule and returns
-- a constraint yielding a value for the rewrite rule variable greater
-- than 0.
costSExpressions    :: RewriteRule -> [SExpression]
costSExpressions rr = [SNode Gt (SLeaf $ "ipvar_" ++ getName rr) (SLeaf "0")]

-- | This function takes as input parameter a relation and the typed
-- TRS and returns the corresponding constraints yielding from the
-- interpretation and the nesting of the relations/constructors.
convertRelationToSExpr                 :: Relation -> TypedTRS -> [Cost] -> [SExpression]
convertRelationToSExpr rel trs retCsts =
    (case cr of
      Nothing -> map (\(a,b) -> SNode Gt a b) (zip (fromMaybe err cl) (replicate 100 (SLeaf "0")))
      _ -> map (\(a,b) -> SNode Gt a b) (zip (fromMaybe err cl) (fromMaybe err cr)))
             ++ multSExpr ++ dtCheckerSExpr ++ lhsRhsSExpression

        where

          (multSExpr, multEmptyL, multEmptyR) = createConstraints leftMult rightMult

          err = throw $ FatalException "Nothing instead of Just occurred. This should not happen!"

          cl :: Maybe [SExpression]
          cl = if null vars
               then Just [SLeaf "0"] -- TODO might need to replicate this
               else convertVarsToSExpr vars
              where
                vars = getConstPart leftInterp ++ leftC0
#ifdef EMPTY_DT
                              ++ map (: []) multEmptyL
#endif
          cr :: Maybe [SExpression]
          cr = if null vars
               then Just [SLeaf "0"]  -- TODO might need to replicate this
               else convertVarsToSExpr vars

                   where
                     vars = getConstPart rightInterp ++ rightC0
#ifdef EMPTY_DT
                              ++ map (: []) multEmptyR
#endif

          -- This function checks if the function return types are
          -- equal of the LHS (the return type of the corresponding
          -- rewrite rule) and the HRS (the return type of the first
          -- called function) of the rewrite rule.
          --
          -- e.g. f(a, b):NAT(cost_f_2) -> d(a, b):NAT(cost_d_2) means
          -- that following constraint will be generated if data types
          -- NAT = NAT are equal: cost_f_2 = cost_d_2
          lhsRhsSExpression :: [SExpression]
          lhsRhsSExpression =
              case findRRByName (getStmtName $ getLeft rel) trs of
                Nothing -> throw $ FatalException "LHS must start with a rewrite rule name."
                Just rrl ->
                    let name = getStmtName $ getRight rel in
                    case findRRByName name trs of
                      Nothing ->
                          case findConstructorTypeByName name trs of
                            Nothing -> []
                            Just ctrType ->
                                if dtRR rrl == getCDatatype ctrType
                                then case findConstructorInType name ctrType of
                                       Nothing -> []
                                       Just ctr ->
                                           case getCCost ctr of
                                             CostInt v ->
                                                 [ SNode Eq
                                                   (getLeafCtr rrl ctrType ctr)
                                                   (SLeaf $ show v)
                                                 ]
                                             CostVar _ -> [] -- TODO: REALLY []?
                                             CostEmpty ->
                                                 throw $ FatalException $
                                                       getStmtName (getRight rel) ++
                                                 " not a rewrite rule nor a constructor."
                                else throw $
                                     FatalException $ "The return value " ++
                                               "of " ++ name ++ " does not return the " ++
                                               "datatype " ++ show (dtRR rrl) ++
                                               " as it should by " ++
                                               "the expression: " ++ show rel
                      Just rrr ->
                          if dtRR rrl == dtRR rrr -- check datatype for equality
                          then map (\(x, y) -> SNode Eq (getLeaf x) (getLeaf y))
                                   (zip (getIVCosts $ retType rrl) (getIVCosts $ retType rrr))
                          else throw $ FatalException $ "The return value " ++
                                   "of " ++ getName rrr ++ " does not return the " ++
                                   "datatype " ++ show (dtRR rrl) ++ " as it should by " ++
                                   "the expression: " ++ show rel
              where
                getLeaf cst = SLeaf $ showCost cst

                getLeafCtr              :: RewriteRule -> ConstructorType -> Constructor -> SExpression
                getLeafCtr rr ctype ctr = SLeaf $ showCost $ getIVCosts (retType rr) !! nr
                    where
                      nr :: Int
                      nr = snd $ case find (\x -> getStmtName (getCStatement $ fst x) ==
                                                  getStmtName (getCStatement ctr))
                           (zip ctrCostList [0..]) of
                                   Nothing -> throw $ FatalException $
                                              "Could not get cost of constructor! Programming error :("
                                   Just x -> x

                      ctrCostList = getCConstructors ctype


                retType rr = getRetTypes $ getSignature rr

                getStmtName                :: Statement -> String
                getStmtName (Function n _) = n
                getStmtName (Atom x)       = getIVVariable x

                dtRR rr = getIVDatatype $ retType rr


          -- This function creates all constraints for nested functions on the
          -- rhs of the rewrite rules. The reason this function is only applied
          -- to the RHS is that for each comparison the child has to be a
          -- rewrite rule, which is only possible if this is the RHS of a
          -- rewrite rule. One could call this the well-typedness checker.
          dtCheckerSExpr :: [SExpression]
          dtCheckerSExpr = dtChecker rhs True
              where
                dtChecker                      :: Statement-> Bool -> [SExpression]
                dtChecker (Atom _)        _    = []
                dtChecker (Function _ []) _    = []
                dtChecker (Function n ch) root =
                    case findRRByName n trs of
                      Nothing ->
                          case findConstructorTypeByName n trs of
                            Nothing -> throw $ FatalException $ "Should not happen. " ++
                                       "Not rewrite rule, nor constructor."
                            Just ctype ->
                                case findConstructorInType n ctype of
                                  Nothing -> throw $ FatalException "No es possible!"
                                  Just ctr ->
                                      constraints ch (getConstructorChildCosts (if root
                                                                                then retCsts
                                                                                else []) ctype
                                                      (getCStatement ctr)) ++
                                        concatMap (`dtChecker` False) ch
                      Just r -> -- check if child is RR, if so create constraint
                               constraints ch (map getIVCosts $
                                               (getParTypes . getSignature) r)
                               ++ concatMap (`dtChecker` False) ch

                rhs = getRight rel


                getConstructorChildCosts                 :: [Cost] -> ConstructorType ->
                                                            Statement -> [[Cost]]
                getConstructorChildCosts def ctype  (Atom x) =
                    [ if getIVVariable x == "X"
                      then def
                      -- then []   -- would be a constraint testing for itself
                      --           -- (e.g. c0 = c0)
                      else ctrCosts (getIVCosts x) ctype
                    ]
                getConstructorChildCosts def ctype (Function _ ch) =
                    concatMap (getConstructorChildCosts def ctype) ch


                constraints           :: [Statement] -> [[Cost]] -> [SExpression]
                constraints chds csts = concatMap (\(nr, x) ->
                                      case findRRByName (getNameOfStmt x) trs of
                                        Nothing ->
                                            case findCtrByName (getNameOfStmt x) trs of
                                              Nothing -> []
                                              Just ctr ->
                                                   -- check children for costs,
                                                   -- however filter out all non
                                                   -- recursive calls (costs are
                                                   -- different there!)
                                                   concatMap
                                                   (\(c, s, Atom x') ->
                                                        if getIVVariable x' == "X"
                                                        then constraints (getChldr c) [s]
                                                        else []
                                                   )
                                                   (zip3 chds csts (getChldr $ getCStatement ctr))

                                        Just crr ->
                                            map (\(a,b) ->
                                                 SNode Eq (costToLeaf a) (costToLeaf b))
                                                    (zip (if length csts <= nr
                                                          then []
                                                          else csts !! nr)
                                                     ((getIVCosts . getRetTypes . getSignature) crr))
                                                  ) (zip [0..] chds)

                getChldr                :: Statement -> [Statement]
                getChldr (Atom _)       = []
                getChldr (Function _ x) = x


                -- This function takes as input parameter a list of costs and
                -- a constructor type and returns the list of costs given in the
                -- constructor type, where variable costs are replaces by the
                -- costs given as first parameter (in the same order as in the
                -- constructor list, given in the constructor type).
                ctrCosts         :: [Cost] -> ConstructorType -> [Cost]
                ctrCosts csts ct = snd $ foldl fun (csts ++ replicate (length list) (CostInt 0), []) list
                    where
                      fun               :: ([Cost], [Cost]) -> Cost -> ([Cost], [Cost])
                      fun (csts', acc) c = case c of
                                            CostVar _ -> (tail csts', acc ++ [head csts'])
                                            _ -> (csts', acc ++ [c])

                      list = map getCCost $ getCConstructors ct

                -- This function converts a cost to its
                -- corresponding SExpression.
                costToLeaf             :: Cost -> SExpression
                costToLeaf CostEmpty   = throw $ FatalException "CostEmpty occurred where it should."
                costToLeaf (CostInt x) = SLeaf (show x)
                costToLeaf (CostVar x) = SLeaf x


                -- This function takes as input parameter a
                -- statement and returns the name of the statement.
                -- This means in case of a function, the function
                -- name, otherwise if it is a variable, the variable
                -- name.
                getNameOfStmt                :: Statement -> String
                getNameOfStmt (Atom x)       = getIVVariable x
                getNameOfStmt (Function n _) = n

          -- This function takes as input parameter two lists of
          -- monom lists of lists. The most inner monoms represent
          -- monoms connected by multiplication, the second inner list
          -- (a list of multiplied monoms) are represent multiplied
          -- monoms connected by addition. Finally the most outer list
          -- just holds several of those addition lists.
          --
          -- e.g.: [[[a, b],[c, d]]] represents: a*b + c*d
          --
          -- The function returns a triple containing the constraints,
          -- the monoms of the left side not having a correspondent
          -- (same datatype and variable name) monom from the right
          -- side. And finally the right monoms not having a
          -- correspondent monom of the left side.
          createConstraints            :: [[[Monom]]] -> [[[Monom]]] -> ([SExpression], [Monom], [Monom])
          createConstraints left right = (\(a,b,c) -> (a, b, concat (concat c))) $
                                         foldl fun ([],[],right) left
              where

                fun                :: ([SExpression], [Monom], [[[Monom]]]) -> [[Monom]] ->
                                      ([SExpression], [Monom], [[[Monom]]])
                fun (acc, e, r) [] = (acc, e, r)
                fun (acc, e, r) l  =
                    if null (head l)
                    then (acc, e, r) -- this should not happen (just
                                     -- in case it does happen)
                    else case getAndRemove (head $ head l) r of
                           ([], _) -> (acc, e ++ [head $ head l], r)
                           (fe, nr) -> (acc ++ makeSExpression l fe, e, nr)

                -- This function gets a double-monom list from a
                -- triple-list of monoms and returns the double-monom
                -- list and the rest of the triple monom list.
                getAndRemove :: Monom -> [[[Monom]]] -> ([[Monom]], [[[Monom]]])
                getAndRemove = getAndRemove' []
                    where
                      getAndRemove'             :: [[[Monom]]] -> Monom ->
                                                   [[[Monom]]] -> ([[Monom]], [[[Monom]]])
                      getAndRemove' acc _ []    = ([], acc)
                      getAndRemove' acc e (s:ss)
                          | null (head s)       = getAndRemove' (acc ++ [s]) e ss
                          | e `sameMonom` elem' = (s, acc ++ ss)
                          | otherwise           = getAndRemove' (acc ++ [s]) e ss
                          where
                            elem' = head $ head s


                -- This function creates SExpressions, given the
                -- left and the right side as a double-list of monoms.
                makeSExpression            :: [[Monom]] -> [[Monom]] -> [SExpression]
                makeSExpression left right =
                    case convertVarsToSExpr left of
                      Nothing -> []
                      Just ll ->
                          case convertVarsToSExpr right of
                            Nothing -> []
                            Just lr ->
                                concatMap (\(l,r) ->
                                     case l of
                                       SLeaf x ->
                                           case r of
                                             SLeaf y ->
                                                 if x == y
                                                 then []
#ifdef USE_GEQ
                                                 else [SNode Geq l r]
                                             _ -> [SNode Geq l r]
                                       _ -> [SNode Geq l r]
#else
                                                 else [SNode Eq l r]
                                             _ -> [SNode Eq l r]
                                       _ -> [SNode Eq l r]
#endif
                                    ) (zip ll lr)

                -- This function checks if two monoms are equal.
                sameMonom                                     :: Monom -> Monom -> Bool
                sameMonom (Monom (a, b, c)) (Monom (d, e, f)) =
                    a == d && c == f &&
                    getIVVariable b == getIVVariable e && getIVDatatype b == getIVDatatype e


          (leftMult, leftC0) = mergeSameMonom (getMultPart leftInterp)
          (rightMult, rightC0) = mergeSameMonom (getMultPart rightInterp)

          leftInterp :: PolynomialInterpretation
          leftInterp = fromMaybe (throw $ FatalException
                                  "Polynomial Interpretation was Nothing!")
                       (getLeftInterp rel)

          rightInterp :: PolynomialInterpretation
          rightInterp = fromMaybe (throw $ FatalException
                                  "Polynomial Interpretation was Nothing!")
                       (getRightInterp rel)


convertVarsToSExpr      :: [[Monom]] -> Maybe [SExpression]
convertVarsToSExpr vars =
    Just (if null vars || null (head vars)
          then [SLeaf "0"]
          else foldl fun (convertVarListToSExpr $ head vars) (tail vars))
    where
      fun          :: [SExpression] -> [Monom] -> [SExpression]
      fun sexp [] = sexp
      fun sexp var = map (\(x,y) -> SNode Plus x y)
                     (zip sexp (convertVarListToSExpr var))


convertVarListToSExpr      :: [Monom] -> [SExpression]
convertVarListToSExpr vars =

    if null vars then throw $ FatalException "Empty var list"
    else foldl fun (convertVarToSExpr $ head vars) (tail vars)
    where
      fun          :: [SExpression] -> Monom -> [SExpression]
      fun sexp var = map (\(y,x) -> SNode Times y x)
                     (zip sexp (convertVarToSExpr var))


convertVarToSExpr                    :: Monom -> [SExpression]
convertVarToSExpr (Monom (-1, n, 1)) =
    throw $ FatalException "Negative values are not supported."
    -- case n of
    --   InternalVariable c _ [] -> [SNode Minus (SLeaf "0") (SLeaf c)]
    --   InternalVariable _ _ cost -> map (\x -> SNode Minus (SLeaf "0") $
    --                                           SLeaf $ showCost x)  cost

convertVarToSExpr (Monom (1, n, 1)) =
    -- trace ("inVarToSExpr: " ++ show n) $
    -- trace ("out: " ++ show (case n of
    -- InternalVariable c _ [] -> [SLeaf c]
    -- InternalVariable _ _ cost -> map (\x -> SLeaf $ showCost x) cost)) $

    case n of
      InternalVariable c _ [] -> [SLeaf c]
      InternalVariable _ _ cost -> map (\x -> SLeaf $ showCost x) cost

convertVarToSExpr (Monom (1, n, e)) =
    throw $ FatalException "Exponential values are not supported"

    -- case n of
    --   InternalVariable c _ [] -> SNode Times (SLeaf $ c)
    --   InternalVariable _ _ cost -> SNode Times (SLeaf $ showCost (head cost)
    --                                            )
    --                                 (convertVarToSExpr (Monom (1, n, e-1)))

convertVarToSExpr (Monom (t, n, e)) =
    throw $ FatalException "Exponential values are not supported"
    -- SNode Times (SLeaf $ show t) (convertVarToSExpr (Monom (1, n, e)))


--
-- PolynomialSolver.hs ends here
