{-# LANGUAGE DeriveDataTypeable #-}
-- TypeError.hs ---
--
-- Filename: TypeError.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Di Dez  3 00:42:09 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:26:21 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 116
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module defines the exception types for the program.
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Exceptions.ExceptionHandler
    ( ProgException(..)
    ) where


import Control.Exception (Exception)
import Data.Typeable


-----------------------------------------------------------------------------
-- SYNONYMS
type ErrorTxt = String


-----------------------------------------------------------------------------
-- | Definition of the Exception Types used in this program
data ProgException = ShowTextOnly ErrorTxt
                       | WarningException ErrorTxt
                       | FatalException ErrorTxt
                       | ParseException ErrorTxt
                       | UnsolveableException ErrorTxt
                       | SemanticException ErrorTxt
                   deriving (Typeable)

-- | Make the data ProgException an instance of Exception
instance Exception ProgException


-- | Overwrites show for better error texts
instance Show ProgException where
    show (ShowTextOnly txt)         = txt
    show (SemanticException txt)    = prefixSemantic ++ txt
    show (WarningException txt)     = prefixWarning ++  txt
    show (FatalException txt)       = prefixFatal ++ txt
    show (ParseException txt)       = prefixParse ++ txt
    show (UnsolveableException txt) = prefixFatal ++ txt


-----------------------------------------------------------------------------
-- HELPER FUNCTIONS


prefixWarning = "Warning: "
prefixFatal =  "FATAL ERROR: "
prefixParse =  "Parse Error: "
prefixSemantic = "Semantic Error: "
--
-- TypeError.hs ends here
