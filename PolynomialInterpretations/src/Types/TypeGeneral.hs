{-# LANGUAGE CPP #-}
-- TypeGeneral.hs ---
--
-- Filename: TypeGeneral.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 17:41:38 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Sa Mai 24 11:06:50 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 261
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Types.TypeGeneral where

import           Control.Exception           (throw)
import           Exceptions.ExceptionHandler
import           Misc.HelperFunctions
import           Types.TypeDatatype


-----------------------------------------------------------------------------
-- | This data type stores the costs of a statement
data Cost = CostInt Int
          | CostVar String
          | CostEmpty
            deriving (Eq)

instance Show Cost where
    show (CostInt x) = show x
    show (CostVar x) = x
    show CostEmpty   = []

instance Num Cost where

    fromInteger = CostInt . fromInteger
    (+) a b = CostInt (nrOfCost a + nrOfCost b)
    (*) a b = CostInt (nrOfCost a * nrOfCost b)
    abs = fail "abs not defined"
    signum = fail "not defined"

nrOfCost :: Cost -> Int
nrOfCost (CostInt x) = x
nrOfCost (CostVar _) = throw $ FatalException "Nr of Variable in CostVar."
nrOfCost (CostEmpty) = 0


class ShowCost a where
    showCost :: a -> String

instance ShowCost Cost where
    showCost (CostInt x) = show x
    showCost (CostVar v) = v
    -- showCost CostEmpty   = throw $ FatalException
    --                        "COST WAS EMPTY. Programming error. Sry :( [Cost instance]"


-----------------------------------------------------------------------------
-- | This type represents internal variables, used for the constraints.
--   The Show instance is interesting here. It prints nothing, if getIVariable
--   is empty. It prints the variable name if getINumber equals (-1) and it
--   prints getINumber if there is a number set and the name is not empty.
--   getIVariable holds the variable name.
--   getINumber holds the number the variable is set to.
--
--   Eq is overwritten. It does not check the costs, but all other members for
--   equality.
data InternalVariable = InternalVariable
    { getIVVariable :: String
    , getIVDatatype :: Datatype
    , getIVCosts    :: [Cost]
    }

instance Show InternalVariable where
#ifdef DEBUG
    -- show (InternalVariable a d c) = a ++ " : " ++ show d ++ "(" ++ showListWithSep show c ", " ++ ")"
#endif
    show (InternalVariable "" d []) = show d
    show (InternalVariable v _ [])  = v     -- only used for displaying
                                            -- parsed rules, there is
                                            -- (show d == v)
    show (InternalVariable _ NIL n) = showListWithSep show n " + "
    show (InternalVariable _ d n)   = show d ++ " (" ++ showListWithSep show n ", " ++ ")"


instance Eq InternalVariable where
    (==) (InternalVariable s1 NIL _) (InternalVariable s2 _  _)
        = s1 == s2
    (==) (InternalVariable s1 _ _) (InternalVariable s2 NIL _)
        = s1 == s2
    (==) (InternalVariable s1 d1 _) (InternalVariable s2 d2 _)
        = d1 == d2 && s1 == s2

-----------------------------------------------------------------------------

--
-- TypeGeneral.hs ends here

