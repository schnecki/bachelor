-- TypeSExpression.hs ---
--
-- Filename: TypeSExpression.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Di Mär 11 17:42:47 2014 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Do Mai  1 18:56:03 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 27
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Types.TypeSExpression where


data Operator = Eq | Gt | Lt | Geq | Leq | Times | Plus | Minus | Exp

instance Show Operator where
    show Eq    = "="
    show Gt    = ">"
    show Geq   = ">="
    show Lt    = "<"
    show Leq   = "<="
    show Times = "*"
    show Plus  = "+"
    show Minus = "-"
    show Exp   = "^"


data SExpression = SNode Operator SExpression SExpression
                 | SLeaf String


instance Show SExpression where
    show (SNode o s1 s2) = "(" ++ show o ++ " " ++ show s1 ++ " " ++ show s2 ++ ")"
    show (SLeaf str)     = str


showSExpressionAsLP                 :: SExpression -> String
showSExpressionAsLP (SLeaf x)       = x
showSExpressionAsLP (SNode o s1 s2) = showSExpressionAsLP s1 ++ " " ++
                                      case o of
                                        Minus -> show Plus ++ " " ++ showSExpressionAsLP' s2
                                        Gt -> show Geq ++ " " ++ showSExpressionAsLP s2 ++ " + 1"
                                        _ -> show o ++ " " ++ showSExpressionAsLP s2
    where
      showSExpressionAsLP' (SLeaf x)       = '-' : x
      showSExpressionAsLP' (SNode o s1 s2) = showSExpressionAsLP' s1 ++ " " ++ show o ++ " " ++
                                             case o of
                                               Minus -> showSExpressionAsLP s2
                                               Gt -> show Geq ++ " " ++ showSExpressionAsLP' s2 ++ " + 1"
                                               _ -> showSExpressionAsLP' s2

--
-- TypeSExpression.hs ends here
