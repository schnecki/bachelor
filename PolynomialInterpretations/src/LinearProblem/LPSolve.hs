{-# LANGUAGE CPP #-}
-- lpSolve.hs ---
--
-- Filename: lpSolve.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Sa Dez 28 18:18:34 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:29:05 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 365
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module is designed to solve a list of constraints using
-- the external linera program solver named LP Solve.
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module LinearProblem.LPSolve
    ( solveInterpreationLP
    ) where

-- min:;\n" ++ showListWithSep show constraints ";\n"

import Types.TypeSExpression
import Types.TypeArgumentOptions

import Misc.HelperFunctions
import Exceptions.ExceptionHandler

import Control.Monad (unless, when, void)
import Data.Char (isNumber)
import Text.ParserCombinators.Parsec
import Control.Exception (throw)
import qualified System.Process as Cmd
import System.IO
import qualified Data.Map.Strict as Map


binary :: String
binary = "lp_solve"

solveInterpreationLP :: ArgumentOptions -> [SExpression] -> IO (Map.Map String Int)
solveInterpreationLP opts sexpr = do

  let txt = intro ++ showListWithSep id vars " + " ++ ";\n" ++ -- function to minimize
            -- concatMap (\x -> "\n :assumption (>= " ++ x ++ " 0)" ) vars ++
            concatMap (\x -> showSExpressionAsLP x ++ ";\n") sexpr
            -- concatMap (\x -> "\n :assumption " ++ show x) sexpr
            ++ "\nint " ++ showListWithSep id vars ", " ++ ";"


  (pName, pHandle) <- openTempFile tempDir "lpP"
  (sName, sHandle) <- openTempFile tempDir "lpS"

  hPutStrLn pHandle txt

  hClose pHandle
  hClose sHandle

  -- execute lp_solve
  Cmd.system $ binary ++ " " ++ pName ++ " > " ++ sName

  -- read and parse from tmp file
  solStr <- readFile sName


#ifdef DEBUG
  when (getDebug opts) $ do
           putStrLn $ "--------------------------------------------------" ++
            "\n\nDEBUG OUTPUT - Linear Problem:\n"
           putStrLn txt
#endif


  let solution = case parse lpSolveResult sName solStr of
                   Left err -> throw $
                               FatalException ("lp_solve failed! Message:\n" ++ show err)
                   Right x -> x


#ifdef DEBUG
  when (getDebug opts) $ do
           putStrLn $ "--------------------------------------------------" ++
            "\n\nDEBUG OUTPUT:\n\n" ++ showListWithSep show solution "\n" ++ "\n"
           putStrLn "\n--------------------------------------------------\n"


#endif


  unless (getKeepFiles opts) $
       void (Cmd.system ("rm " ++ pName ++ " " ++ sName))


  return $ Map.fromList solution

    where
      -- The directory to save the temporary files in.
      tempDir = getTempDir opts

      -- This function gets all variables in the SExpression list and
      -- removes all duplicates. It finally returns a list of strings.
      vars :: [String]
      vars = map fst $
             Map.toList $
             Map.fromList $ map (\a -> (a,a)) (concatMap getVars sexpr)

      intro :: String
      intro = "min: "

      getVars               :: SExpression -> [String]
      getVars (SLeaf v)     = if all isNumber v then [] else [v]
      getVars (SNode _ l r) =
          (case l of
             SNode{} -> getVars l
             SLeaf v -> if all isNumber v then [] else [v]) ++
          (case r of
             SNode{} -> getVars r
             SLeaf v -> if all isNumber v then [] else [v])

      insertSolution :: Map.Map String Int -> [(String, Int)] -> Map.Map String Int
      insertSolution m []            = m
      insertSolution m ((k, v):rest) = insertSolution (Map.insert k v m) rest


lpSolveResult :: Parser [(String, Int)]
lpSolveResult = unsolveable
                <|> variables
                <|> do
                  _ <- skipLine
                  lpSolveResult


skipLine :: Parser ()
skipLine = do
    _ <- newline
    return ()
  <|> do
    _<- manyTill anyChar newline
    return ()

unsolveable :: Parser a
unsolveable = do
  _ <- string "This problem is infeasible"
  throw $ FatalException "The linear problem is infeasible."


variables :: Parser [(String, Int)]
variables = do
  _ <- string "Actual values of the variables:"
  many (spaces >> varMap)


varMap :: Parser (String, Int)
varMap = do

  n <- name
  _ <- spaces
  v <- int
  _ <- spaces
  return (n, v)


name :: Parser String
name = do
    l <- letter
    r <- many (alphaNum <|> oneOf "\'#@_")
    return (l:r)


-- name :: Parser String
-- name = do
--   l <- letter
--   r <- many (alphaNum <|> char '_')
--   return $ l:r


int :: Parser Int
int = do
  ds <- many digit
  return (read ds :: Int)

--
-- lpSolve.hs ends here
