\beamer@endinputifotherversion {3.26pt}
\beamer@sectionintoc {1}{Basics}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Resource Analysis - Problem}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Amortised Resource Analysis - Solution}{4}{0}{1}
\beamer@sectionintoc {2}{\IeC {\textquotedblleft }Carbon Credits\IeC {\textquotedblright } for Resource-Bounded Computations Using Amortised Analysis}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Paper Content}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Results}{6}{0}{2}
\beamer@sectionintoc {3}{Georg Moser's Notes on Amortised Cost Analysis}{7}{0}{3}
\beamer@subsectionintoc {3}{1}{The Idea}{7}{0}{3}
\beamer@sectionintoc {4}{Bachelorthesis: Amortised Resource Analysis and Carbon Credits}{8}{0}{4}
\beamer@subsectionintoc {4}{1}{Requierement 1}{8}{0}{4}
\beamer@subsectionintoc {4}{2}{Requierement 2}{9}{0}{4}
\beamer@subsectionintoc {4}{3}{Requierement 3}{10}{0}{4}
\beamer@sectionintoc {5}{Questions - Remarks}{11}{0}{5}
