(TeX-add-style-hook "_region_"
 (lambda ()
    (TeX-add-symbols
     "thetitle"
     "theauthor"
     "theinstitute"
     "sectiontitle")
    (TeX-run-style-hooks
     "inputenc"
     "utf8"
     "latex2e"
     "beamer10"
     "beamer"
     "t")))

