(TeX-add-style-hook "main"
 (lambda ()
    (LaTeX-add-bibliographies
     "bib/db")
    (TeX-add-symbols
     '("exampleBox" 1)
     "thetitle"
     "theauthor"
     "supervisor"
     "theinstitute"
     "showtoc")
    (TeX-run-style-hooks
     "framed"
     "inputenc"
     "utf8"
     "tikz"
     "xcolor"
     ""
     "latex2e"
     "beamer10"
     "beamer"
     "t"
     "presentation_setup"
     "basics"
     "carbon_credits"
     "notes_by_georg"
     "my_work"
     "questions"
     "backup_slides")))

