(TeX-add-style-hook "final"
 (lambda ()
    (LaTeX-add-bibliographies
     "biblio.bib")
    (TeX-add-symbols
     '("exampleBox" 1)
     "thetitle"
     "theauthor"
     "supervisor"
     "theinstitute"
     "showtoc")
    (TeX-run-style-hooks
     "framed"
     "listings"
     "tikz"
     "xcolor"
     "latex2e"
     "beamer10"
     "beamer"
     ""
     "presentation_setup"
     "backup_slides"
     "carbon_credits"
     "notes_by_georg"
     "my_work"
     "questions")))

