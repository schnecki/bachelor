(TeX-add-style-hook "basics"
 (lambda ()
    (LaTeX-add-labels
     "fig:stack")
    (TeX-add-symbols
     "sectiontitle"
     "mypagetitle"
     "thefootnote")))

