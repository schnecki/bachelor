(TeX-add-style-hook "main"
 (lambda ()
    (LaTeX-add-bibliographies
     "biblio.bib")
    (LaTeX-add-labels
     "sec:Term_Rewrite_Systems"
     "sec:Results"
     "tbl:obs"
     "sec:Future_Work")
    (TeX-add-symbols
     '("exampleBox" 1)
     '("icomment" 1)
     "thetitle"
     "theauthor"
     "supervisor"
     "theinstitute"
     "showtoc"
     "mypagetitle")
    (TeX-run-style-hooks
     "pgfplots"
     "subfig"
     "adjustbox"
     "amortised"
     "framed"
     "listings"
     "tikz"
     "comment"
     "xcolor"
     "etex"
     ""
     "latex2e"
     "beamer10"
     "beamer"
     "t"
     "presentation_setup"
     "basics"
     "TRS"
     "my_work"
     "questions")))

