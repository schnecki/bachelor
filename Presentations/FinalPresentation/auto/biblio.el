(TeX-add-style-hook "biblio"
 (lambda ()
    (LaTeX-add-bibitems
     "JHLH10"
     "JLHSH09"
     "TeReSe"
     "AM10"
     "Pol92"
     "DW91"
     "AotoY03"
     "Yamada01"
     "moser"
     "Arts"
     "klop1990term"
     "sat"
     "tarjan85"
     "Jost09carboncredits"
     "MoserNotes")))

