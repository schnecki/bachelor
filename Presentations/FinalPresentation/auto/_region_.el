(TeX-add-style-hook "_region_"
 (lambda ()
    (LaTeX-add-labels
     "d:runtimecomplexity")
    (TeX-add-symbols
     '("exampleBox" 1)
     "thetitle"
     "theauthor"
     "supervisor"
     "theinstitute"
     "sectiontitle")
    (TeX-run-style-hooks
     "amortised"
     "framed"
     "listings"
     "tikz"
     "xcolor"
     ""
     "latex2e"
     "beamer10"
     "beamer"
     "fleqn"
     "presentation_setup")))

