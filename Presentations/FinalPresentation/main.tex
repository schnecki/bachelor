%%% final.tex ---
%%
%% Filename: final.tex
%% Description:
%% Author: Manuel Schneckenreither
%% Maintainer:
%% Created: Mi Jun 11 20:50:13 2014 (+0200)
%% Version:
%% Package-Requires: ()
%% Last-Updated: Do Jun 19 01:06:48 2014 (+0200)
%% By: Manuel Schneckenreither
%% Update #: 70
%% URL:
%% Doc URL:
%% Keywords:
%% Compatibility:
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%% Commentary:
%%
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%% Change Log:
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation; either version 3, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; see the file COPYING.  If not, write to
%% the Free Software Foundation, Inc., 51 Franklin Street, Fifth
%% Floor, Boston, MA 02110-1301, USA.
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%% Code:


\documentclass[t]{beamer}
\usepackage{etex}

% title of paper
\newcommand{\thetitle}{Amortized Resource Analysis and Typed Term
  Rewriting}
\newcommand{\theauthor}{Manuel Schneckenreither}
\newcommand{\supervisor}{Assoc. Prof.~Dr.~Georg Moser}
\newcommand{\theinstitute}{Supervisor: \supervisor{}
  \newline University of Innsbruck --- Bachelor Program  for Computer
  Science}
\newcommand{\icomment}[1]{\begin{center}\tt {#1} \end{center}}
% inlcude the presentation style setup file
% \include{presentation_setup}

\title{\thetitle}
\author{\theauthor}
\institute{\theinstitute}
\date{24. June 2014}


% init colors
\usepackage{xcolor}
\definecolor{exmplcolor}{rgb}{0,0.1,0.2}
\newcommand{\exampleBox}[1]
{
  % \begin{framed}
  \begin{example}
    \textcolor{exmplcolor}{#1}
  \end{example}
  % \end{framed}
}

\usepackage{comment}
\usepackage{tikz}
\usepackage{listings}
\lstset{
  numbers=none,
  stepnumber=1,
  firstnumber=1,
  numberfirstline=true,
  captionpos=b,
  basicstyle=\footnotesize,
  extendedchars=true,
  literate= {->}{{$\rightarrow$}}1 {-->}{{$\rightarrow$}}2
  {µ}{{$\mu$}}1 {⋅}{{$\cdot$}}1
  {o--}{{\rule[0.5ex]{3mm}{0.1mm}}}2,
}

\lstdefinestyle{base}{
  emptylines=1,
  moredelim=**[is][keywordstyle]{**}{**},
  morecomment=[l]{{-- }},
  % commentstyle=\itshape\color{green!30!black},
}

\lstdefinestyle{base-nocomments}{
  emptylines=1,
  moredelim=**[is][keywordstyle]{**}{**},
}

\usepackage{framed}
\usepackage{amortised}
\usepackage{adjustbox}
\usepackage{subfig}
\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{positioning}

\makeatletter
\@addtoreset{figure}{section}
\makeatother
\renewcommand\thefigure{\arabic{section}.\arabic{figure}}
\include{presentation_setup}


\begin{document}

\newcommand{\showtoc}{
  \frame{
    \frametitle{Table of Contents}
    \tableofcontents
  }
}


\frame{\maketitle}
\showtoc{}

\pagenumbering{arabic}
% \sectionnumbering{arabic}


% basics
\input{basics}

\section{Term Rewrite Systems}
\label{sec:Term_Rewrite_Systems}


\begin{frame}[fragile]
  \frametitle{Term Rewrite System (TRS)}
  \framesubtitle{Term Rewrite Systems}

  \begin{itemize}
  \item A TRS is a \textit{system of equations}, where all equations are rewrite rules.
  \item Rewrite rules are only allowed to be used in one direction.
  \item TRSs are Turing-complete.
  \end{itemize}

  \pause

  \begin{exampleblock}{Example 1: TRS $\mathcal{R} := \mathsf{minus}$}
    \begin{lstlisting}[caption=, frame=none, style=base]
      minus(x, 0)         -> x
      minus(s(x), s(y))   -> minus(x, y)
    \end{lstlisting}
    \center{
      {\footnotesize where $\mathsf{s}(x)$ is the successor function.}
    }

  \end{exampleblock}

  \pause

  \begin{exampleblock}{Example Derivation, $\mathcal{R} = \mathsf{minus}$}
    \begin{lstlisting}[caption=, style=base, escapeinside=\?\?]
      minus(s(?\alert<3-3>{s(0)}?), s(?\alert<3-3>{0}?))   --> minus(?\alert<4-4>{s(0)}?, 0) --> ?\alert<5-5>{s(0)}?
      minus(   2  ,   1 )   ?\hspace{-0.07cm}?--> minus( 1 , 0) ?\hspace{-0.03cm}?-->  1
    \end{lstlisting}

  \end{exampleblock}


  \hfill \dots \footnotesize where $k$ represents $\mathsf{s}^k(\mathsf{0})$

  % \begin{align*}
  %   & \mathsf{minus}(\mathsf{s}(\mathsf{s(0)}),\mathsf{s}(0)) & \rightarrow
  %   & \mathsf{minus}(\mathsf{s}(0),\mathsf{0}) & \rightarrow & \mathsf{s}(0) \hspace{5cm}\\
  %   & \mathsf{minus}(2,1) & \rightarrow & \mathsf{minus}(1,\mathsf{0}) &
  %   \rightarrow & 1 & \\
  %   &&&&&\text{\hfill \dots when $n$ represents $\mathsf{s}^n(\mathsf{0})$}
  % \end{align*}


  \vfill

\end{frame}


\begin{frame}[fragile]
  \frametitle{Term Rewrite System (TRS) Example 2 }
  \framesubtitle{Term Rewrite Systems}

  \begin{exampleblock}{Example 2: TRS $\mathcal{R} := \mathsf{quot}$}
    \begin{lstlisting}[caption=, frame=none, style=base]
      minus(x, 0)         -> x
      minus(s(x), s(y))   -> minus(x, y)

      quot(0, s(y))       -> 0
      quot(s(x), s(y))    -> s(quot(minus(x, y), s(y)))
    \end{lstlisting}

  \end{exampleblock}

  \pause

  \begin{exampleblock}{Example: Derivation, $\mathcal{R} = \mathsf{quot}$}
    \lstset{language=Java,numbers=none, style=base}
    \begin{lstlisting}[frame=none, label={lst:example_derivation}]
      **quot(4, 2)** -> s(quot(**minus(3, 1)**, 2))
      -> s(quot(**minus(2, 0)**, 2)) -> s(**quot(2, 2)**)
      -> s(s(quot(**minus(1, 1)**, 2)))
      -> s(s(quot(**minus(0, 0)**, 2)))
      -> s(s(**quot(0, 2)**))
      -> s(s(0))
    \end{lstlisting}
  \end{exampleblock}


  % \hfill \dots \footnotesize where $k$ represents $\mathsf{s}^k(\mathsf{0})$

  % \begin{align*}
  %   & \mathsf{minus}(\mathsf{s}(\mathsf{s(0)}),\mathsf{s}(0)) & \rightarrow
  %   & \mathsf{minus}(\mathsf{s}(0),\mathsf{0}) & \rightarrow & \mathsf{s}(0) \hspace{5cm}\\
  %   & \mathsf{minus}(2,1) & \rightarrow & \mathsf{minus}(1,\mathsf{0}) &
  %   \rightarrow & 1 & \\
  %   &&&&&\text{\hfill \dots when $n$ represents $\mathsf{s}^n(\mathsf{0})$}
  % \end{align*}


  \vfill

\end{frame}


% TRS
\input{TRS}


% bachelor thesis
% \showtoc
\input{my_work}


\begin{frame}[fragile]

  \frametitle{Typed Term Rewrite Systems}
  \framesubtitle{Amortized Resource Analysis}

  \only<1>{
    \begin{align*}
      A &::= X \mid \langle
      \constypewo{c_1}{\overline{T_1}},\dots,\constypewo{c_n}{\overline{T_n}}
      \rangle \mid A_1 \times \cdots \times A_n \mid \mu X.A \\
      F &::= \funtype{\overline{A}}{A}{}{} \tkom
    \end{align*}
  }

  \only<2->{
    \begin{equation*}
      T ::= X
      \mid \mu X.\{ \constype{c_1}{\overline{T_1}}{\alert {k_1}}, \ldots ,
      \constype{c_n}{\overline{T_n}}{\alert {k_n}} \}
      \mid \funtype{\overline{T}}{T}{p}{}
      \tkom
    \end{equation*}
  }

  \lstset{language=,numbers=none}
  \begin{lstlisting}[caption=, frame=single, style=base, escapeinside=\?\?]
    **Type Definitions:**
    Nat?\only<2->{(\alert{$c_0$})}? ::= µX.< 0?\only<2->{\alert{:0}}?, s(X)?\only<2->{\alert{:$c_0$}}? >

    **Function Symbol Type Definitions:**
    minus ::= Nat?\only<2->{(\alert{$m_1$})}? x Nat?\only<2->{(\alert{$m_2$})}? -> Nat?\only<2->{(\alert{$m_r$})}?
    quot  ::= Nat?\only<2->{(\alert{$q_1$})}? x Nat?\only<2->{(\alert{$q_2$})}? -> Nat?\only<2->{(\alert{$q_r$})}?

    **Rewrite Rules:**
    minus(x, 0)         -> x
    minus(s(x), s(y))   -> minus(x, y)

    quot(0, s(y))       -> 0
    quot(s(x), s(y))    -> s(quot(minus(x, y), s(y)))
  \end{lstlisting}


\end{frame}


\section{Live Demo}

\begin{frame}[fragile]
  \frametitle{Live Demo}
  \framesubtitle{Practical Part}
  % \subsection{\mypagetitle}

  \centering
  Both programs are written in \textbf{Haskell}.
  \vspace{2cm}

  \centering
  Let's try them out!

\end{frame}

% Introduction
% \showtoc
% \input{carbon_credits}


% Notes by Georg
% \showtoc
% \input{notes_by_georg}


\section{Details}

\begin{frame}[fragile]
  \frametitle{Amortized Resource Analysis}
  \framesubtitle{Details}
  % \subsection{\mypagetitle}

  \begin{columns}
    \column{0.5\textwidth}

    \begin{figure}
      \trimbox{0cm 0cm 0cm 1cm}{
        \begin{tikzpicture}[>=latex,font=\sffamily]

          % nodes
          \alert<1>{\node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
            sep=0pt,draw=black,fill=blue!10,semithick] at (0,5) (A) {Typed TRS};}

          \alert<2>{\node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
            sep=0pt,draw=black,fill=blue!10,semithick] at (0,3) (B)
            {Linear Constraint Problem};}

          \alert<3>{\node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
            sep=0pt,draw=black,fill=blue!10,semithick] at (0,1) (C)
            {Costs for Annotated Types};}

          \alert<4>{\node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
            sep=0pt,draw=black,fill=blue!10,semithick] at (0,-1) (D) {Complexity};}


          % arrows
          \draw [->,shorten >=2pt,shorten <=2pt,semithick] (A.south) --
          +(0,-1.7em) -| (B) node [text width=4.5cm,right,midway] {\footnotesize
            Inference rules + \\well-typedness \\constraints};

          \draw [->,shorten >=2pt,shorten <=2pt,semithick]
          (B.south) -- +(0,-1.7em) -| (C) node [text width=2.5cm, right,midway] {\footnotesize
            Constraint solver\\ (e.g. lp\_solve)};

          \draw [->,shorten >=2pt,shorten <=2pt,semithick]
          (C.south) -- +(0,-1.7em) -| (D) node [text width=4.0cm, right,midway] {\footnotesize
            Insert costs into data \\structure};


        \end{tikzpicture}
      }
    \end{figure}
    \column{0.5\textwidth}
    \only<1>{
      \begin{exampleblock}{Typed TRS}
        \begin{flalign*}
          & minus \defsym  Nat \times Nat \rightarrow  Nat &
        \end{flalign*}
        \vspace{-1cm}
        \begin{flalign*}
          & \mathsf{minus}(x,\mathsf{0})       &  \rightarrow & x &\\
          & \mathsf{minus}(\mathsf{s}(x), \mathsf{s}(y)) &  \rightarrow &
          \mathsf{minus}(x, y) &
        \end{flalign*}
      \end{exampleblock}
    }
    \only<2>{
      \begin{exampleblock}{Linear Constraint Problem}
        \begin{align*}
          &\hspace{0.2cm}\vdots\\
          p_0 &= p_0\\
          p_1 &= p_1\\
          p_r &= p_r\\
          p_{minus} &= p_{minus} - 1 + p_0 + p_1\\
          &\hspace{0.2cm} \vdots
        \end{align*}
      \end{exampleblock}


    }
    \only<3>{
      \begin{exampleblock}{Costs for Annotated Types}
        \begin{align*}
          \hspace{0.2cm}\vdots\\
          p_0 &= 1\\
          p_1 &= 0\\
          p_r &= 0\\
          p_{minus} &= 1\\
          \hspace{0.2cm} \vdots
        \end{align*}
      \end{exampleblock}
    }
    \only<4>{
      \begin{exampleblock}{Complexity}
        \begin{flalign*}
          & minus \defsym  \funtype{Nat(1) \times Nat(0)}{Nat(0)}{1}{} &
        \end{flalign*}
        \vspace{-1cm}
        \begin{flalign*}
          & \mathsf{minus}(x,\mathsf{0})       &  \rightarrow & x &\\
          & \mathsf{minus}(\mathsf{s}(x), \mathsf{s}(y)) &  \rightarrow &
          \mathsf{minus}(x, y) &
        \end{flalign*}
        \rule[0.5ex]{\textwidth}{0.1mm}
        % \rule{\textwidth}{0.1pt}
        \vspace{0.5cm}
        $O(minus) = 1 \cdot p_0$\\
        $\Rightarrow$ \textsf{minus} is linear in first argument
      \end{exampleblock}
    }


  \end{columns}
\end{frame}


\begin{frame}
  \frametitle{Polynomial Interpretation Analysis}
  \framesubtitle{Details}
  % \subsection{\mypagetitle}

  \begin{columns}
    \column{0.5\textwidth}

    \begin{figure}
      \trimbox{0cm 0cm 0cm 1cm}{
        \begin{tikzpicture}[>=latex,font=\sffamily]

          % nodes
          \node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
          sep=0pt,draw=black,fill=blue!10,semithick] at (0,5) (A) {Typed TRS};

          \node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
          sep=0pt,draw=black,fill=blue!10,semithick] at (0,3) (B)
          {Linear Constraint Problem};

          \node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
          sep=0pt,draw=black,fill=blue!10,semithick] at (0,1) (C)
          {Costs for Annotated Types};

          \node [text width=4.5cm,text centered,minimum width=1cm,minimum height=1.5em,outer
          sep=0pt,draw=black,fill=blue!10,semithick] at (0,-1) (D) {Complexity};


          % arrows
          \draw [->,shorten >=2pt,shorten <=2pt,semithick] (A.south) --
          +(0,-1.7em) -| (B) node [text width=4.5cm,right,midway] {\footnotesize
            {\color{red}Polynomial Interpretations} + \\well-typedness \\constraints};

          \draw [->,shorten >=2pt,shorten <=2pt,semithick]
          (B.south) -- +(0,-1.7em) -| (C) node [text width=2.5cm, right,midway] {\footnotesize
            Constraint solver\\ (e.g. lp\_solve)};

          \draw [->,shorten >=2pt,shorten <=2pt,semithick]
          (C.south) -- +(0,-1.7em) -| (D) node [text width=4.0cm, right,midway] {\footnotesize
            Insert costs into data \\structure};

        \end{tikzpicture}

      }
    \end{figure}
    \column{0.5\textwidth}
    \begin{itemize}
    \item Same structure.
    \item Completely different approach to reach linear constraint problem.
    \item If a TRS $\RS$ is well-typed, then there exists a linear interpretation that
      orients $\RS$.
    \end{itemize}


  \end{columns}
\end{frame}


% inlcude backup slides

\section{Results}
\label{sec:Results}


\newcommand{\mypagetitle}{Successfully Typed TRS}
\begin{frame}
  \frametitle{\mypagetitle}
  \framesubtitle{Results (in milliseconds)}


  \begin{table}[t]
    \footnotesize{}
    \centering
    \begin{tabular}{| l || r || r || r || r | }
      \hline
      % BEGIN RECEIVE ORGTBL exec
      Example & \textbf{\textbf{ARA}} & \textbf{\textbf{PIA}} & \textbf{\textbf{TcT S1}} & \textbf{\textbf{TcT S2}} \\
      \hline
      \hline
      addition.trs & 10.5 & \textbf{\textbf{5.5}} & 48.4 & 95.5 \\
      huge.trs & 106471.0 & \textbf{\textbf{148.0}} & Maybe & 23488.5 \\
      id.trs & \textbf{\textbf{12.0}} & 13.6 & 52.2 & 41.5 \\
      list.trs & \textbf{\textbf{10.7}} & 12.6 & Maybe & 51.6 \\
      queue2.trs & \textbf{\textbf{13.9}} & 14.9 & Maybe & 21631.9 \\
      queue.trs & \textbf{\textbf{14.3}} & 19.2 & Maybe & 20902.4 \\
      quotient.trs & 15.6 & \textbf{\textbf{10.6}} & 130.2 & 3523.0 \\
      reverse.trs & 10.7 & \textbf{\textbf{9.0}} & 80.0 & 2143.6 \\
      typed-bits.trs & \textbf{\textbf{9.8}} & 14.9 & 342.6 & 3475.6 \\
      typed-div.trs & 9.1 & \textbf{\textbf{6.8}} & 132.2 & 3488.8 \\
      typed-flatten.trs & \textbf{\textbf{9.8}} & 15.6 & 98.3 & 2666.6 \\
      typed-jones1.trs & 10.0 & \textbf{\textbf{6.9}} & 76.8 & 2178.2 \\
      typed-jones2.trs & 10.5 & \textbf{\textbf{7.1}} & Maybe & 4508.1 \\
      typed-jones4.trs & \textbf{\textbf{9.8}} & 16.4 & 71.5 & 555.9 \\
      typed-jones6.trs & \textbf{\textbf{9.6}} & 14.3 & 108.3 & 2981.6 \\
      typed-rationalPotential.raml.trs & \textbf{\textbf{12.8}} & 20.4 & 172.8 & 13238.4 \\
      \hline
      % END RECEIVE ORGTBL exec
    \end{tabular}
    \caption{\textbf{TcT S1}
      \textit{-a irc -s ``semantics~:maxdegree 1''}, \textbf{TcT S2} \textit{-a irc -s
        ``raml''}.}
    \label{tbl:obs}

  \end{table}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Illustrated}
  \framesubtitle{Results}

  \begin{figure}[ht]
    \centering
    \begin{tikzpicture}
      \begin{axis}[
        xlabel=Example,
        ylabel=Time in ms,
        xtick=\empty,
        symbolic x  coords={addition,huge,id,list,queue2,queue,quotient,reverse,typed-bits,typed-div,typed-flatten,typed-jones1,typed-jones2,typed-jones4,typed-jones6,typed-rationalPotential},
        legend pos=outer north east]
        \addplot[ybar,mark=*,blue] plot coordinates {
          (addition, 10.5)
          (id, 12.0)
          (list, 10.7)
          (queue2, 13.9)%14.9|Maybe|21631.9|
          (queue, 14.3)
          (quotient, 15.6)
          (reverse, 10.7)
          (typed-bits, 9.8)
          (typed-div, 9.1)
          (typed-flatten, 9.8)
          (typed-jones1, 10.0)
          (typed-jones2, 10.5)
          (typed-jones4, 9.8)
          (typed-jones6, 9.6)
          (typed-rationalPotential, 12.8)
        };
        \addlegendentry{ARA}


        \addplot[ybar,mark=*,red] plot coordinates {
          (addition, 5.5)
          (id, 13.6)
          (list, 12.6)
          (queue2, 14.9)
          (queue, 19.2)
          (quotient, 10.6)
          (reverse, 9.0)
          (typed-bits, 14.9)
          (typed-div, 6.8)
          (typed-flatten, 15.6)
          (typed-jones1, 6.9)
          (typed-jones2, 7.1)
          (typed-jones4, 16.4)
          (typed-jones6, 14.3)
          (typed-rationalPotential, 20.4)
        };
        \addlegendentry{PIA}


      \end{axis}
    \end{tikzpicture}
  \end{figure}

\end{frame}


\begin{frame}
  \frametitle{Success by TcT Only (Selected Examples)}
  \framesubtitle{Results (in milliseconds)}

  \begin{table}[t]
    \footnotesize
    \centering
    \begin{tabular}{| l || p{0.6cm} | c || p{0.6cm} | c || p{1.1cm} | c || p{1.1cm} | c | }
      \hline
      Example & \multicolumn{2}{| c || }
      {\textbf{ARA}} & \multicolumn{2}{| c || }{\textbf{PIA}}
      & \multicolumn{2}{| c || }{\textbf{TcT S1}}
      & \multicolumn{2}{| c | }{\textbf{TcT S2}}\\
      % BEGIN RECEIVE ORGTBL obs2
      %
      \hline
      \hline
      infeasible & \hfill 10.4 & F & \hfill 5.0 & F & \hfill 78.2 & M & \hfill 1540.9 & S \\
      multiplication & \hfill 8.8 & F & \hfill 5.3 & F & \hfill 94.8 & M & \hfill 11655.8 & S \\
      typed-appendAll.raml & \hfill 12.7 & F & \hfill 7.1 & F & \hfill 297.8 & \textbf{S} & \hfill 2386.8 & S \\
      typed-bubblesort-nat & \hfill 18.0 & F & \hfill 8.3 & F & \hfill 1181.7 & M & \hfill 39546.1 & S \\
      typed-clique & \hfill 7.1 & F & \hfill 2.4 & F & \hfill 346.7 & M & \hfill 20083.9 & S \\
      typed-duplicates.raml & \hfill 28.9 & F & \hfill 23.8 & F & \hfill 638.6 & M & \hfill 54236.4 & S \\
      typed-dyade.raml & \hfill 57.7 & F & \hfill 17.8 & F & \hfill 1237.9 & M & \hfill 30715.1 & S \\
      typed-egypt & \hfill 14.7 & F & \hfill 14.0 & F & \hfill 2276.2 & \textbf{S} & \hfill 6069.8 & S \\
      typed-flatten.raml & \hfill 26.3 & F & \hfill 28.6 & F & \hfill 564.4 & M & \hfill 43726.0 & S \\
      typed-insertionsort.raml & \hfill 32.5 & F & \hfill 25.7 & F & \hfill 527.7 & M & \hfill 37508.3 & S \\
      typed-isort & \hfill 10.8 & F & \hfill 18.6 & F & \hfill 208.0 & M & \hfill 9279.0 & S \\
      typed-kruskal & \hfill 6.9 & F & \hfill 21.3 & F & \hfill 1584.3 & M & \hfill 58552.5 & S \\
      typed-minsort.raml & \hfill 21.9 & F & \hfill 46.2 & F & \hfill 425.5 & M & \hfill 44863.2 & S \\
      typed-mult2 & \hfill 7.1 & F & \hfill 8.3 & F & \hfill 105.5 & M & \hfill 11711.8 & S \\
      typed-mult & \hfill 7.4 & F & \hfill 30.6 & F & \hfill 93.8 & M & \hfill 4622.6 & S \\
      typed-pairs & \hfill 7.5 & F & \hfill 2.4 & F & \hfill 195.0 & M & \hfill 10103.3 & S \\
      % typed-prod2 & \hfill 8.2 & F & \hfill 14.7 & F & \hfill 152.7 & M & \hfill 25059.6 & S \\
      % typed-qbf & \hfill 14.5 & F & \hfill 12.4 & F & \hfill 474.9 & M & \hfill 4626.4 & S \\
      % typed-quad & \hfill 7.1 & F & \hfill 23.8 & F & \hfill 146.4 & M & \hfill 15071.3 & S \\
      % typed-quicksort-nat & \hfill 11.1 & F & \hfill 14.0 & F & \hfill 675.9 & M & \hfill 53038.1 & S \\
      % typed-quicksort & \hfill 16.8 & F & \hfill 37.2 & F & \hfill 523.7 & M & \hfill 48134.8 & S \\
      % typed-reverse & \hfill 6.8 & F & \hfill 8.7 & F & \hfill 121.4 & M & \hfill 10594.9 & S \\
      % typed-sat & \hfill 32.0 & F & \hfill 11.6 & F & \hfill 433.1 & M & \hfill 13095.1 & S \\
      % typed-shuffleshuffle & \hfill 7.7 & F & \hfill 28.6 & F & \hfill 254.6 & M & \hfill 29956.3 & S \\
      % typed-shuffle & \hfill 7.2 & F & \hfill 25.7 & F & \hfill 194.0 & M & \hfill 19022.7 & S \\
      % typed-square & \hfill 7.7 & F & \hfill 16.1 & F & \hfill 96.8 & M & \hfill 4581.3 & S \\
      % typed-subtrees.raml & \hfill 9.4 & F & \hfill 21.3 & F & \hfill 169.2 & M & \hfill 8888.4 & S \\
      % typed-tadd2 & \hfill 7.0 & F & \hfill 8.3 & F & \hfill 141.1 & M & \hfill 1214.5 & S \\
      % typed-tadd & \hfill 7.6 & F & \hfill 7.4 & F & \hfill 182.2 & M & \hfill 1807.4 & S \\
      \hline
      %
      % END RECEIVE ORGTBL obs2
    \end{tabular}

    \caption{TcT ad.
      % This table shows the advantages of TcT to implemented variants from
    % this paper. It listss for which TcT could successfully calculate it's
    % runtime complexity but could not
    % be solved by neither the amortized resource analysis (ARA) nor polynomial
    % interpretation
    % analysis (PIA). TcT uses several methods which exceed the capabilities of the
    % ARA and the PIA.\newline
    Legend: \textbf{F}\ldots Failure,  \textbf{S}\ldots Success,
    \textbf{T}\ldots Timeout, \textbf{M}\ldots Maybe}
    % \label{tbl:obs2}
  \end{table}


\end{frame}


\begin{frame}
  \frametitle{List of Infeasible Problems}
  \framesubtitle{Results (in milliseconds)}

  \begin{table}[t]
    \footnotesize{}
    \centering
    \begin{tabular}{| l || p{1cm} | c || p{0.8cm} | c || p{1cm} | c || p{0.5cm} | c | }% {| l || r | c || r | c ||  r | c || r | c | }
      \hline
      Example & \multicolumn{2}{| c || }
      {\textbf{ARA}} & \multicolumn{2}{| c || }{\textbf{PIA}}
      & \multicolumn{2}{| c || }{\textbf{TcT S1}}
      & \multicolumn{2}{| c | }{\textbf{TcT S2}}\\
      % BEGIN RECEIVE ORGTBL failures
      %
      \hline
      \hline
      typed-bfs.raml.trs & \hfill  & \textbf{T} & \hfill \textbf{40.4} & F & \hfill 1181.7 & M & \hfill  & T \\
      typed-bitonic.raml.trs & \hfill 14.3 & F & \hfill 46.2 & F & \hfill 2073.5 & M & \hfill  & T \\
      typed-bitvectors.raml.trs & \hfill 8385.1 & F & \hfill 129.1 & F & \hfill 4976.5 & M & \hfill  & T \\
      typed-clevermmult.raml.trs & \hfill 175.7 & F & \hfill 30.6 & F & \hfill 1483.9 & M & \hfill  & T \\
      typed-eratosthenes.raml.trs & \hfill 461.8 & F & \hfill 54.0 & F & \hfill 2254.8 & M & \hfill  & T \\
      typed-eratosthenes.trs & \hfill 378.4 & F & \hfill 37.2 & F & \hfill 2245.5 & M & \hfill  & T \\
      typed-listsort.raml.trs & \hfill  & \textbf{T} & \hfill \textbf{21.3} & F & \hfill 948.2 & M & \hfill  & T \\
      typed-matrix.raml.trs & \hfill \textbf{648.1} & F & \hfill \textbf{5.0} & F & \hfill 1866.6 & M & \hfill  & T \\
      typed-mergesort-nat.trs & \hfill 19.5 & F & \hfill 5.3 & F & \hfill 759.0 & M & \hfill  & T \\
      typed-mergesort.raml.trs & \hfill 38.2 & F & \hfill 7.1 & F & \hfill 819.4 & M & \hfill  & T \\
      typed-mergesort.trs & \hfill 31.8 & F & \hfill 40.4 & F & \hfill 859.7 & M & \hfill  & T \\
      typed-mmult-nat.trs & \hfill 14.1 & F & \hfill 129.1 & F & \hfill 404.1 & M & \hfill  & T \\
      typed-queue.raml.trs & \hfill 50.3 & F & \hfill 17.8 & F & \hfill 997.4 & M & \hfill  & T \\
      typed-quicksort.raml.trs & \hfill 54.4 & F & \hfill 54.0 & F & \hfill 882.4 & M & \hfill  & T \\
      typed-splitandsort.raml.trs & \hfill 222.3 & F & \hfill 18.6 & F & \hfill 1180.7 & M & \hfill  & T \\
      typed-tuples.raml.trs & \hfill 26.9 & F & \hfill 21.3 & F & \hfill 706.1 & M & \hfill  & T \\
      \hline
      %
      % END RECEIVE ORGTBL failures
    \end{tabular}
    \caption{Failures. % This table lists selected examples which could not be solved by any
    % variant. The timeout was set to 60 seconds. All TRSs which could not be
    % resolved in that time, like all examples in column TcT S2, are missing the
    % execution time and are symbolized with the outcome T.\newline
    Legend: \textbf{F}\ldots Failure,  % \textbf{S}\ldots Success,
    \textbf{T}\ldots Timeout (60 sec.), \textbf{M}\ldots Maybe}
    % \label{tbl:failures}
  \end{table}
\end{frame}


\begin{frame}
  \frametitle{Estimated and Clocked Time}
  \framesubtitle{Time Report (in [h:mm])}


  \begin{table}[b!]\footnotesize
    \centering
    \begin{minipage}{0.88\textheight}
      \begin{tabular}{| l || r | r |}
        \hline
        % \multirow{2}{*}{Task}
        % & \multicolumn{2}{| c || }{\textbf{Estimated}} &
        % \multicolumn{2}{|c | }{\textbf{Clocked}} \\
        % & Sum & Part & Sum & Parts \\
        % BEGIN RECEIVE ORGTBL time
% --
\multicolumn{1}{|l||}{\textbf{Task}} & \textbf{Estimated} & \textbf{Clocked} \\
\hline
\-- Initial Presentation & 12:00 & 12:00 \\
\-- Main Preparation & 112:00 & 108:38 \\
\-- Preparation for Requirement 1 & 20:00 & 23:40 \\
\-- Preparation for Requirement 2 & 27:00 & 7:59 \\
\-- Design for Requirement 1 & 8:00 & 2:16 \\
\-- Design for Requirement 2 & 8:00 & 0:00 \\
\-- Code Requirement 1 & 117:30 & 234:17 \\
\-- Code Requirement 2 & 84:30 & 76:37 \\
\-- Testing/Comparing & 10:00 & 3:39 \\
\-- Writing the Paper & 109:00 & 96:49 \\
\hline
\textbf{Total time} & \textbf{508:00} & \textbf{565:55} \\
\hline %
        % END RECEIVE ORGTBL time
      \end{tabular}
      % \caption{}
    \end{minipage}
  \end{table}


\end{frame}


\begin{frame}[fragile]
  \frametitle{Month vs. Clocked Time}
  \framesubtitle{Time Report}


  \begin{figure}[ht]
    \centering
    \hspace{-1.2cm}
    \begin{tikzpicture}
      \begin{axis}[
        xlabel=Month,
        ylabel=Clocked Time,
        symbolic x coords={Sep,Oct,Nov,Dec,Jan,Feb,Mar,Apr,May,Jun},
        xtick={Sep,Nov,Jan,Mar,May}]
        \addplot[smooth,mark=*,blue] plot coordinates {
          (Sep, 81)
          (Oct, 47)
          (Nov, 20)
          (Dec, 71)
          (Jan, 53)
          (Feb, 20)
          (Mar,104)
          (Apr, 33)
          (May, 79)
          (Jun, 53)
        };

      \end{axis}
    \end{tikzpicture}
  \end{figure}

\end{frame}

\section{Future Work}
\label{sec:Future_Work}


\begin{frame}
  \frametitle{Future Work}
  \framesubtitle{Future Work}

  \begin{itemize}
  \item Extend beyond linear cases
  \item<2-> Integration into TcT
  \item<3-> Considering Higher-Order Functions, which are partially applied functions, e.g.
    \begin{flalign*}
    & f \defsym & [\mathsf{Int}] \rightarrow [\mathsf{Int}] & \hspace{2cm} & map
    \defsym (\mathsf{a} \rightarrow \mathsf{a}) \rightarrow [\mathsf{a}] \rightarrow [\mathsf{a}] & \hspace{2cm} \\
    & f =    & \mathsf{map}\ \mathsf{(+2)} &
    \end{flalign*}


  \end{itemize}


\end{frame}

% questions
\input{questions}


\bibliographystyle{apalike}
\bibliography{biblio.bib}

\end{document}


\begin{comment}

  :splice nil/t  ...return only table body lines. Default is nil.
  :fmt fmt       ...format to use, e.g. "$%s$" or (2 "$%s$" 4 "%s\%%") for specific columns.
  :skip n        ...skip n rows
  #+ORGTBL: SEND time orgtbl-to-latex :splice nil :skip 0 :tstart "% --" :tend "\\hline % " :hfmt (1 "\\multicolumn{1}{|l||}{%s}")
  | *Task*                            | *Estimated* | *Clocked* |
  |-----------------------------------+-------------+-----------|
  | \-- Initial Presentation          |       12:00 |     12:00 |
  | \-- Main Preparation              |      112:00 |    108:38 |
  | \-- Preparation for Requirement 1 |       20:00 |     23:40 |
  | \-- Preparation for Requirement 2 |       27:00 |      7:59 |
  | \-- Design for Requirement 1      |        8:00 |      2:16 |
  | \-- Design for Requirement 2      |        8:00 |      0:00 |
  | \-- Code Requirement 1            |      117:30 |    234:17 |
  | \-- Code Requirement 2            |       84:30 |     76:37 |
  | \-- Testing/Comparing             |       10:00 |      3:39 |
  | \-- Writing the Paper             |      109:00 |     96:49 |
  |-----------------------------------+-------------+-----------|
  | *Total time*                      |    *508:00* |  *565:55* |


  | Month     | Clocked |
  |-----------+---------|
  | September |   81:00 |
  | October   |   47:06 |
  | November  |   20:34 |
  | December  |   71:56 |
  | January   |   53:53 |
  | February  |   20:51 |
  | March     |  104:14 |
  | April     |   33:21 |
  | May       |   79:37 |
  | June      |   53:23 |

  | Example                      |  ARA |  PIA | TcT S1 |  TcT S2 |
  |------------------------------+------+------+--------+---------|
  | addition                     | 10.5 |  5.5 |   48.4 |    95.5 |
  | id                           | 12.0 | 13.6 |   52.2 |    41.5 |
  | list                         | 10.7 | 12.6 |  Maybe |    51.6 |
  | queue2                       | 13.9 | 14.9 |  Maybe | 21631.9 |
  | queue                        | 14.3 | 19.2 |  Maybe | 20902.4 |
  | quotient                     | 15.6 | 10.6 |  130.2 |  3523.0 |
  | reverse                      | 10.7 |  9.0 |   80.0 |  2143.6 |
  | typed-bits                   |  9.8 | 14.9 |  342.6 |  3475.6 |
  | typed-div                    |  9.1 |  6.8 |  132.2 |  3488.8 |
  | typed-flatten                |  9.8 | 15.6 |   98.3 |  2666.6 |
  | typed-jones1                 | 10.0 |  6.9 |   76.8 |  2178.2 |
  | typed-jones2                 | 10.5 |  7.1 |  Maybe |  4508.1 |
  | typed-jones4                 |  9.8 | 16.4 |   71.5 |   555.9 |
  | typed-jones6                 |  9.6 | 14.3 |  108.3 |  2981.6 |
  | typed-rationalPotential.raml | 12.8 | 20.4 |  172.8 | 13238.4 |


\end{comment}


% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% final.tex ends here
