#!\bin\bash

PROGRAM=`which tct`
#COMMAND="$PROGRAM -v a -a irc -s \"semantics :maxdegree 1\""
COMMAND="$PROGRAM -v a -a irc -s \"raml\""
TIMES=10                        # number of test runs per file
FOLDERS="./doc/examples/ ./doc/examples/infeasible/"
COLWIDTH=10                     # width of columns
PRINTOUTCOME=0                  # 1 means print Success/Failure text also
TIMEOUT=60

function displayUsage {
    echo -e -n "Usage:\n$0"
	  echo -e -n "\n\nTo edit the COMMAND, COLUMN WIDTH, FOLDERS, etc. edit the file directly.\n\n"
}

function setColWidth {

    len=$(($COLWIDTH-`echo $new | wc -c`))
    # bef=$(($len/2))
    # aft=$(($len-$(($len/2))))
    aft=1
    bef=$(($len-1))
}

function print {
    new=`echo "$*" | tr ' ' '_'`
    setColWidth $new
    printf "%*s" $bef
    printf "%s" "$*"
    printf "%*s|" $aft
}


function runOnFile {
    time=0.00
    i=0
    while [ $i -lt $TIMES ]
    do
        # t=`(TIMEFORMAT="%E"; time $COMMAND $1 > /dev/null) |& tr -d '\n'`
        # time=`echo "format=0.0; $time+$t" | bc`
        ts=$(date +%s%N);
        o=`eval timeout $TIMEOUT $COMMAND $1 2>/dev/null`
        outcome=$?              # save exit code for late usage
        t=$((($(date +%s%N) - $ts)/1000000)) ;
        time=`echo "scale=2; $time+$t" | bc -l`


        print `echo $t | bc`

        i=$((i+1));
    done

    # print time
    avg=`echo "scale=1; $time/$TIMES" | bc -l`
    if [ `echo "$avg < 1" | bc` -eq 1 ]; then
        avg=0$avg;
    fi;

    print `echo $avg | bc`

    # print outcome
    if [ $outcome -eq 124 ]; then
        print "Timeout";
    else
        if [ $PRINTOUTCOME -eq 1 ]; then
            if [ $outcome -eq 0 ]; then
                print "Success: "$o;
            else
                print "Failure: "$o;
            fi;
        else
            print "$o"
        fi;
    fi;
}


function header {

    printf "| Example%*s |" $(($1-8))

    for i in $(seq 1 $TIMES); do
        print "$i [ms]"
    done;

    print "Avg"
    print "RetVal"
    echo                        # new line

    echo `printf "%*s" $(($1+13+$(($TIMES*$(($COLWIDTH+1)))))) | tr ' ' '-'`

}


# check whether user had supplied -h or --help . If yes display usage
for p in $@; do
    if [[ ( $p == "--help") ||  $p == "-h" ]]
    then
		    displayUsage
		    exit 0
    fi;
done;


if [ ! -e $PROGRAM ]; then
    echo "Fatal Error: File $COMMAND not found! Did you compile the program yet?";
    exit 1;
fi;


for fld in $FOLDERS; do
    FILES=`find $fld -maxdepth 1 -iname "*.trs" | sort`
    maxlen=0

    # get longest basename
    for f in $FILES; do
        bn=`basename "$f"`
        if [ `echo $bn | wc -c` -gt $maxlen ]; then
            maxlen=`echo $bn | wc -c`;
        fi;
    done;

    printf "%s\n------------------------------\n\n" "Folder: $fld"

    header $maxlen

    for f in $FILES; do
        # find out length of space to be added to filename
        bn=`basename "$f"`
        len=`echo $(basename "$f") | wc -c`
        len=$(($maxlen-len))

        # generate string to add to filename
        add=`printf "%*s" $len`

        # run command, print table
        printf "%s" "| $bn$add |";
        runOnFile $f;
        echo
    done;
    printf "\n\n"
done
