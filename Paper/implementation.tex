%%% implementation.tex ---
%%
%% Filename: implementation.tex
%% Description:
%% Author: Manuel Schneckenreither
%% Maintainer:
%% Created: Mi Mai 14 18:45:39 2014 (+0200)
%% Version:
%% Package-Requires: ()
%% Last-Updated: Mi Jun 18 23:52:54 2014 (+0200)
%% By: Manuel Schneckenreither
%% Update #: 194
%% URL:
%% Doc URL:
%% Keywords:
%% Compatibility:
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%% Commentary:
%%
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%% Change Log:
%%
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% This program is free software; you can redistribute it and/or
%% modify it under the terms of the GNU General Public License as
%% published by the Free Software Foundation; either version 3, or
%% (at your option) any later version.
%%
%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
%% General Public License for more details.
%%
%% You should have received a copy of the GNU General Public License
%% along with this program; see the file COPYING.  If not, write to
%% the Free Software Foundation, Inc., 51 Franklin Street, Fifth
%% Floor, Boston, MA 02110-1301, USA.
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%%% Code:

\chapter{Implementation}

This chapter is split up in three different sections. First, we introduce the
reader to common code of the two implemented variants. Then specific parts for
the amortized resource analysis are discussed, before looking at interesting
differences compared to the implementation of the polynomial interpretation
analysis.

\section{Common Implementation Characteristics}
\label{sec:Common_Implementation_characteristics}


First of all it needs to be mentioned that both variants were implemented using
the ``Haskell Programming Language''\begin{footnote}{For more information about
    the Haskell, see http://www.haskell.org/}\end{footnote}.


Both programs are command line programs taking two types of command line
parameters as input. These are non-mandatory options and a required input text
file containing the typed TRS to be analyzed. Therefore, the \textit{structure}
of the \textit{typed TRS data object} is almost identical for the two
variations. Furthermore, the \textit{parser} which parses the input file is
identical. Adding to this, the argument options are parsed in the same way for
both programs.

Besides of the same parser, as already mentioned, the \textit{linear problem
  solving code} is quite similar. Both programs have the possibility to switch
between a linear problem solver named ``lp\_solve'' and the SMT solver
``MiniSmt''. However, from an algorithmic point of view the similarities between
the two programs are very sparse.

% developed by Harald Zankl and Simon Legner at the
% University of Innsbruck, Austria.
% However, due to the completely different
% approaches of the computational portions of the software, these will be
% discussed separately.


\section{Computational Parts of Amortized Resource Analysis}
\label{sec:Implementation_of_Amortized_Resource_Analysis}

The implementation of the amortized resource analysis has to deal with several
challenges. It must create some kind of envelope which holds for each rewrite
rule the corresponding typing relation, its linear constraints and the current
cost variables. % Therefore, there is for every rewrite rule such a envelope,
% holding this information.
All rewrite rule have then be again packaged together
in another wrapper, which can distinguish between envelopes to be proven, and
already proved envelopes. Furthermore, this wrapper has to hold a list of global
variables, such that every variable name is unique.

To achieve this behavior, the program creates an envelope called \emph{Context},
which handles the needed information. A wrapper named \emph{Prove} then holds
several such \emph{Contexts} and a global counter for unique variable name picking.

Before being concerned with the inference rules the constraints have to be
initialized. Due to that reason each Context is initialized by creating the
well-typedness constraints for the costs as given in Formula~\ref{eq:welltyped}.

\subsection{Inference Rule Implementation}
\label{subsec:Inference_Rule_Implementation}

For the implementation of the inference rules we decided to integrate the
\textsf{relax} rule, as well as the \textsf{sup} and \textsf{\textsf{sub}} rules
into all the other inference rules, similar to~\citeauthor{Hoffmann11}
(\cite{Hoffmann11}).
% ,
% cf.~\ref{fig:2}
The \textsf{relax} rule was merged into the other inference rules by loosening
the strict bound of cost resources. This can be done by first applying the
\textsf{relax} rule before utilizing the desired inference rule. In
Figure~\ref{fig:example_id_merge} we gave an example for the merged \textsf{id}
and \textsf{relax} rule. The \textsf{relax} rule was applied first, yielding to
the constraint $p \geqslant 0$ by loosing the strict cost variables from $0$.

Likewise, the \textsf{sup} and \textsf{sub} rules were integrated in the other
inference rules, just not using the cost variables from the typing relation, but
the ones from the types. Due to the fact, that the rules are merged using the
$\geq$ relation, integrating one of the two rules leads to simultaneously
integrating the other one also. Note that the relation $<:$ was defined
by~\citeauthor{JLHSH09} in~\cite{JLHSH09} with respect to the potential,
therefore $A <: B$ means $A$ has bigger potential and with that also higher
costs than $B$.

Furthermore, the \textsf{share} rule is applied in such a manner that all
variables with the same name are renamed instantly, whereas in theory only two
variables are renamed for each application of the \textsf{share} rule. This
means the \textsf{share} rule can only be applied once in the implementation
yielding to unique variable names in the whole typing judgment. Therefore, if
multiple occurrences of variables are detected, the share rule is called at the
beginning of the whole solving procedure. After this first step it is ignored
when selecting the next rule.

\begin{figure}[t]
  \centering
  \begin{tabular}{l@{\hspace{10ex}}r}

    $\infer[\text{\textsf{id} (before)}]{\tjudge{\typed{x}{A}}{0}{}{\typed{x}{A}}}{}$
    % \\[3ex] %
    &

    $\infer[\text{\textsf{id}+\textsf{relax}}
    ]
    {
      \tjudge{\typed{x}{A}}
      {p}
      {}
      {\typed{x}{A}}
    }
    {
      p \geqslant 0 % & A_l <: A_r
    }
    $
    \\[2ex]
  \end{tabular}
  \caption{Left: The \textsf{id} rule as given in Figure~\ref{fig:2}.\newline Right: The
    \textsf{id} rule as implemented after merging with the \textsf{relax} rule. }
  \label{fig:example_id_merge}
\end{figure}


Moreover, all inference rule implementations check if the basic requirements are
being held, before trying to apply the rule characteristics to the given input
data. For example, if the identity rule (\textsf{id}-rule) gets called, it checks if there is
just one context item, as well as the type and number of terms, available in the
typing relation of the input data. If the input data does not hold the
requirements, the identity rule fails immediately. This decision was made to
ensure minimal overhead.

The strategy used to apply the inference rules is a try and error algorithm. As
input one typing relation is given to the function that applies the inference
rules. This function then applies all inference rules to the input, which
results in a list of typing relations with the length of all succeeded inference
rule executions. At this point, a function checks if the list can be reduced by
removing type relations with variable name or type constraints that do not hold.
Afterwards the program determines if one of the list items might already have
solved all inference derivation problems inside of the wrapper. If so, the
process gets stopped and the solution returned, otherwise the function which
applies the inference rules is called again, with the first element of the list.
In case the list gets empty, an exception is thrown telling the user that the
problem cannot be solved using these inference rules.

This procedure ensures that all possibilities are tried, before giving up.
However, it seems to be quite time-consuming to more or less randomly (a order
has been set at compile time) try out the inference rules until a solution is
found. Therefore, it was quite important to integrate as many inference rules
into each other to keep the number of possible branches low.

In case a solution for the inference rule derivation was found, the constrains
of the different typing relation  wrappers are extracted and combined to one
linear problem. This problem is then solved by the users choice between
``lp\_solve'' and ``MiniSmt''.

Finally the solution is, in case the linear problem was solvable, inserted
into the typed TRS and afterwards print on the command line.


\subsection{Example Input and Output of Amortized Resource Analysis}
\label{subsec:Example_Input_and_Output_of_Amortized_Resource_Analysis}


Here we give an example input file which defines the same typed TRS as given in
Listing~\ref{lst:example_typed_trs}. Listing~\ref{lst:example_output} shows the
corresponding output, when the amortized resource analysis is called
given a file containing the content of~\ref{lst:example_typed_trs} as input file
without any other command line parameters given. Note that the program allows to
set the constructor cost to a specific integer value using the notation $c:n$,
where $c$ is the constructor and $n$ the integer value.


\lstset{language=,numbers=none,caption={Example input file for typed TRS which
    is the same example as in Listing~\ref{lst:example_typed_trs}.}}
\begin{lstlisting}[frame=single, style=base, label={lst:example_input_file}]
  (VAR x y)

  (CONSTR
    -- This whole line is a comment!
    -- 0:0 sets the cost of constructor 0 to integer value 0
    Nat = µX.< 0:0, s(X) >
  )

  (SIG
    minus :: Nat x Nat -> Nat
    quot  :: Nat x Nat -> Nat
  )

  (RULES
    minus(x, 0)       -> x
    minus(s(x), s(y)) -> minus(x, y)

    quot(0, s(y))     -> 0
    quot(s(x), s(y))  -> s(quot(minus(x, y), s(y)))
  )
\end{lstlisting}

\newpage
\lstset{language=,numbers=none,caption={Result output when the amortized
    resource analysis program is given a file with the contents
    of Listing~\ref{lst:example_typed_trs}.}}
\begin{lstlisting}[frame=single, style=base-nocomments, label={lst:example_output}]
  Variables:
    x, y

  Constructors:
    NAT(ipvarc0) = µX.< 0:0, s(X):ipvarc0 >

  Parsed Rules:
    minus :: NAT(2) x NAT(0) o--1--> NAT(2)
    minus(x, 0) -> x
    minus(s(x), s(y)) -> minus(x, y)

    quot :: NAT(2) x NAT(0) o--1--> NAT(0)
    quot(0, s(y)) -> 0
    quot(s(x), s(y)) -> s(quot(minus(x, y), s(y)))

  --------------------------------------------------

  Complexity:

    (Info in this output omitted)

    O(minus) = 2⋅p0
    O(quot) = 2⋅p0

\end{lstlisting}

The output shows that the first parameter of quot is the one that affects the
runtime complexity. It does this with a factor of $2$. This means given an
input of $\mathsf{quot}(4,2) \defsym
\mathsf{quot}(\mathsf{s}(\mathsf{s}(\mathsf{s}(\mathsf{s}(\mathsf{0})))),
\mathsf{s}(\mathsf{s}(\mathsf{0})))$ like in
Listing~\ref{lst:example_derivation} the asymptotic bound for the runtime
complexity is $O(2 \cdot 4) = O(8)$.


\section{Computational Parts of Polynomial Interpretation Resource Analysis}
\label{sec:Implementation_of_Polynomial_Interpretation_Resource_Analysis}

The advantage of the polynomial interpretation analysis is that there are no
inference rules which when used in the proper sequence yield to a linear problem
but a straightforward method which directly converts the given rewrite rules to
a linear problem. However, it is important to note that the same well-typedness
constraints must be generated by the program to ensure that the right result is
produced. These steps are done in the following way.

First of all, a polynomial interpretation per rewrite rule is generated. This is
done by recursively interpreting the term $t$ with respect to the given function
signatures, constructors and variables variables. An example can be seen in
Figure~\ref{fig:example_interpretation_convertion}. % The term $t$ is recursively
% interpreted with respect to the given function signatures, constructors,
% variables, and the assignment $\alpha$.

% In
% Figure~\ref{fig:example_interpretation_convertion} $t = minus(s(x), s(y))$ is
% added with the type information that this function returns a type $\mathsf{Nat}(p_r)$.
% The signature $\mathcal{F}(minus) = \mathsf{Nat}(p_0) \times \mathsf{Nat}(p_1) \to \mathsf{Nat}(p_r)$ then
% allows to conclude that the subterms $s(x)$ and $s(y)$ have to be of the types
% $\mathsf{Nat}(p_0)$ respectively $\mathsf{Nat}(p_1)$. Adding to this the function minus has to be
% taken into account for the interpretation. This yields to a leaf named
% ``minus''. In the next step, the constructor $s(X)$ is applied to the two
% branches of the tree. Internally the constructor $s(X)$ is extended by adding a
% cost variable to $\mathsf{Nat}(k) = \mu X. \langle \ldots, s(X):k, \ldots \rangle$.
% Therefore, by interpreting the constructor $s(X)$ to the term $s(x) : \mathsf{Nat}(p_0)$
% the constructor will have a cost of $p_0$ since the variable $k = p_0$ for this
% specific case. Likewise, $s(y) :\mathsf{Nat}(p_1)$ will be interpreted.

Unfortunately, the recursive calls builds up a tree structure similar as in
Figure~\ref{fig:example_interpretation_convertion}. To get rid of this tree
structure a function flattens the interpretation to a polynomial. This results
in the so called polynomial interpretation of a term $t$.

% \begin{figure}[t]
%   \centering

%   \begin{tikzpicture}[->,>=stealth,shorten >=1pt,auto,node distance=2cm, main
%     node/.style={draw, minimum height=0.63cm }]

%     \node at (1, 0) [main node] (1) {\groundinter{minus(s(x), s(y)) : $Nat(p_r)$}};
%     \node at (-1, -2)[main node] (2) [] {\groundinter{s(x) : $Nat(p_0)$} + \groundinter{s(y) :
%       $Nat(p_1)$}};
%     \node at (3.5, -2)[main node] (5) [] {minus};

%     \node at (-3,-4)[main node] (3) [] {\groundinter{x : $Nat(p_0)$} + $p_0$};
%     \node at (3,-4)[main node] (4) [] {\groundinter{y : $Nat(p_1)$} + $p_1$};

%     \path[every node/.style={font=\sffamily\small}]
%     (1) edge node [left] {resolve function} (2)
%     edge node [right] {function interpretation} (5)
%     % edge [bend right] node[left] {0.3} (2)
%     % edge [loop above] node {0.1} (1)
%     (2) edge node [right] {constructor $s(X)$} (3)
%     edge node {} (4)
%     % edge [loop left] node {0.4} (2)
%     % edge [bend right] node[left] {0.1} (3)
%     % (3) edge node [right] {0.8} (2)
%     % edge [bend right] node[right] {0.2} (4)
%     % (4) edge node [left] {0.2} (3)
%     % edge [loop right] node {0.6} (4)
%     % edge [bend right] node[right] {0.2} (1);
%     ;
%   \end{tikzpicture}
%   \caption{This figure shows how a polynomial interpretation is recursively
%     created with respect to the given example in
%     Listing~\ref{lst:example_typed_trs}. In the first step the function minus
%     with function signature $\mathcal{F}(minus) = Nat(p_0) \times Nat(p_1) \to
%     Nat(p_r)$ gets applied. Then the recursive constructor $Nat(k) = \mu X.
%     \langle \ldots, s(X):k, \ldots \rangle$ is applied. All leaves summed up
%     $\groundinter{x : Nat(p_0)} + p_0 +
%     \groundinter{y : Nat(p_1)} + p_1 + minus$ represent the interpretation of the root term.}
%   \label{fig:example_interpretation_convertion}

% \end{figure}

\begin{figure}[ht]
  \centering

  \begin{equation*}
    \groundinter{\mathsf{minus}(\mathsf{s}(x), \mathsf{s}(y))} \overset{!}{\geqslant} \groundinter{\mathsf{minus}(x, y)}
  \end{equation*}
  \begin{equation*}
    \Longleftrightarrow
  \end{equation*}
  \begin{equation*}
    \groundinter{\typed{x}{\mathsf{Nat}(p_0)}} + \groundinter{\typed{y}{\mathsf{Nat}(p_1)}} + p_0 + p_1 + minus \overset{!}{\geqslant}
  \end{equation*}
  % \begin{equation*}
  %   \overset{!}{\geqslant}
  % \end{equation*}
  \begin{equation*}
    \groundinter{\typed{x}{\mathsf{Nat}(p_0)}} + \groundinter{\typed{y}{\mathsf{Nat}(p_1)}} + p_0 + p_1
  \end{equation*}
  \vspace{-0.2cm}
  \begin{equation*}
    \Longrightarrow~\text{Equations }\ref{eq:ri1} \text{ and}~\ref{eq:ri2}
  \end{equation*}
  \hrule
  \begin{equation}
    \label{eq:ri1}
      \begin{array}{l l l}
        p_0 + p_1 + minus & \overset{!}{>} & p_0 + p_1 \\
        \Rightarrow minus & \overset{!}{>} & 0 \\
      \end{array}
  \end{equation}

  \begin{equation}
    \label{eq:ri2}
      \begin{array}{l l l l}
        \groundinter{x : \mathsf{Nat}(p_0)} & \overset{!}{=} & \groundinter{x : \mathsf{Nat}(p_0)} & \wedge \\\
        \groundinter{y : \mathsf{Nat}(p_1)} & \overset{!}{=} & \groundinter{y : \mathsf{Nat}(p_1)} &
      \end{array}
  \end{equation}

  \caption{Example constraints from polynomial interpretation of the rewrite
    rule $\mathsf{minus}(\mathsf{s}(x), \mathsf{s}(y)) \to \mathsf{minus}(x,y)$. The variable interpretations with
    type information attached are looked at separately.}
  \label{fig:resolve_interpretation}
\end{figure}

From that point on, each rewrite rule $l \to r$ is individually considered. The
polynomial interpretation of the left hand side $l$, e.g.
$\groundinter{\typed{x}{\mathsf{Nat}(p_0)}} +
\groundinter{\typed{y}{\mathsf{Nat}(p_1)}} + p_0 + p_1 + minus$ in
Figure~\ref{fig:example_interpretation_convertion}, is compared to the
corresponding right hand side interpretation $\groundinter{r}^\gamma$ which is
$\groundinter{\typed{x}{\mathsf{Nat}(p_0)}} +
\groundinter{\typed{y}{\mathsf{Nat}(p_1)}} + minus$ for this example. Similarly
to~\cite{sat} the variables with type information are compared separately and on
the \textit{greater-or-equal} relation $\geqslant$ in comparison to the rest of
the polynomial interpretations which are compared using the $>$ relation, cf.\
Figure~\ref{fig:resolve_interpretation}. If the types and variable labels
coincide, like in Equation~\ref{eq:ri2}, the cost variables form a constraint.
Moreover, the all parts together without type declarations build one
constraint, % where $\groundinter{l} > \groundinter{r}$,
see Equation~\ref{eq:ri1}.

These constraints are completed by adding the well-typedness constraints as
given in Formula~\ref{eq:welltyped}, this includes constraints from each rewrite
rule that combine the input parameter for each function from the right hand side
with the return values of the called child-functions.

The resulting linear problem is solved with the same manner as in the amortized
resource analysis tool. For comparison reasons the output is also inserted to
the internal typed TRS and printed on the command line. The output when a file
with the contents of Listing~\ref{lst:example_typed_trs} is given to the program
can be seen in Listing~\ref{lst:example_output_pi}.

In comparison to the amortized resource analysis result a section for displaying
the polynomial interpretation for each function symbol was added. This input
data yields in both variants to exactly the same asymptotic runtime
complexities.


\newpage
\lstset{language=,numbers=none,caption={Result output when the polynomial
    interpretation resource analysis program is given a file with the contents
    of Listing~\ref{lst:example_typed_trs}.}}
\begin{lstlisting}[frame=single, style=base-nocomments, label={lst:example_output_pi}]
  Polynomial Interpretations (pX represtens the X'th parameter)

    [0] = 0
    [s] = [p0 : Nat] + ipvar_s
    [minus] = [p0 : Nat] + [p1 : Nat] + 1
    [quot] = [p0 : Nat] + [p1 : Nat] + 1
  --------------------------------------------------

  Analyzed Rules:

  Variables:
    x, y

  Constructors:
    Nat(ipvar_s) = µX.< 0:0, s(X):ipvar_s >

  Parsed Rules:
    minus :: Nat(2) x Nat(0) o--1-->  Nat(2)
    minus(x, 0) -> x
    minus(s(x), s(y)) -> minus(x, y)

    quot :: Nat(2) x Nat(0) o--1-->  Nat(0)
    quot(0, s(y)) -> 0
    quot(s(x), s(y)) -> s(quot(minus(x, y), s(y)))

  --------------------------------------------------

  Complexity:

    (Info in this output omitted)

    O(minus) = 2⋅p0
    O(quot) = 2⋅p0

\end{lstlisting}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% implementation.tex ends here
