{-# LANGUAGE CPP #-}
-- lpSolve.hs ---
--
-- Filename: lpSolve.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Sa Dez 28 18:18:34 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Mai 27 15:01:41 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 316
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module is designed to solve a list of constraints using
-- the external linera program solver named LP Solve.
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module LinearProblem.LPSolve
    ( solveConstraintsLP
    ) where

-- min:;\n" ++ showListWithSep show constraints ";\n"

import Types.TypeConstraint
import Types.TypeTypedTRS

import Misc.HelperFunctions
import Exceptions.ExceptionHandler


import qualified System.Process as Cmd
import System.IO
import qualified Data.Map.Strict as Map
import Text.ParserCombinators.Parsec
import Control.Exception (throw)
import Control.Monad (unless, void)

binary :: String
binary = "lp_solve"


solveConstraintsLP :: LPProblem -> FilePath -> Bool -> IO (Map.Map String Int)
solveConstraintsLP p tempDir keepFiles = do
  -- write to tmp file
  let numCostVars = length $ concatMap getCConstructors $ (getConstructors . getPTRS) p
      vars0 = map (\x -> "ipvar" ++ show x) [0..(getPVarNr p - 1)]
      vars1 = map (\x -> "ipvarcost" ++ show x) [0..(numCostVars - 1)]
      varList = showListWithSep id (vars0 ++ vars1) ", "


  (pName, pHandle) <- openTempFile tempDir "lpP"
  (sName, sHandle) <- openTempFile tempDir "lpS"
  hPutStrLn pHandle ("min: " ++ showListWithSep id (vars0 ++ vars1) " + " ++
                                 ";\n" ++ showListWithSep show (getPConstraints p) ";\n"
                                   ++ ";\n\n" ++ "int " ++ varList
                                   ++ ";" -- declaration ensures int as result0
                    )
  hClose pHandle
  hClose sHandle

  -- execute lp_solve
  Cmd.system $ binary ++ " " ++ pName ++ " > " ++ sName

  -- read and parse from tmp file
  solStr <- readFile sName


  let solution = case parse lpSolveResult sName solStr of
                   Left err -> throw $
                               FatalException ("lp_solve failed! Message:\n" ++ show err)
                   Right x -> x

  unless keepFiles $
       void (Cmd.system ("rm " ++ pName ++ " " ++ sName))


  let list = Map.fromList (map (\x -> ("ipvar"++ show x, 0)) [0..(getPVarNr p)])


  return $ insertSolution list solution

      where
        insertSolution :: Map.Map String Int -> [(String, Int)] -> Map.Map String Int
        insertSolution m []            = m
        insertSolution m ((k, v):rest) = insertSolution (Map.insert k v m) rest


lpSolveResult :: Parser [(String, Int)]
lpSolveResult = unsolveable
                <|> variables
                <|> do
                  _ <- skipLine
                  lpSolveResult


skipLine :: Parser ()
skipLine = do
    _ <- newline
    return ()
  <|> do
    _<- manyTill anyChar newline
    return ()

unsolveable :: Parser a
unsolveable = do
  _ <- string "This problem is infeasible"
  throw $ FatalException "The linear problem is infeasible."


variables :: Parser [(String, Int)]
variables = do
  _ <- string "Actual values of the variables:"
  many (spaces >> varMap)


varMap :: Parser (String, Int)
varMap = do

  n <- name
  _ <- spaces
  v <- int
  _ <- spaces
  return (n, v)


name :: Parser String
name = do
  l <- letter
  r <- many alphaNum
  return $ l:r


int :: Parser Int
int = do
  ds <- many digit
  return (read ds :: Int)

--
-- lpSolve.hs ends here
