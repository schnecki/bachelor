{-# LANGUAGE CPP #-}
-- ConstraintsSolver.hs ---
--
-- Filename: ConstraintsSolver.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 19:32:08 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mi Mai  7 18:52:51 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 59
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module LinearProblem.ConstraintsSolver
    ( solveConstraints
    ) where


import Types.TypeConstraint
import Types.TypeArgumentOptions
import LinearProblem.LPSolve
import SMTSolving.SMTSolver

import qualified Data.Map.Strict as Map

#ifdef DEBUG
import Debug.Trace (trace)
#endif


solveConstraints :: LPProblem -> ArgumentOptions -> IO (Map.Map String Int)
solveConstraints p opt =
    if getSMTSolver opt
    then solveInterpreationSMT opt sExpr
    else solveConstraintsLP p tempDir keepFiles -- use LP Solve

    where
      tempDir = getTempDir opt
      keepFiles = getKeepFiles opt
      sExpr = map constraintToSExpression (getPConstraints p)


--
-- ConstraintsSolver.hs ends here
