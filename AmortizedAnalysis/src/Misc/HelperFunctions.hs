-- HelperFunctions.hs ---
--
-- Filename: HelperFunctions.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 17:56:00 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mo Mär 17 00:00:51 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 23
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Misc.HelperFunctions where

-----------------------------------------------------------------------------
-- HELPER FUNCTIONS

-- | This function shows a list and appends a serperator in the middle
-- of two elements. There will be no seperator before the first element and
-- after the last element.
showListWithSep :: Show a => (a -> String) -> [a] -> String -> String
showListWithSep _ [] _       = []
showListWithSep f (x:[]) _   = f x
showListWithSep f (x:xs) sep = f x ++ sep ++ showListWithSep f xs sep


--
-- HelperFunctions.hs ends here
