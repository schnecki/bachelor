{-# LANGUAGE CPP #-}
-- Parser.hs ---
--
-- Filename: Parser.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 20:44:58 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:37:08 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 1669
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module is the parser. It is able to open, get the contents and
-- parse a file.
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Parsing.TRSParser
    ( parseTRSFromFile
    ) where

import Misc.HelperFunctions
import Exceptions.ExceptionHandler

import Types.TypeDatatype
import Types.TypeGeneral
import Types.TypeTypedTRS
import Types.TypeArgumentOptions

import qualified Control.Exception as E
import Control.Exception.Base (throw, SomeException)
import Control.Monad (when, unless)
import Text.ParserCombinators.Parsec
import Data.Char (toUpper, isNumber)
import Data.List
import Data.Maybe (isJust, fromMaybe)
#ifdef DEBUG
import Debug.Trace
#endif

parseTRSFromFile ::  ArgumentOptions -> IO (Either ParseError TypedTRS)
parseTRSFromFile args = E.catch
                              (parseFromFile trsInputParser (getFilePath args))
                              (\e -> throw  $ ParseException $ show (e :: SomeException))

comments :: Parser Char
comments = do
  _ <- string "--" -- <|> string "#"
  _ <- manyTill anyChar newline
  return 'c'


-- | This function parses a simpleTRS. First the variables (VAR part), then the
--   constructors part (CONSTR ...) and afterwards
--   rewrite rules withouth the relations (the SIG part) and finally the
--   relations (the RULES part). After all these inputs, there needs to be the
--   eof.
trsInputParser :: Parser TypedTRS
trsInputParser = do
  _ <- skipMany (space <|> comments)
  v <- parens variables
  _ <- skipMany (space <|> comments)
  c <- parens constructorTypes
  when (multipleDatatypes (map getCDatatype c))
       (fail $ "Multiple definition of same datatype! This error occurs in the " ++
        " in the CONSTR section. Datatypes are case INSENSITIVE e.g. (Nat == NAT)!")
  unless (all (checkLenOfChildCtr c) (concatMap getCConstructors c))
             (fail "Not reachable. Exception will be thrown before!")
  let ctrNames = map (\x -> case getCStatement x of
                              Function f _ -> f
                              Atom iv -> getIVVariable iv)
                 (concatMap getCConstructors c)
      ctrs = emptyConstr (map getCStatement (concatMap getCConstructors c))
  _ <- skipMany (space <|> comments)
  rr <- parens $ rewriteRules ctrNames
  _ <- skipMany (space <|> comments)
  r <- parens $ relations  -- call with variables (incl. constructors with no parameters)
       (v ++ ctrs)
       (rr ++ getConstrFuncs c)  -- rewrite rules and construtors
  unless (all (checkLeftLinearity v) $ map getLeft r)
             (fail $ "There are non-left linear rewrite rules given! " ++
                   "The theory does not support non-left linear rewrite rules. See the paper!")
#ifndef DEBUG
  _ <- skipMany (space <|> comments)
  _ <- eof
#endif
  return $ TypedTRS v c (bindFunctionsToRules r rr)
      where
        -- this function checks if a element occurs multiple times in a list
        multipleDatatypes []     = False
        multipleDatatypes (c:cs) = (c `elem` cs) || multipleDatatypes cs

        -- This function checks a given statement, if each variable occurs
        -- only once.
        checkLeftLinearity        :: [InternalVariable] -> Statement -> Bool
        checkLeftLinearity v stmt = all ((== 1) . length)
                                       (groupBy groupFun $ sortBy sortFun $ allVars stmt)
            where
              allVars :: Statement -> [InternalVariable]
              allVars (Atom x)        = if x `elem` v then [x] else []
              allVars (Function _ ch) = concatMap allVars ch

              sortFun a b = compare (getIVVariable a) (getIVVariable b)
              groupFun a b = getIVVariable a == getIVVariable b


        emptyConstr          :: [Statement] -> [InternalVariable]
        emptyConstr []       = []
        emptyConstr (Function n [] : rest ) =
                             InternalVariable n NIL [] : emptyConstr rest
        emptyConstr (_:rest) = emptyConstr rest

        checkLenOfChildCtr   :: [ConstructorType] -> Constructor -> Bool
        checkLenOfChildCtr c = stmtCheck . getCStatement
            where
              stmtCheck                 :: Statement -> Bool
              stmtCheck (Atom _)        = True
              stmtCheck (Function n ch) = all (checkCosts n) ch

              checkCosts                :: String -> Statement ->  Bool
              checkCosts _ (Function _ _) = throw $ FatalException
                                          "Nested constructors are not allowed!"
              checkCosts n (Atom iv)      =
                  (getIVDatatype iv == NIL) || (getIVVariable iv == "X") ||
                  (length (getIVCosts iv) <= lengthCtrTypeVars (getIVDatatype iv)) ||
                  throw (FatalException $ "Length of cost variables of " ++
                            show (getIVDatatype iv) ++ " in constructor " ++ n ++
                           " is longer than the datatype accepts.")


              lengthCtrTypeVars   :: Datatype -> Int
              lengthCtrTypeVars dt = case findCtrTypeByNameInList dt of
                                      Nothing -> throw $ FatalException $ "Could not find " ++
                                                 "constructors for datatype " ++ show dt ++ "."
                                      Just x -> length $
                                                filter (\x -> case getCCost x of
                                                                CostInt _ -> False
                                                                _ -> True
                                                       ) (getCConstructors x)

              findCtrTypeByNameInList    :: Datatype -> Maybe ConstructorType
              findCtrTypeByNameInList dt = find
                                          (\x -> getCDatatype x == dt) c


-- | This function parses a parser inside of parenthesis.
parens   :: Parser a -> Parser a
parens p = do
  _ <- char '('
  _ <- skipMany (space <|> comments)
  res <- p
  _ <- skipMany (space <|> comments)
  _ <- char ')'
  return res


-- | This parsers parses the (VAR x y...) section of the input file.
--   It retunrs the all the variables in a list.
variables :: Parser [InternalVariable]
variables = do
  _ <- string "VAR" <?> "the string 'VAR'"
  many (many1 space >> variable)


-- | This functions parsers one variable. It requires a space and then a
-- variable name.
variable :: Parser InternalVariable
variable = do
  n <- name
  return $ InternalVariable n NIL []

-- | This functions parsers one variable or function as a string. For
-- some reason this parse cannot parse the colon ':'?
-- name :: Parser String
-- name = do
--   l <- letter <|> -- noneOf "() "
--                    oneOf "#@"
--   r <- try (string "::") <|> many (alphaNum <|> oneOf "\'#@"
--             ) <?> "the rest of the variable"
--   when (startsWith (l:r) "ipvar")
--            (throw $ FatalException $
--             "Strings starting with 'ipvar' are reserved for the system!" ++
--             " Use something else...")
--   return (l:r)
--        <?> "a variable or function name"
name :: Parser String
name = do
  s <- string "::" <|> many (alphaNum <|> oneOf "\'#@")
  when (startsWith s "ipvar")
        (throw $ FatalException $
         "Strings starting with 'ipvar' are reserved for the system!" ++
         " Use something else...")
  return s


startsWith :: String -> String -> Bool
startsWith _ [] = True
startsWith [] _            = False
startsWith (h1:tl) (h2:ts) = h1 == h2 && startsWith tl ts


-- | This parses the beginning of the constructor part and then
--   many constructors
constructorTypes :: Parser [ConstructorType]
constructorTypes = do
  _ <- string "CONSTR" <?> "the string 'CONSTR'"
  _ <- skipMany (space <|> comments)
  ctr <- many constructorType

  -- check constructors for declared datatypes
  unless (null $  checkConstructors ctr (listOfDatatypes ctr))
           (fail "Checking constructors failed! Programming Error...Sry :(")

  return ctr
      where
        listOfDatatypes :: [ConstructorType] -> [Datatype]
        listOfDatatypes = map getCDatatype

        checkConstructors          :: [ConstructorType] -> [Datatype] -> [ConstructorType]
        checkConstructors [] _     = []
        checkConstructors (c:cs) d =
            case withoutXs allChds  of
              [] -> checkConstructors cs d
              rest -> concatMap (\x -> case find (== getIVDatatype x) d of
                                         Nothing -> throw $ ParseException $
                                                    "Datatype " ++ getIVVariable x ++
                                                                    " not declared! "
                                         _ -> []
                                ) rest

            where
              allChds :: [InternalVariable]
              allChds = concatMap listOfChilds (map getCStatement (getCConstructors c))

              withoutXs :: [InternalVariable] -> [InternalVariable]
              withoutXs = filter (\x -> getIVVariable x /= "X")


              listOfChilds                 :: Statement -> [InternalVariable]
              listOfChilds (Function _ ch) = concatMap listOfChilds ch
              listOfChilds (Atom x)        = [x]


-- | This function parses a constructor. Each is sparated by a new
--   line. It takes as input parameter a list of STring which cannot
--   be parsed as datatypes.

constructorType :: Parser ConstructorType
constructorType = do
  d <- try(do
            _ <- skipMany (space <|> comments)
            datatype)
  _ <- skipMany space
  p <- try (parens parameters) <|>
       try (parens (skipMany space >> return []) <|> return [])
  _ <- skipMany space
  _ <- char '='
  _ <- skipMany space
  i <- constrInputParams -- returns [] for non recursive constructors, else ["X"]
  c <- constructors p i  <?> "a constructor"
  return $ ConstructorType d (map (CostVar . getIVVariable) p) c (not (null i))


-- | This function parses all constructos within '<' and '>'
constructors :: [InternalVariable] -> [InternalVariable] -> Parser [Constructor]
constructors p v = do
  _ <- char '<'
  _ <- skipMany (space <|> comments)
  c <- constructorList p v
  _ <- skipMany (space <|> comments)
  _ <- char '>'
  return c

-- | This parser function parses a list of constructor values, separated by a
--   comma (,).
constructorList :: [InternalVariable] -> [InternalVariable] -> Parser [Constructor]
constructorList p v =
  try (do
        x <- constructor p v             <?> "a constructor"
        notFollowedBy (skipMany space >> char ',')
        return [x]
      )
  <|> try (do
        x <- constructor p v
        _ <- skipMany (space <|> comments)
        _ <- char ','
        xs <- constructorList p v
        return $ x:xs
      )


-- | This function parses a constructor.
constructor   :: [InternalVariable] -> [InternalVariable] -> Parser Constructor
constructor p v = do
  _ <- skipMany (space <|> comments)

  s <- try number <|>
       try (function v [] False) <|>
       try dataTypeCtr <|>
       try (do
             n <- name
             return $ Function n [])

  -- check for nested constructors
  when (isNestedConstructor s) (fail "Nested constructors are not allowed!")

  c <- try (do
             _ <- char ':'
             cost p <?> (" a cost, which is an Integer, or one of the " ++
                         "following variables: {" ++
                         showListWithSep show p ", " ++ "}")
           )
       <|> return CostEmpty -- will be asigned when creating the contexts

  return $ Constructor s c
    where
      isNestedConstructor (Atom _)            = False
      isNestedConstructor (Function _ [] )    = False
      isNestedConstructor (Function n (c:cs)) =
          case c of
            Function _ _ -> True
            _            -> isNestedConstructor (Function n cs)


-- | This function parses datatypes as constructor parameters.
dataTypeCtr :: Parser Statement
dataTypeCtr = do
  n <- name <?> "a constructor name"
  _ <- skipMany space
  fc <- parens ctrTypeChd
  return $  Function n
             (map (\x ->  Atom $ InternalVariable
                          (getDatatypeString (fst x)) (fst x) (snd x)) fc)


-- | This function parses the constructor parameters.
ctrTypeChd :: Parser [(Datatype, [Cost])]
ctrTypeChd = do
  _ <- skipMany space
  p `sepBy` sep <?> "datatypes as function body"

    where
      sep = skipMany space >>  char ',' >> skipMany space
      p = do
        d <- datatype
        c <- try (parens ((cost [] <|>
                          (spaces >> return CostEmpty)
                          ) `sepBy` sep)
                 )
             <|> return []
        if getDatatypeString d == "X" && not (null c)
        then throw $ ParseException
          "X stands for a recursive datatype. You cannot set the costs for it."
        else return (d,c)


-- | This function parses the costs of a constructor.
cost   :: [InternalVariable] -> Parser Cost
cost v = do
  s <- try (do
             vv <- variable
             unless (vv `elem` v) (fail $ "Constructor variable " ++ show vv ++ " not declared.")
             return $ Atom vv
           )
       <|> try (do
                 ns <- many1 digit <?> "a number"
                 let n = read ns :: Int
                 return $ Function (show n) [])

  case s of
    Atom x        -> return $ CostVar $ getIVVariable x
    Function n [] -> return $ CostInt (read n)
    _             -> fail $
                     "Could not determine costs. Must be a variable or an integer. "
                     ++ show s


-- | This function parses the input parameters for the constructors, either
--   starting with a 'µ' or just nothing, for constructors and types without
--   the need of variables.
constrInputParams :: Parser [InternalVariable]
constrInputParams =
    try (do
         _ <- string "µ" <|> string "u" <|> string "mu"
         _<- spaces
         l <- char 'X'
         _<- spaces
         _ <- char '.'
         _<- spaces
         return [InternalVariable [l] NIL []]
        )
    <|>
    return []


-- | This parser gets a list of statements.
parameters :: Parser [InternalVariable]
parameters =
    try (do
         _ <- skipMany (space <|> comments)
         s <- name <?> "a constructor variable"
         notFollowedBy (skipMany (space <|> comments) >> char ',')
         return [InternalVariable s NIL []]
        )
    <|>
    try (do
         _ <- skipMany (space <|> comments)
         s <- name
         _ <- skipMany (space <|> comments)
         _ <- char ','
         ss <- parameters
         return $ InternalVariable s NIL [] : ss
        )


-- | This function parses the SIG string and the following rewrite
--   rules. It takes as input a list of strings which contain names
--   on which the parser will fail.
rewriteRules :: [String] -> Parser [RewriteRule]
rewriteRules strs = do
  _ <- string "SIG" <?> "the string 'SIG'"
  _ <- space
  many1 $ rewriteRule strs


-- | This function parses a Signature of a rewrite rule. This includes
--   the function name, the "::" and the type signature itself. It
--   takes as input parameter names which are not allowed to be
--   parsed.
rewriteRule :: [String] -> Parser RewriteRule
rewriteRule strs =
  try (do _ <- skipMany (space <|> comments)
          n <- name
          when (isJust (find (n ==) strs))
                (fail $ "Error: Same name " ++ n ++ " for rewrite rule and constructor!")
          _ <- skipMany space
          _ <- string "::"
          _ <- skipMany space
          par <- try ( do
                       p <- many sigtype
                       _ <- skipMany space
                       _ <- string "->"
                       _ <- skipMany space
                       return p ) <|> return []
          ret <- typedDatatype
          return $ RewriteRule n (Signature par ret CostEmpty CostEmpty) []
       )
       <?> "a rewrite rule name incl. signature (e.g. minus :: Nat x Nat -> Nat)"


-- | This function parses a list of datatypes. It is recursive, with the base of
--   one datatype and the induction step being a datatype, the separtor 'x' and
--   another typelist.
typeList :: Parser [InternalVariable]
typeList = try ( do
                 d <- typedDatatype
                 _ <- skipMany (space <|> comments)
                 _ <- char 'x'
                 _ <- skipMany (space <|> comments)
                 r <- typeList
                 return $ d:r
               )
           <|> do
             d <- typedDatatype
             return [d]

-- | This function parses a list of datatypes. It is recursive, with the base of
--   one datatype and the induction step being a datatype, the separtor 'x' and
--   another typelist.
sigtype :: Parser InternalVariable
sigtype = try ( do
                 d <- typedDatatype
                 _ <- skipMany (space <|> comments)
                 _ <- char 'x'
                 _ <- skipMany (space <|> comments)
                 return $ d
               )
           <|> do
             d <- typedDatatype
             return d


-- | This function parses a datatyped and puts it in the TypedDatatype wrapper
--   with an empty InterVariable name a cost of -1
typedDatatype :: Parser InternalVariable
typedDatatype = do
  d <- datatype
  return  $ InternalVariable "" d []


-- | This function converts a sting to a Datatype using read.
datatype :: Parser Datatype
datatype = do
  lw <- many1 letter
  let dt = Datatype $ map toUpper lw

  if dt == NIL
  then fail "Datatype 'NIL' cannot be declared!"
  else return dt


-- | This function parses the Rules section. Starting with the
--   string RULES and then followed by rules.
relations :: [InternalVariable] -> [RewriteRule] -> Parser [Relation]
relations v rr = do
  _ <- string "RULES" <?> "the string 'RULES'"
  _ <- space
  many $ relation v rr


-- | This function parses a Relation. This includes the left statement
-- the arrow and also the right statement.
relation :: [InternalVariable] -> [RewriteRule] -> Parser Relation
relation v rr =
  try (do
        _ <- skipMany (space <|> comments)
        l <- try (function v rr True) <|>
             try (do
                   n <- name
                   _ <- char '(' >> many (char ' ') >> char ')'
                   return $ Function n []
                 )
                  <?> "a function (left statement)"
        _ <- skipMany space
        _ <- string "->"
        _ <- skipMany space
        r <- try (statement v rr) <|>
             try (do
                   n <- name
                   _ <- char '(' >> many (char ' ') >> char ')'
                   return $ Function n []
                 )
                  <?> "right statement"
        return $ Relation l r)
       <?> "a rule (e.g. minus(x, 0) -> x)"


-- | This parses parses an integer number and returns a Statement.
number :: Parser Statement
number = do
  ns <- many1 digit <?> "a number"
  let n = read ns :: Int
  -- return $ convertNumber n           -- uncomment to convert numbers
  return $ Function (show n) []


-- | This parses parses a function. If the Boolean flag is set to true
--   it will check the list of rewrite rules for the function names.
--   Otherwise it don't.
function :: [InternalVariable] -> [RewriteRule] -> Bool -> Parser Statement
function v rr c = do
  n <- name <?> "a function name"
  unless (not c || n `elem` ns rr)
             (fail $ "Function signature of '" ++ n ++ "' not declared.")
  _ <- skipMany space
  f <- parens (functionChilds v rr) <?> "a function body"
  when (c && not (null f) && length f /= (length . getParTypes) (getSignature $ getRR n rr))
       (throw $ FatalException $
        "Error when checking for children length of given rewrite rule " ++ n)
  return $  Function n f
      where
        ns :: [RewriteRule] -> [String]
        ns = foldl names []
        names :: [String] -> RewriteRule -> [String]
        names acc r = getName r : acc

        getRR       :: String -> [RewriteRule] -> RewriteRule
        getRR n rrs = fromMaybe
                      (throw $ FatalException $
                       "Could not find rewrite rule or constructor named " ++ n ++ "!")
                      (find (\x -> getName x == n) rrs)

-- | This parsers parses a Statement. This can be a number (is a
-- function), a function or a variable.
statement :: [InternalVariable] -> [RewriteRule] -> Parser Statement
statement v rr =
            try number
            <|>
            try (
                 do
                  lw <- name <?> "a variable"
                  notFollowedBy (many space >> char '(')
                  if InternalVariable lw NIL [] `elem` v then
                      return $ Atom $ InternalVariable lw NIL []
                  else fail $ "No variable with name " ++ lw ++ " declared, must be some of: "
                       ++ (showListWithSep show v ", ")
                 <?> "a declared variable")
            <|>
              (try (function v rr True) <?> "a function")


-- | This parser gets a list of statements. Input parameter is the list
-- of declared variables.
functionChilds :: [InternalVariable] -> [RewriteRule] -> Parser [Statement]
functionChilds v rr =
    try (do
         _ <- skipMany space
         s <- statement v rr <?> "a statement"
         notFollowedBy (skipMany space >> char ',')
         return [s]
        )
    <|>
    try (do
         _ <- skipMany space
         s <- statement v rr
         _ <- skipMany space
         _ <- char ','
         ss <- functionChilds v rr
         return $ s:ss
        )


-----------------------------------------------------------------------------
-- HELPER FUNCTIONS FOR THIS MODUL

-- | This function binds the relations to the appropriate rewrite rules.
-- It takes a list of Relations and a list of rewrite rules (with empty
-- relation parts) and returns the rewrite rules with relations.
bindFunctionsToRules :: [Relation] -> [RewriteRule] -> [RewriteRule]
bindFunctionsToRules [] rr     = rr
bindFunctionsToRules (f:fs) rr = bindFunctionsToRules fs (createRels rr f)
    where createRels []  _   = []
          createRels (x:xs) g = if getName x == getFunName (getLeft g)
                              then (x {getRelations = getRelations x ++ [g] } )
                                       : createRels xs g
                              else x : createRels xs g


-- | This function converts numbers to the representingfunctions.
-- E.g. 4 will be converted to s(s(s(s(0())))), where 0() is a function
-- itself (arity 0).
convertNumber :: Int -> Statement
convertNumber = convertNumber'
    where convertNumber' 0 = Function "0" []
          convertNumber' x = Function "s" [convertNumber' (x-1)]


-- | This function actually just creates dummy RewriteRules from the
--   Constructor Functions. These are needed
getConstrFuncs   :: [ConstructorType] -> [RewriteRule]
getConstrFuncs ct
    = foldl fun [] (concatMap (\ct' -> map (\x -> (x, getCDatatype ct')) $ getCConstructors ct') ct)
      where fun                       :: [RewriteRule] -> (Constructor, Datatype) -> [RewriteRule]
            fun acc (Constructor c _, dt) =
                case c of
                  Function n ch -> RewriteRule n (createSignature ch) [] : acc
                  _ -> acc
                where
                  createSignature      :: [Statement] -> Signature
                  createSignature chld = Signature par ret CostEmpty CostEmpty

                      where
                        ret = InternalVariable (show dt) dt (getCostsOfCtr dt)
                        par = foldl getParTypes [] chld

                  getParTypes                  :: [InternalVariable] -> Statement -> [InternalVariable]
                  getParTypes _ (Function _ _) = throw $ FatalException
                                               "Nested constructors are not allowed!"
                  getParTypes acc (Atom x)     = acc ++ [
                                                         (if getIVVariable x == "X"
                                                          then InternalVariable (show dt) dt (getCostsOfCtr dt)
                                                          else InternalVariable (getIVVariable x) (getIVDatatype x)
                                                                   (getCostsOfCtr (getIVDatatype x))
                                                        )]
                  getCostsOfCtr    :: Datatype -> [Cost]
                  getCostsOfCtr dt = createCosts $ fromMaybe
                                     (throw $ FatalException "not possbile")
                                     (find (\x -> getCDatatype x == dt) ct)

                  createCosts       :: ConstructorType -> [Cost]
                  createCosts ctype = (filter (\x -> case x of
                                                       CostInt _ -> False
                                                       _ -> True
                                                 )) (map getCCost $ getCConstructors ctype)


--
-- Parser.hs ends here
