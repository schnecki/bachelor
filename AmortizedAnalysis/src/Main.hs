{-# LANGUAGE CPP #-}
-- Main.hs ---
--
-- Filename: Main.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 19:49:03 2013 (+0100)
-- Version: 1.0
-- Package-Requires: ()
-- Last-Updated: Do Mai 29 19:30:13 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 1361
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This is the main starting point of the application.
-- See README and Documentation/... for more information on
-- how to use the program.

-- Usage help text:
--     $ ./progName -h

-- GHCI argument passing:
--     *Main> :main -h -v filePath.trs
--
-- or with set, e.g. when used :trace to DEBUG:
--     *Main> :set args ../Examples/list.trs --debug -v
--     *Main> :trace main
--


-- To enabl/disable DEBUG output:
-- compile/'start ghci' with -DDEBUG and run with --debug
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Main
    ( main )
    where


import Types.TypeDatatype
import Types.TypeTypedTRS
import Types.TypeArgumentOptions
import Types.TypeGeneral
import Types.TypeConstraint
import InferenceRules.Analyzer
import Parsing.TRSParser (parseTRSFromFile)
import Misc.HelperFunctions
import Parsing.Arguments.ArgumentOptionsParser
import Exceptions.ExceptionHandler
import LinearProblem.ConstraintsSolver

import qualified Control.Exception as E
import Data.List (find)
import Data.Maybe (fromMaybe)
import Text.ParserCombinators.Parsec (ParseError)
import Control.Monad (when)
import System.Exit
#ifdef DEBUG
import Debug.Trace (trace)
#endif

import qualified Data.Map.Strict as M


-- | This is the starting point of the program. It coordinates the
-- program flow.
main :: IO ()
main =
    E.catch
         (do                    -- Read arguments
           maybeArgs <- parseArgOpts :: IO (Maybe ArgumentOptions)
           let args = fromMaybe (E.throw $ FatalException "args where empty") maybeArgs


#if DEBUG
           -- print message if in debug mode
           when (getDebug args)
                (putStrLn ("DEBUG: Parsed arguments:\n" ++ show args ))
#endif

           -- parse file (file gets read in by parser) and convert to TypedTRS
           -- (some semantic checks are also done by the parser)
           tmp <- parseTRSFromFile args
           let trs = getTRS tmp


           putStrLn "Parsed Typed Term Rewrite System:\n"
           print trs
           putStr "--------------------------------------------------\n\nAnalyzing..."

           -- ceate constraints
           let lpProb = fromMaybe (E.throw $ FatalException "constraints where empty")
                        $ analyzeTRS args trs

           -- verbose output shall print constraints
           when (getVerbose args) $ putStr
                    ("\rTyped Term Rewrite System With Variables:\n\n" ++
                     show (getPTRS lpProb) ++
                     "\n\nConstraints:\n\n" ++
                     showListWithSep show (getPConstraints lpProb) ";\n" ++
                     ";\n\n--------------------------------------------------" ++
                     "\n\nSolving Constraints...")

           -- solve constraints
           varMap <- solveConstraints lpProb args

           -- show solved constratins
           when (getVerbose args) $ do
             putStrLn "\rSolved Constraints:\n"
             putStr (showListWithSep show (M.toList varMap) "\n")
             putStr "\n\n\n"

           -- enter variables to TRS
           let typedTRStmp = putVarsIntoTRS (getPTRS lpProb) varMap
               typedTRS = typedTRStmp { getConstructors = putVarsIntoCtrsType varMap $
                                        removeDatatypeFromCtrs (getConstructors typedTRStmp)}
           putStrLn "\rAnalyzed Rules:                \n"
           print typedTRS

           -- print complexity
           putStrLn "--------------------------------------------------\n\nComplexity:\n"
           printComplexity typedTRS
         )
         (\e -> print (e :: ProgException) >> exitFailure)

-- | This function just unpacks a Either ParseErro SimpleTRS to
--   the appropriate SimpleTRS or an exception.
getTRS             :: Either ParseError TypedTRS -> TypedTRS
getTRS (Right trs) = trs
getTRS (Left err)  = E.throw $ ParseException $ show err

putVarsIntoCtrsType   :: M.Map String Int -> [ConstructorType] -> [ConstructorType]
putVarsIntoCtrsType m = map (\x -> x { getCConstructors =
                                       putVarsIntoCtrs m (getCConstructors x) } )

putVarsIntoCtrs   ::  M.Map String Int -> [Constructor] -> [Constructor]
putVarsIntoCtrs m = map (putVarsIntoCtr m)

putVarsIntoCtr                     ::  M.Map String Int -> Constructor -> Constructor
putVarsIntoCtr m (Constructor s c) = Constructor (putVarsIntoStmt s m) c

putVarsIntoTRS     :: TypedTRS -> M.Map String Int -> TypedTRS
putVarsIntoTRS t m = t { getRewriteRules =
                             map (`putVarsIntoRR` m) (getRewriteRules t)  }


putVarsIntoRR     :: RewriteRule -> M.Map String Int -> RewriteRule
putVarsIntoRR (RewriteRule n s r) m =
    RewriteRule n (putVarsIntoSig s m) (map (`putVarsIntoRelation` m) r)

putVarsIntoSig               :: Signature -> M.Map String Int -> Signature
putVarsIntoSig (Signature par ret c p) m =
    Signature (map (`putVarsIntoIV` m) par) (putVarsIntoIV ret m)
              (putVarsIntoCost c m) (putVarsIntoCost p m)


putVarsIntoRelation                :: Relation -> M.Map String Int -> Relation
putVarsIntoRelation (Relation l r) m = Relation (putVarsIntoStmt l m) (putVarsIntoStmt r m)


putVarsIntoIV :: InternalVariable -> M.Map String Int -> InternalVariable
putVarsIntoIV (InternalVariable n d c) m = InternalVariable n d
                                           (map (`putVarsIntoCost` m) c)
                                           -- case M.lookup (show c) m of
                                           --   Just x -> InternalVariable n d (CostInt x)
                                           --   Nothing -> InternalVariable n d CostEmpty


putVarsIntoCost               :: Cost -> M.Map String Int -> Cost
putVarsIntoCost (CostVar n) m = case M.lookup n m of
                                  Just x -> CostInt x
                                  Nothing -> CostInt 0
putVarsIntoCost x _           = x


putVarsIntoStmt                :: Statement -> M.Map String Int -> Statement
putVarsIntoStmt (Function n c) m = Function n (map (`putVarsIntoStmt` m) c)
putVarsIntoStmt (Atom x) m       = Atom $ putVarsIntoIV x m


printComplexity     :: TypedTRS -> IO ()
printComplexity trs = do
  putStr $ "\tInfo: pX represents the X'th input parameter (starting with p0).\n" ++
           "\tWhereas T(Y,Z) represents the Y'th constructor and it's Z'th\n" ++
           "\tparameter of the datatype T.\n\n\t"

  putStrLn $ concatMap (\x -> complexityRR trs x ++ "\n\t") (getRewriteRules trs)


getParameters    :: RewriteRule -> [InternalVariable]
getParameters rr = map (\a ->  InternalVariable ('p' : show a) NIL [] )  [0..]

-- getParameters rr = map (\(a,b) -> b { getIVVariable = 'p' : show a })
--                    (zip [0..] $ (getParTypes . getSignature) rr)

complexityRR                     :: TypedTRS -> RewriteRule -> String
complexityRR trs (RewriteRule n s c) = "O(" ++ n ++  ") = " ++
                                   if null compl then "1" else compl
    where vars = getParameters (RewriteRule n s c)
          compl = complexitySig trs vars s


complexitySig :: TypedTRS -> [InternalVariable] -> Signature -> String
complexitySig trs vars (Signature par _ _ _) =
    params ++ (if null costs || null params then "" else " + ") ++ costs


        where params = showListWithSep complexityIV vs " + "
              costs = showListWithSep complexityIV (sumOfSameNames $ concatMap innerCosts par) " + "
              filterZeros = filter (\x -> case sum (getIVCosts x) of
                                   0 -> False
                                   _ -> True
                                   )

              vs = filterZeros $ map (\(x, y) -> x { getIVCosts = getIVCosts y })
                                                                  (zip vars par)

              sumOfSameNames :: [InternalVariable] -> [InternalVariable]
              sumOfSameNames = sum'
                  where
                    sum' []     = []
                    sum' (x':xs') = elem' : sum' newList
                        where
                        (elem', newList) = foldl fun (x',[]) xs'

                    fun :: (InternalVariable, [InternalVariable]) -> InternalVariable ->
                               (InternalVariable, [InternalVariable])
                    fun (acc, list) e =
                            if getIVVariable acc == getIVVariable e
                            then
                                (acc { getIVCosts = zipWith (+) (getIVCosts acc)
                                                    (getIVCosts e)
                                     }, list)
                            else (acc, list ++ [e])


              innerCosts :: InternalVariable -> [InternalVariable]
              innerCosts iv = filterZeros $
                              concatMap (\(a, b) ->
                                         map (\(c, d) ->
                                                  -- d { getIVVariable =
                                                  d { getIVVariable = " [from " ++
                                                      show (getIVDatatype iv) ++ "(" ++
                                                      tail (getIVVariable a) ++ "," ++
                                                      show c ++  ")]"
                                                    })
                                         (zip [0..] b))
                              (zip vars (subCosts $ getIVDatatype iv))

              subCosts :: Datatype -> [[InternalVariable]]
              subCosts dt = map fun $ constrs dt
                  where
                    fun :: Constructor -> [InternalVariable]
                    fun c = case getCStatement c of
                              Atom _ -> []
                              Function _ ch -> map getVariable ch   -- has to be an Atom


              constrs :: Datatype -> [Constructor]
              constrs dt = getCConstructors $ ctrtype dt
              ctrtype :: Datatype -> ConstructorType
              ctrtype dt = fromMaybe
                           (E.throw $ FatalException $ "Could not find datatype " ++
                             show dt ++".")
                           (find (\x -> dt == getCDatatype x) $ getConstructors trs)


complexityIV                             :: InternalVariable -> String
complexityIV iv = show (sum $ getIVCosts iv) ++
                  if take (length " [from ") (getIVVariable iv) == " [from "
                  then getIVVariable iv
                  else '⋅' : getIVVariable iv


removeDatatypeFromCtrs :: [ConstructorType] -> [ConstructorType]
removeDatatypeFromCtrs = map (\x -> x { getCConstructors = map wrapper (getCConstructors x) })
    where
      wrapper                                 :: Constructor -> Constructor
      wrapper (Constructor (Atom x) c)        = Constructor
                                                (Atom $ x { getIVDatatype = NIL }) c
      wrapper (Constructor (Function n ch) c) =
          Constructor (Function n (map wrapper' ch)) c

      wrapper' :: Statement -> Statement
      wrapper' (Atom x) = Atom $ x { getIVDatatype = NIL }
      wrapper' (Function n ch) = Function n (map wrapper' ch)


--
-- Main.hs ends here
