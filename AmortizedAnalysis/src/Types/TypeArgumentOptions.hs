-- TypeArgumentInfo.hs ---
--
-- Filename: TypeArgumentInfo.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 20:22:08 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mi Mai  7 18:37:22 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 61
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module holds the information for the argument
-- information data object.
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module Types.TypeArgumentOptions where

data ArgumentOptions = ArgumentOptions
    { getFilePath      :: FilePath
    , getVerbose       :: Bool
    , getMaxVariable   :: Int
    , getDebug         :: Bool
    , getMaxTreeHeight :: Int
    , getHelpText      :: Bool
    , getKeepFiles     :: Bool
    , getSMTSolver     :: Bool
    , getTempDir       :: String
    , getUseRelaxRule  :: Bool
    } deriving (Show, Eq)


--
-- TypeArgumentInfo.hs ends here
