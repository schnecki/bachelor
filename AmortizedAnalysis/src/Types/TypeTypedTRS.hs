{-# LANGUAGE CPP #-}
-- TypeTypedTRS.hs ---
--
-- Filename: TypeTypedTRS.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 20:37:33 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mo Jun  2 13:20:15 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 254
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This method holds all the data types for a typed TRS.
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Types.TypeTypedTRS where


import Misc.HelperFunctions
-- see also following import files for type declarations
import Types.TypeGeneral
import Types.TypeDatatype
import Exceptions.ExceptionHandler

import Data.Maybe (fromMaybe)
import Data.List (find)
import Control.Exception (throw)
#ifdef DEBUG
import Debug.Trace (trace)
#endif

-----------------------------------------------------------------------------
-- SYNONYMS
type FunctionName    = String


-----------------------------------------------------------------------------
-- | Definition of the simple TRS.
-- It holds a list of variables and a list of rewrite rules.
data TypedTRS = TypedTRS
    { getVariables :: [InternalVariable]
    , getConstructors :: [ConstructorType]
    , getRewriteRules :: [RewriteRule]
    }


-- | Instance declaration of TypedTRS for Show.
-- It shows the Variables first and then the rules.
instance Show TypedTRS where
    show (TypedTRS v c r) =
           "Variables:\n\t" ++ showListWithSep show v ", " ++
             "\n\nConstructors:\n" ++ showListWithSep show c "\n" ++
              "\n\nParsed Rules:\n" ++ showSolutionRules c r "\n\n"
             -- "\n\nParsed Rules:\n" ++ showListWithSep show r "\n\n"


showSolutionRules :: [ConstructorType] -> [RewriteRule] -> String -> String
showSolutionRules ctrs rrs sep = concatMap (\x -> showSolutionRule ctrs x ++ sep) rrs


showSolutionRule :: [ConstructorType] -> RewriteRule -> String
showSolutionRule ctrs (RewriteRule n s rs) =
    "\t" ++ n ++ " :: " ++ showSolutionSig ctrs s ++ "\n\t" ++
         showListWithSep show rs "\n\t"


showSolutionSig :: [ConstructorType] -> Signature -> String
showSolutionSig ctrs (Signature par post CostEmpty _) =
    showListWithSep (solution ctrs) par " x "
                        ++ " --> " ++ showListWithSep (solution ctrs) [post] ""
showSolutionSig ctrs (Signature par post p q) =
    showListWithSep (solution ctrs) par " x " ++ " --" ++ show p ++ -- "/" ++
    -- show q ++
             "--> " ++ showListWithSep (solution ctrs) [post] ""

-- | This function can be used to be called with showListWithSep
-- defined in HelperFunctions.hs.
solution         :: [ConstructorType] -> InternalVariable -> String
solution ctrs iv = show (getIVDatatype iv) ++
                   if null listOfCostsToPrint
                   then []
                   else "(" ++ showListWithSep show listOfCostsToPrint ", "  ++ ")"


     where
       listOfCostsToPrint = foldl fun [] (zip costs (getCConstructors constrType))

       fun             :: [Cost] -> (Cost, Constructor) -> [Cost]
       fun acc (c, ct) =
           case getCCost ct of
             CostInt _ -> acc
             _ -> acc ++ [c]

       costs = getIVCosts iv

       constrType :: ConstructorType
       constrType = fromMaybe (throw $ FatalException
                               "Could not get datatype to display solution")
                    (find (\x -> getIVDatatype iv == getCDatatype x) ctrs)

-----------------------------------------------------------------------------
-- | This type holds the constructors information.
data ConstructorType  = ConstructorType
    { getCDatatype          ::  Datatype
    , getCParameters        :: [Cost]
    , getCConstructors      :: [Constructor]
    , getRecursiveType      :: Bool
    }

instance Show ConstructorType where
    show x = "\t" ++ show (getCDatatype x)
             -- ++ (if not (null costList)
             --    then "(" ++ showListWithSep show costList ", " ++ ")"
             --    else "")

             ++ (if not (null (getCParameters x))
                 then "(" ++ showListWithSep show (getCParameters x) ", " ++ ")"
                 else "")
             ++ " = "
             ++ (if getRecursiveType x
                 then "µX."
                 else "")
             ++ "< " ++ showListWithSep show (getCConstructors x) ", " ++" >"

        where
          costList = getListOfCtrCosts x

          getListOfCtrCosts         :: ConstructorType -> [Cost]
          getListOfCtrCosts ctrType = foldl fun [] (getCConstructors ctrType)

          fun         :: [Cost] -> Constructor -> [Cost]
          fun acc ctr = acc ++ costs
              where
                costs = case getCCost ctr of
                          CostEmpty -> []
                          y -> [y]


-----------------------------------------------------------------------------
-- | This data type holds the information of a constructor.
data Constructor = Constructor
    { getCStatement :: Statement
    , getCCost :: Cost
    }

instance Show Constructor where
    show (Constructor s CostEmpty) = show s
    show (Constructor s c) = showCtrStmt s ++ ":" ++ showCost c

showCtrStmt                 :: Statement -> String
showCtrStmt (Function f []) = f
showCtrStmt (Function f ch) = f ++ "("++ unwordsWith (map showCtrStmt ch) ", "
                              ++ ")"
    where
      unwordsWith         :: [String] -> String -> String
      unwordsWith [] _    = []
      unwordsWith wds del = head wds ++ foldl (\acc x -> acc ++ del ++ x) [] (tail wds)
showCtrStmt (Atom iv)       =
    case iv of
      InternalVariable n _ [] -> n
      InternalVariable n _ x         -> n ++ "(" ++ showListWithSep show x ", " ++ ")"


-----------------------------------------------------------------------------
-- | Definition of the simple rewrite rule.
data RewriteRule = RewriteRule
    { getName :: String
    , getSignature :: Signature
    , getRelations :: [Relation]
    }


-- | Instance declaration of RewriteRule for Show.
-- It shows the Signature (with Name) and then each relation.
instance Show RewriteRule where
    show (RewriteRule n s rs) =
        "\t" ++ n ++ " :: " ++ show s ++ "\n\t" ++ showListWithSep show rs "\n\t"

-----------------------------------------------------------------------------
-- | Definition of the simple Signature
data Signature = Signature
    { getParTypes :: [InternalVariable]
    , getRetTypes :: InternalVariable
    , getCostsP :: Cost
    , getCostsQ :: Cost
    }


-- | Instance delcaration of Signature for Show.
-- It shows the Signature with 'x's between each Datatype.
instance Show Signature where
    show (Signature par r p q)
        | null costsP = (if null par then ""
                         else showListWithSep show par " x ") ++
                           " -> " ++ show r
        | otherwise   = showListWithSep show par " x " ++ " --" ++ costsP -- ++ "/" ++
                        -- show q
                        ++ "--> " ++ show r
        where costsP = show p

-----------------------------------------------------------------------------
-- | A simple Relation
data Relation = Relation
    { getLeft :: Statement
    , getRight :: Statement
    }

-- | Instance declaration of Relation for Show.
-- It shows the left side of the relation, then appends an arrow (" -> " ) followed
-- by the right side of the relation.
instance Show Relation where
    show (Relation l r) = show l ++ " -> " ++ show r

-----------------------------------------------------------------------------
-- | A simple Statement. Either a Function or a Atom Variable
data Statement = Function
    { getFunName :: FunctionName
    , getChilds :: [Statement]
    }
               | Atom
    { getVariable :: InternalVariable
    }

-- | Instance delcaration of Statement for Show.
-- It shows either the function with name and all childs, or the (atom)
-- variable.
instance Show Statement where
    show (Atom x)        =  show x
    show (Function n []) =  n
    show (Function n c)  =  n ++ "(" ++ showListWithSep show c ", " ++ ")"


findRRByName          :: String -> TypedTRS -> Maybe RewriteRule
findRRByName name trs = find ((name ==). getName) (getRewriteRules trs)


isRewriteRule       :: String -> TypedTRS -> Bool
isRewriteRule n trs = n `elem` map getName (getRewriteRules trs)


findConstructorInType             :: String -> ConstructorType -> Maybe Constructor
findConstructorInType name crtype = find fun (getCConstructors crtype)
    where
      fun :: Constructor -> Bool
      fun ctr = case getCStatement ctr of
                  Atom y -> getIVVariable y == name
                  Function y _ -> y == name

findConstructorTypeByName:: String -> TypedTRS -> Maybe ConstructorType
findConstructorTypeByName name trs = find fun (getConstructors trs)
    where
      fun        :: ConstructorType -> Bool
      fun crtype = any (\x ->
                        case getCStatement x of
                          Atom y -> getIVVariable y == name
                          Function y _ -> y == name) (getCConstructors crtype)


findCtrByName       :: String -> TypedTRS -> Maybe Constructor
findCtrByName n trs = case findConstructorTypeByName n trs of
                        Nothing -> Nothing
                        Just ctrType -> findConstructorInType n ctrType


--
-- TypeTypedTRS.hs ends here
