-- TypeDatatype.hs ---
--
-- Filename: TypeDatatype.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Mo Dez  2 23:09:50 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mo Mär 17 00:05:03 2014 (+0100)
--           By: Manuel Schneckenreither
--     Update #: 61
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This module holds all supported data types.
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Types.TypeDatatype where


-----------------------------------------------------------------------------
-- | Any data type can be added here. The parser will automatically know them
-- using the Read instance automatically derived.
data Datatype = Datatype {
      getDatatypeString :: String
    } | NIL                        -- constructor NIL cannot be parsed!
    deriving (Read, Eq)


instance Show Datatype where
    show (NIL)        = "NIL"
    show (Datatype x) = x


--
-- TypeDatatype.hs ends here
