-- TypeConstraint.hs ---
--
-- Filename: TypeConstraint.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Sa Dez  7 16:24:08 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Mi Mai  7 19:05:04 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 146
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This file holds the types for the constraints.
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module Types.TypeConstraint where

import Misc.HelperFunctions
import Types.TypeTypedTRS
import Types.TypeSExpression

data LPProblem = LPProblem
    { getPTRS :: TypedTRS
    , getPConstraints :: [Constraint]
    , getPVarNr :: Int
    } deriving (Show)


-- | This data definition is reposible for the atomic types in the constraints.
--   There are two possiblilities: An Integer alone, e.g. 4, or a variable
--   including a factor, e.g. 2 * x.
data CFact = CVar Int String
           | CConst Int
             deriving (Eq)


instance Show CFact where
    show (CConst x) = show x
    show (CVar n x)
        | n == 1    = x
        | n == (-1) = "-" ++ x ++ ""
        | otherwise = show n ++ " " ++ x


-----------------------------------------------------------------------------
-- | This data type defines a constraint. The lists are read as a sum
--   of input. E.g. CEq [x, 2, 4] [y, 3] is therefore: x + 2 + 4 = y + 3
--
-- CLeq: less or equal to
-- CEq : equal to
-- CGeq: greater or equal to
data Constraint = CLeq [CFact] [CFact]
                | CEq  [CFact] [CFact]
                | CGeq [CFact] [CFact]
                  deriving (Eq)

-- | Show instance of Constraint. Print with using showListWithSep from the module
--   HelperFunctions
instance Show Constraint where
    show constr = case constr of
                    CLeq l1 l2 -> showListWithSep show l1 " + " ++ " <= " ++
                                  showListWithSep show l2 " + "
                    CEq l1 l2  -> showListWithSep show l1 " + " ++ " = " ++
                                  showListWithSep show l2 " + "
                    CGeq l1 l2 -> showListWithSep show l1 " + " ++ " >= " ++
                                  showListWithSep show l2 " + "

constraintToSExpression     :: Constraint -> SExpression
constraintToSExpression ctr = SNode (getOperator ctr)
                              (listOfFactToSExpr $ left ctr)
                              (listOfFactToSExpr $ right ctr)
    where
      left  (CLeq l _) = l
      left  (CEq  l _) = l
      left  (CGeq l _) = l
      right (CLeq _ r) = r
      right (CEq  _ r) = r
      right (CGeq _ r) = r


listOfFactToSExpr        :: [CFact] -> SExpression
listOfFactToSExpr []     = SLeaf "0"
listOfFactToSExpr (f:[]) = sExpressionLeaf f
listOfFactToSExpr (f:fs) = SNode Plus (sExpressionLeaf f) (listOfFactToSExpr fs)

sExpressionLeaf :: CFact -> SExpression
sExpressionLeaf c =
    case c of
      (CConst x)      -> if x < 0
                         then SNode Minus (SLeaf "0") (SLeaf $ show (x * (-1)))
                         else SLeaf $ show x
      (CVar n x)
          | n == 1    -> SLeaf x
          | n == (-1) -> SNode Minus (SLeaf "0") (SLeaf x)
          | otherwise -> SNode Times (SLeaf $ show n) (SLeaf x)


getOperator            :: Constraint -> Operator
getOperator (CLeq _ _) = Leq
getOperator (CEq _ _)  = Eq
getOperator (CGeq _ _) = Geq


--
-- TypeConstraint.hs ends here
