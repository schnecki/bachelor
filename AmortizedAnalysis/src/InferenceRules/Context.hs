{-# LANGUAGE CPP #-}
-- Context.hs ---
--
-- Filename: Context.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Sa Dez 21 16:42:12 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:40:41 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 2055
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module InferenceRules.Context
    ( createCtxsFromTRS ) where

import Types.TypeContext
import Types.TypeGeneral
import Types.TypeDatatype
import Types.TypeTypedTRS
import Types.TypeConstraint
import Exceptions.ExceptionHandler

import Data.List (find, zip4, nubBy)
import Data.Char (isNumber)
import Control.Exception (throw)
#ifdef DEBUG
import Debug.Trace (trace)
#endif
import Data.Maybe (fromMaybe)
import Control.Arrow (first)

varNameCost :: [Char]
varNameCost = "ipvarc"

-- | This datatype is used to represent and the constructors and then
-- to calculate it's well-typedness costs out of this stucture. It is
-- a tree with arbitariy number of nodes (children in functions and
-- either with empty leaves (Nothing) or with leaves that contain
-- something (Just a).
--
-- It is used as 'Tree Constructor' on which each node holds its
-- constructor cost inside of the constructor. Variables are
-- represented by Nothing, nullary (0-ary) constructors (constructors
-- without parameters) are represented as Just x leaves.
data Tree a = Node a [Tree a] | Leaf (Maybe a) deriving (Show)


-- | This function takes a TRS and creates the contexts to be solved.
--   It then invokes the commands to solve it using the inference rules
--   implemented and given in the paper.
createCtxsFromTRS     :: TypedTRS -> Prove
createCtxsFromTRS trs = wellTypednessConstrains $ pGreaterOrEqualToQ $
    prove { getContexts = createContext (getConstructors $ getTRS prove)
                          (getRewriteRules $ getTRS prove)
          }
    where
      -- set variable to the trs and set the costs for the signature
      prove = setSignatureCosts (bindVariablesToTRS trs)

      -- P and Q costs for every rewrite rule.
      setSignatureCosts   :: Prove -> Prove
      setSignatureCosts p = p0 { getTRS = newTRS0 }
          where
            t = getTRS p
            len = 2 * length (getRewriteRules t)
            (p0, vars2) = getNewVariableName p len
            newTRS0 = t { getRewriteRules = newRR (getRewriteRules t) 0 }

            newRR          :: [RewriteRule] -> Int -> [RewriteRule]
            newRR [] _     = []
            newRR (r:rs) n = r { getSignature = ( getSignature r) {
                                                  getCostsP = CostVar $ vars2 !! n
                                                , getCostsQ = CostVar $ vars2 !! (n+1)
                                                }} : newRR rs (n+2)


-- | This function calls the function which are used to create the
-- contexts for the list of given rewrite rules.
createContext                             :: [ConstructorType] -> [RewriteRule] -> [Context]
createContext  _                      []   = []
createContext  ctrs (RewriteRule _ s rels:rest) =
    map (\r -> Context (preSignature ctrs s r )             -- input variables
               (getCostsP s, getCostsQ s)
               (Conditions [] [] [])                        -- empty constraints
               (postSignature s r)                          -- rule output statement
               []                                           -- start with empty history
        ) rels
    ++ createContext ctrs rest


-- | This function defines the preconditions for the given signature
-- and relation. A precondition is the left side of a inference rule
-- statement, e.g. if you got:
--
-- x:NAT(v0, v1) |-p/q- 0:NAT(v0, v1)
--
-- then the left side x:NAT(v0, v1) is called precondition.
preSignature         :: [ConstructorType] -> Signature -> Relation -> [InternalVariable]
preSignature ctrs sig rel =
    if length pureAtoms > length datatypes ||
       length (childs $ getLeft rel) /= length (getParTypes sig)
    then throw $ SemanticException $
             "Rewrite rule " ++ show rel ++ " does not fit to signature. "
             ++ ( if length (childs $ getLeft rel) /= length (getParTypes sig)
                  then "There are too many types given in the signature."
                  else  "There are not enough types given in the signature." )
    else zipWith (curry bindDatatype) atoms datatypes

    where

      -- This function puts the datatypes and the variables together
      -- to form the left side of the context, the preSignature.
      bindDatatype                   :: (Statement, InternalVariable) -> InternalVariable
      bindDatatype (Function n _, d) = InternalVariable n (getIVDatatype d) (getIVCosts d)
      bindDatatype (Atom x, v)       = v { getIVVariable = getIVVariable x }


      -- This function create a list of all atoms from the given
      -- relation.
      atoms :: [Statement]
      atoms = createListOfAtoms $ getLeft rel

      -- This is a list of variables only. Function-Atoms like 0()
      -- will not be in this list.
      pureAtoms :: [Statement]
      pureAtoms = filter (\x -> case x of
                                  Atom _ -> True
                                  _ -> False) atoms

      -- This function is used to create a list of atoms inclusive
      -- numbers (e.g. Function 0 []).
      createListOfAtoms                 :: Statement -> [Statement]
      createListOfAtoms (Function n []) = [Function n []]
      createListOfAtoms (Function _ ch) = concatMap createListOfAtoms ch
      createListOfAtoms (Atom x)        = [Atom x]


      -- This function gets all childs from a statement.
      childs :: Statement -> [Statement]
      childs x = case x of
                   Function _ ch -> ch
                   _ -> []


      -- This is a list of datatypes. It is created using the
      -- datatype information as well as the signature from the
      -- relation.
      datatypes :: [InternalVariable]
      datatypes = createListOfParTypes (getParTypes sig) (childs $ getLeft rel)

          where
            -- Helper function to create list of datatypes. It creates a
            -- list of parameter types.
            --
            -- It takes a list of datatypes (InternalVariables) and a
            -- list of constructor childs and builds up the datatype
            -- including the variable names and cost variables.
            createListOfParTypes               :: [InternalVariable] -> [Statement] ->
                                                  [InternalVariable]
            createListOfParTypes []   []       = []
            createListOfParTypes [] _          = throw $
              FatalException "Error while creating list of parameters. Datatypes were nil."
            createListOfParTypes (d:ds) (Function n ch:ss) =
                case ch of
                  [] -> d : createListOfParTypes ds ss  -- Take the signatures variables
                  _ -> createListOfParTypes (constrParams d n ctrs ++ ds) (ch ++ ss)

                                                    -- take the signatures variables
            createListOfParTypes (d:ds) (_:ss) = d : createListOfParTypes ds ss

            createListOfParTypes _ []          = [] -- throw the rest
                                                    -- away (function
                                                    -- without chlds)

            ctrType :: Datatype -> ConstructorType
            ctrType dt = fromMaybe (throw $ FatalException "ctr error")
                      (find (\x -> dt == getCDatatype x) ctrs)


            -- another helper function for the list of datatypes.
            constrParams        :: InternalVariable -> String -> [ConstructorType] ->
                                   [InternalVariable]
            constrParams _ n [] = throw $
              FatalException $ "Constructor " ++ n ++ " not found! The left sides of the " ++
                               "rewrite rules must contain constructors only!"
            constrParams d n (ConstructorType _ _ ctr _ : rest) =
                fromMaybe (constrParams d n rest) (constrParams' d n ctr)
                    where
                      constrParams'        :: InternalVariable -> String -> [Constructor]
                                           -> Maybe [InternalVariable]
                      constrParams' _ _ [] = Nothing
                      constrParams' d' n' (Constructor (Function c ch) _ : restCtr)
                          | n' == c         =
                              mapM (\x ->
                                    case x of
                                      Atom (InternalVariable vn _ vc ) ->
                                          Just $
                                          if vn == "X"
                                          then d'        -- e.g. µX.<s(X)>, use d' as cost
                                          else InternalVariable "" (Datatype vn) $
                                               fillCosts (Datatype vn) vc
                                      _ -> throw $ FatalException
                                           "Nested constructors are not allowed."
                                   ) ch
                          | otherwise = constrParams' d n restCtr

                      -- This function takes a datatype and a list
                      -- of costs as input and traverses through the
                      -- constructors of the datatype to build up the
                      -- costs including constructor costs set as an
                      -- integer.
                      fillCosts         :: Datatype -> [Cost] -> [Cost]
                      fillCosts dt csts = fst $
                                          foldl fun ([], 0) constructors
                          where
                            constructors :: [Constructor]
                            constructors = getCConstructors $ ctrType dt


                            fun         :: ([Cost], Int) -> Constructor -> ([Cost], Int)
                            fun (acc, i) ctr' =
                                case getCCost ctr' of
                                  CostInt y -> (acc ++ [CostInt y], i)
                                  _ -> (acc ++ [csts !! i], i+1)


-- | Defines the post condition with the given signature and relation.
-- A post condition is the right hand side of a rewrite rule statement
-- in the inference rule tree. See preSignature documentation for more
-- information.
--
-- It takes as input a signature and a relation and returns the
-- Context-Statement, where a context statement is a statement, e.g.
-- f(0) with information including datatype and costs.
postSignature         :: Signature -> Relation -> CtxStatement
postSignature sig rel =
    case postStmt of
      CtxAtom x -> CtxAtom (x { getIVDatatype = datatype
                              , getIVCosts = getIVCosts tmp})
      CtxFunction n _ ch _ -> CtxFunction n datatype ch (getIVCosts tmp)


    where
      postStmt :: CtxStatement
      postStmt = convertToCtxStmt $ getRight rel

      datatype = getIVDatatype tmp
      tmp = getRetTypes sig

      -- ctrType :: InternalVariable -> ConstructorType
      -- ctrType d = fromMaybe (throw $ FatalException "ctr post error")
      --             (find (\x -> getIVDatatype d == getCDatatype x) ctrs)


      convertToCtxStmt                 :: Statement -> CtxStatement
      convertToCtxStmt (Function n ch) = CtxFunction n NIL
                                         (map convertToCtxStmt ch) []
      convertToCtxStmt (Atom n)        = CtxAtom n


-- | This function binds default variables to the signatures of the rewrite rules
--   as well as the constructor cost parameters
--   given in the TRS. It returns a Prove with empty contexts.
bindVariablesToTRS     :: TypedTRS -> Prove
bindVariablesToTRS trs = proveWithSigs $ (Prove [] [] trs 0) { getTRS = bindConstrCosts trs }
    where
      sigs :: [Signature]
      sigs = map getSignature (getRewriteRules trs)


      -- Bind constructor cost variables to unique names
      bindConstrCosts   :: TypedTRS -> TypedTRS
      bindConstrCosts t = t { getConstructors = bindCtrTypes (getConstructors t) }

          where
            bindCtrTypes :: [ConstructorType] -> [ConstructorType]
            bindCtrTypes ctrType = map fillCParameters $ snd (foldl fun (0, []) ctrType)
                where
                  fun         :: (Int, [ConstructorType]) -> ConstructorType ->
                                 (Int, [ConstructorType])
                  fun acc ctr = (\(constrs, int) ->
                                     (int, snd acc ++
                                         [ctr { getCConstructors = constrs }])
                                ) (bindCtrs (fst acc) (getCConstructors ctr))

                  fillCParameters     :: ConstructorType -> ConstructorType
                  fillCParameters ctr = ctr { getCParameters =
                                                  foldl (\acc x ->
                                                       case getCCost x of
                                                         CostInt _ -> acc
                                                         _ -> acc ++ [getCCost x]) []
                                                  (getCConstructors ctr)
                                            }


            -- This function finally binds constructor cost variables.
            bindCtrs :: Int -> [Constructor] -> ([Constructor], Int)
            bindCtrs i = foldl fun ([], i)

                where
                  -- bind variables to constructors
                  fun           :: ([Constructor], Int) -> Constructor ->
                                   ([Constructor], Int)
                  fun (ls, j) c =
                      case getCStatement c of
                        Function _ [] -> -- e.g. < 0 > or < 0:0 >
                           case getCCost c of
                             CostEmpty -> (ls ++ [c { getCCost = CostVar $
                                                      varNameCost ++ show j
                                                    }], j + 1)
                             _ -> (ls ++ [c], j)
                        Function n ch ->
                            case getCCost c of
                              CostEmpty -> (ls ++ [ c { getCStatement = Function n newChd
                                                      , getCCost = CostVar
                                                        (varNameCost ++ show newInt)
                                                     }]
                                           , newInt + 1)
                              _ -> (ls ++ [c { getCStatement = Function n newChd
                                             }], j)
                          where
                            (newChd, newInt) = foldl fun2 ([], j) ch

                            fun2 :: ([Statement], Int) -> Statement ->
                                    ([Statement], Int)
                            fun2 (ls', k) (Atom x) =
                                if getIVVariable x == "X"
                                then (ls' ++ [Atom x], k)           -- recursive
                                else (ls' ++ [Atom x { getIVCosts = newCosts } ]
                                     , newInt + 1)


                                      -- [Atom x {getIVCosts =
                                      --          map (\y -> CostVar $ varNameCost ++
                                      --                     show y) [k..max']
                                      --         }], max' + 1)
                                    where

                                      (newCosts, newInt) = foldl fun3 ([], k)
                                                           (addEmpty $ getIVCosts x)

                                      fun3 :: ([Cost], Int) -> Cost -> ([Cost], Int)
                                      fun3 (cts, i) ct =
                                          case ct of
                                            CostEmpty ->
                                                (cts ++ [ CostVar $
                                                          varNameCost ++ show i ]
                                                , i + 1)
                                            _ -> (cts ++ [ct], i)

                                      addEmpty           :: [Cost] -> [Cost]
                                      addEmpty inp
                                          | has > should =  take should inp
                                          | has < should =
                                              inp ++ replicate (should - has) CostEmpty
                                          | otherwise    = inp

                                          where
                                            has = length inp
                                            should = foldl (\acc x' ->
                                                            case getCCost x' of
                                                              CostInt _ -> acc
                                                              _ -> acc + 1) 0
                                                     (getCConstructors constrType)

                                      constrType = fromMaybe
                                           (throw $ FatalException $
                                            "Could not find constructor " ++
                                            show (getIVVariable x) ++ "."
                                           )
                                            (getConstructorTypeByDatatype trs $
                                             getIVDatatype x)


                        Atom _ -> (ls ++ [c {getCCost = CostVar $ "atom"++
                                             varNameCost ++ show j }]
                                  , j+1)


      proveWithSigs :: Prove -> Prove
      proveWithSigs p = p { getTRS = (getTRS p) { getRewriteRules = newRRs }
                          , getVarNr = newV }
          where
            (newV, newRRs) = setRRs 0 (getRewriteRules $ getTRS p)

      setRRs            :: Int -> [RewriteRule] -> (Int, [RewriteRule])
      setRRs i rrs = foldl setSignature (i, []) rrs

          where
            setSignature :: (Int, [RewriteRule]) -> RewriteRule ->
                            (Int, [RewriteRule])
            setSignature (v, rrs) rr =
                         (newV, rrs ++
                              [rr { getSignature = newSig }] )

                where

                  (Signature oldPar oldRet oldP oldQ) = getSignature rr

                  -- This function creates a new signature with
                  -- variables and costs out of a "blank" one or it
                  -- uses the given number, if there is any set in the
                  -- input file like this: < c:0 >.
                  newSig = Signature newPar newRet oldP oldQ

                  (v0, newPar) = foldl funPar (v, []) oldPar
                  funPar             :: (Int, [InternalVariable]) -> InternalVariable ->
                                        (Int, [InternalVariable])
                  funPar (i', ivs) iv =
                      (newInt, ivs ++ [InternalVariable "" (getCDatatype ctrt)
                                                        newCost
                                      ])
                          where
                            (newInt, newCost) =
                                foldl fun (i', []) (getCConstructors ctrt)

                            fun :: (Int, [Cost]) -> Constructor -> (Int, [Cost])
                            fun (nr, acc) constr =
                                case getCCost constr of
                                  -- if a number, then use it
                                  CostInt x -> (nr, acc ++ [CostInt x])
                                  -- if nothing defined, create variable
                                  _ -> (nr + 1, acc ++ [CostVar $ "ipvar" ++ show nr])

                            ctrt = ctrType iv

                  (newV, newRet) = (\(a,b) -> (a, head b)) $ funPar (v0, []) oldRet

      -- This function gets a constructor type out of the internal
      -- variable information.
      ctrType :: InternalVariable -> ConstructorType
      ctrType d = fromMaybe (throw $ FatalException "ctr signature error")
                  (find (\x -> getIVDatatype d == getCDatatype x)
                            (getConstructors trs))


-- | This function takes as input parameter a prove and adds all well-typedness
--   rules found in the proves context. It returns the new prove, with the
--   the constraints for well-typedness.
wellTypednessConstrains   :: Prove -> Prove
wellTypednessConstrains p = newP { getContexts = newCtx }
    where
      leftRRs :: [Statement]
      leftRRs = map getLeft $ concatMap getRelations $ (getRewriteRules . getTRS) p

      rightRR :: [[Statement]]
      rightRR = map (map getRight . getRelations) $ (getRewriteRules . getTRS) p

      rightRelWithCosts :: [(Statement, [Cost])]
      rightRelWithCosts = concatMap (\rr -> zip ((map getRight . getRelations) rr)
                                      (repeat $ csts rr)) $
                          (getRewriteRules . getTRS) p

          where
            csts    :: RewriteRule -> [Cost]
            csts rr = getIVCosts . getRetTypes . getSignature $ rr


      trs = getTRS p

      rhsConstraints :: [Constraint]
      rhsConstraints =
          -- trace ("right: " ++ show rightRelWithCosts) $
          -- trace ("constr: " ++ show (concatMap rhsStmtConstraints rightRelWithCosts)) $
          concatMap rhsStmtConstraints rightRelWithCosts

      rhsStmtConstraints                       :: (Statement, [Cost]) -> [Constraint]
      rhsStmtConstraints (Atom _, _)           = []
      rhsStmtConstraints (Function _ [], _)    = []
      rhsStmtConstraints (Function n ch, csts) =
          case findRRByName n trs of
            Just rr ->
                concatMap (\(c, cst) -> case findRRByName (getStmtName c) trs of
                                          Nothing ->
                                              case findCtrByName (getStmtName c) trs of
                                                Nothing -> [] -- a variable
                                                Just cCtr ->
                                                    if length l - 1  > idx
                                                    then l !! idx
                                                    else concat l
                                                        where
                                                          idx = ctrNr cCtr
                                                          l = (zipWith (curry lr)
                                                                      cst (ctrCost cCtr cst))
                                          Just cRR -> concat $
                                              zipWith (curry lr) cst
                                                   (getRetCosts cRR)

                          ) (zip ch (map getIVCosts $ (getParTypes . getSignature) rr)) ++
                concatMap rhsStmtConstraints (zip ch (concatMap (`getChildCosts` csts) ch))
            Nothing ->
                case findCtrByName n trs of
                  Nothing -> []
                  Just ctr ->
                      concatMap (\(c, cst) -> case findRRByName (getStmtName c) trs of
                                                Nothing ->
                                                    case findCtrByName (getStmtName c) trs of
                                                      Nothing -> []
                                                      Just cCtr -> concat $
                                                          zipWith (curry lr)
                                                                  cst (ctrCost cCtr cst)
                                                Just cRR -> concat $
                                                    zipWith (curry lr)
                                                            cst (getRetCosts cRR)
                                ) (zip ch (ctrChldCost (getCStatement ctr) csts)) ++
                      concatMap rhsStmtConstraints (zip ch (concatMap (`getChildCosts` csts) ch))


          where
            lr :: (Cost, Cost) -> [Constraint]
            lr (l, r) = if l == r then [] else [CEq [CVar 1 $ showCost l] [CVar 1 $ showCost r]]

            getStmtName                :: Statement -> String
            getStmtName (Atom x)       = getIVVariable x
            getStmtName (Function n _) = n

            -- isRR                :: Statement -> Bool
            -- isRR (Atom x)       = False
            -- isRR (Function n _) = isRewriteRule n trs

            -- getRR      :: Statement -> RewriteRule
            -- getRR stmt = fromMaybe undefined (findRRByName (getFunName stmt) trs)

            getCtr      :: Statement -> Constructor
            getCtr stmt = fromMaybe undefined (findCtrByName (getFunName stmt) trs)

            getRetCosts :: RewriteRule -> [Cost]
            getRetCosts = getIVCosts . getRetTypes . getSignature

            getChildCosts                     :: Statement -> [Cost] -> [[Cost]]
            getChildCosts (Atom x)        def = []
            getChildCosts (Function n []) def = []
            getChildCosts (Function n ch) def =
                case findRRByName n trs of
                  Just rr -> map getIVCosts $ (getParTypes . getSignature) rr
                  Nothing ->
                      case findCtrByName n trs of
                        Nothing -> throw $ FatalException $
                                   n ++ " is neither a rewrite rule, nor a constructor?!?" ++
                                  " This should not have happened. Programming error!"
                        Just ctr -> ctrChldCost (getCStatement ctr) def
                            -- map (`ctrChldCost` def) (getChilds $ getCStatement ctr)

            ctrChldCost                    :: Statement -> [Cost] -> [[Cost]]
            ctrChldCost (Atom x) def       = if getIVVariable x == "X"
                                             then [def]
                                             else [ctrTypeCosts (getIVVariable x) def ]
            ctrChldCost (Function n [])  d = [ctrCost (getCtr $ Function n []) d]
            ctrChldCost (Function _ chs) d = concatMap (`ctrChldCost` d) chs


            ctrNr                      :: Constructor -> Int
            ctrNr (Constructor stmt _) =

                case findConstructorTypeByName (getStmtName stmt) trs of
                        Nothing -> throw $ FatalException "not possible"
                        Just ctr -> snd $ head $ filter (\(a, _) ->
                                                         getStmtName (getCStatement a) ==
                                                         getStmtName stmt) $
                                    zip (getCConstructors ctr) [0..]


            ctrCost                        :: Constructor -> [Cost] -> [Cost]
            ctrCost (Constructor stmt _) d =
                case findConstructorTypeByName (getStmtName stmt) trs of
                  Nothing -> throw $ FatalException $ "Could not find constructor type: " ++ show stmt
                  Just ct -> map (\(x,y) -> case x of
                                       CostInt _ -> x
                                       _ -> y
                                 ) $
                                zip (map getCCost (getCConstructors ct)) d


            ctrTypeCosts     :: String -> [Cost] -> [Cost]
            ctrTypeCosts n d = case find (\x -> (getDatatypeString . getCDatatype) x == n)
                             (getConstructors trs) of
                               Nothing -> throw $ FatalException $
                                          "Could not find datatype " ++ n ++ "."
                               Just ctrtype -> map (\(x,y) -> case x of
                                                               CostInt {} -> x
                                                               _ -> y
                                                   ) $
                                               zip (map getCCost (getCConstructors ctrtype)) d

      ctrRHSConstraints = concatMap rhsCtrCond rightRR

      -- If the RHS's of the same rewrite rule consist of one constructor each,
      -- then the costs of these constructors have to be the same!
      rhsCtrCond       :: [Statement] -> [Constraint]
      rhsCtrCond stmts = if length ctrsNames < 2 -- only proceed if we got more
                                                 -- than 1 constructor
                         then []
                         else fst $ foldl fun ([], getCCost $ head ctrs) (tail ctrs)

          where

            fun                      :: ([Constraint], Cost) -> Constructor
                                     -> ([Constraint], Cost)
            fun (cds, prevCosts) ctr =  (CEq [CVar 1 $ showCost prevCosts]
                                             [CVar 1 $ showCost $ getCCost ctr] : cds,
                                             getCCost ctr)

            ctrsNames = ctrsOnly stmts
            ctrs :: [Constructor]
            ctrs = map ((fromMaybe
                        (throw $ FatalException
                         "Could not find constructor. This should not happen!")) .
                        (`findCtrByName` getTRS p) . getFunName) ctrsNames

            -- Remove all non constructors
            ctrsOnly :: [Statement] -> [Statement]
            ctrsOnly = nubBy (\a b -> getFunName a == getFunName b) .
                       filter (\x -> case x of
                                              Function _ [] -> True
                                              _ -> False)


      -- returns a list of signatures for each given rewrite rule
      paramDt :: [[InternalVariable]]
      paramDt = map (getParTypes . getSignature) $
                replicator ((getRewriteRules . getTRS) p)
          where
            replicator :: [RewriteRule] -> [RewriteRule]
            replicator = foldl fun []
                where
                  fun acc rr = acc ++ replicate (length $ getRelations rr) rr

      -- This function gets the constructor by its name from the trs
      -- getConstructor :: (Monad m) => String -> m Constructor
      -- getConstructor = getConstructorByName (getTRS p)

      -- This function computes the new P and new context
      (newP, newCtx) = (newP', computeNewContexts list)
          where
            list :: [(Context, Statement, [InternalVariable], String)]
            list = zip4 (getContexts p) leftRRs paramDt vars
            (newP', vars) = getNewVariableName p (length (getContexts p))


      -- This function takes as input a quadruple containing the
      -- current context, the statment from the rewrite rule, the
      -- pre-signature of the function and a new variable name for the
      -- new p.
      computeNewContexts     :: [(Context, Statement, [InternalVariable], String)] ->
                                 [Context]
      computeNewContexts = map newCtx'
              where
                newCtx' :: (Context, Statement, [InternalVariable], String) ->
                           Context
                newCtx' (ctx, stmt, dt, var) =
                    ctx { getCosts = (CostVar var, snd (getCosts ctx))
                        , getConditions =
                            (getConditions ctx) {
                            getCostConstraints = getCostConstraints (getConditions ctx) ++
                                  newCond (listOfCtrs stmt dt) var
                                              (fst $ getCosts ctx)
                          -- ++ ctrRHSConstraints -- NOT USED, might not be right!
                            ++ rhsConstraints
                             }
                           }


      -- This function takes as input a statement and a list of
      -- internal variables which ought to correlate to the signature
      -- of the statement. It then returns a list of Trees for each
      -- child of the topmost function of the statement. The Tree
      -- contains a constructor for each node and either a Just
      -- Constructor for a leaf, or Nothing in case of a variable.
      --
      --
      -- Example for: head(queue(s(0),r))
      --
      -- [ Node queue(NAT(c1), NAT(c2)):v0
      --    [ Node s(X):c1
      --       [ Leaf (Just 0:0) ]
      --    , Leaf Nothing
      --    ]
      -- ]
      listOfCtrs              :: Statement -> [InternalVariable] ->
                                 [Tree Constructor]
      listOfCtrs (Function _ ch) sig = map listOfCtrs' l
          where
            l = zip3 ch (map getIVDatatype sig) (map getIVCosts sig)


      listOfCtrs'                   :: (Statement, Datatype, [Cost]) -> Tree Constructor
      listOfCtrs' (stmt, dt, costs) =
          case stmt of
            Atom x ->
                case constr (getIVVariable x) of
                  Nothing -> Leaf Nothing
                  Just y -> Leaf $ Just $
                            y { getCCost = costs !! nrOfC }

            Function n [] ->
                case constr n of
                  Nothing -> Leaf Nothing
                  Just y -> Leaf $ Just $
                            y { getCCost = costs !! nrOfC }

            Function n ch ->
                case constr n of
                  Nothing -> throw $ FatalException $ "The left side of a rewrite" ++
                             "rule must contain constructors only!"
                  Just x ->
                      -- trace ("stmt: " ++ show stmt ++ " function: " ++ n ++ " list: " ++
                      --        show ((getAllCosts (getCStatement x) costs)) ++ " nr: " ++ show nrOfC) $
                      Node (x { getCCost = costs !! nrOfC })
                            (map listOfCtrs'
                            (zip3 ch (getDatatypes $ getCStatement x)
                             (zipWith fillCosts (getDatatypes $ getCStatement x)
                              (getAllCosts (getCStatement x)
                               (filter (\x -> case x of
                                                CostInt _ -> False
                                                _ -> True ) costs))
                             )))

          where


            -- This function takes as input a datatype and a list of
            -- cost parameters and returns a list of costs, where the
            -- variables have been replaced with the costs from the
            -- first list.
            fillCosts         :: Datatype -> [Cost] -> [Cost]
            fillCosts d csts = fst $ foldl fun ([], 0) constructors
                where
                  constructors :: [Constructor]
                  constructors = getCConstructors $ ctrType d


                  fun         :: ([Cost], Int) -> Constructor -> ([Cost], Int)
                  fun (acc, i) ctr' =
                      case getCCost ctr' of
                        CostInt y -> (acc ++ [CostInt y], i)
                        _ -> if length csts <= i
                             then (acc ++ [CostInt 0], i)  -- worst case: 0
                             else (acc ++ [csts !! i], i+1)     -- take the given costs


            getAllCosts :: Statement -> [Cost] -> [[Cost]]
            getAllCosts stmt def = map (\x ->
                                        if getIVVariable x == "X"
                                        then def
                                        else getIVCosts x) (getAtoms stmt)

            getDatatypes :: Statement -> [Datatype]
            getDatatypes = map (\x -> if getIVVariable x == "X"
                                      then dt
                                      else getIVDatatype x
                               )  . getAtoms

            getAtoms :: Statement -> [InternalVariable]
            getAtoms (Function _ ch) = concatMap getAtoms ch
            getAtoms (Atom x) = [x]


            nrOfC :: Int
            nrOfC = nrOfConstr (ctrType dt) stmt

            constr   :: String -> Maybe Constructor
            constr s = getConstructorByName (getTRS p) s :: Maybe Constructor


            -- This function gets a constructor type out of the internal
            -- variable information.
            ctrType :: Datatype -> ConstructorType
            ctrType d = fromMaybe (throw $ FatalException $ "ctr signature error: " ++ show d)
                        (find (\x -> d == getCDatatype x)
                                  (getConstructors (getTRS p)))


      -- This function takes as input parameter a list of Tree
      -- Constructor, a string which represents the new variable
      -- (from: pre |--p/q-- post), and the cost variable p of the
      -- rewrite rule (from: lhs --p/q--> rhs) and returns the
      -- corresponding well-typedness constraints.
      newCond :: [Tree Constructor] -> String -> Cost -> [Constraint]
      newCond tree var p' =
          [CEq
           [CVar 1 var]                    -- bind new variable
           ([ CVar 1 $ showCost p'          -- p
            , CConst (-1)                  --  negative one
            ] ++
            concatMap sumCosts tree
           )
          ]
          where
            sumCosts                   :: Tree Constructor -> [CFact]
            sumCosts (Node ctr chd)    = CVar 1 (showCost $ getCCost ctr) :
                                         concatMap sumCosts chd
            sumCosts (Leaf (Just ctr)) = [CVar 1 (showCost $ getCCost ctr)]
            sumCosts (Leaf Nothing)    = []


-- | This function takes as input a constuctor type and a statement
-- and returns the position of the constuctor in the constructor type
-- or throws an exception if it cannot be found!
nrOfConstr :: ConstructorType -> Statement-> Int
nrOfConstr ctrType conStmt = nrOfConstr' (getCConstructors ctrType) 0
    where
      nrOfConstr' []   _   = throw $ FatalException $
                             "Datatype " ++ show conStmt ++ " not found in type " ++
                             "definition of type " ++ show (getCDatatype ctrType) ++
                             ". The types in the rewrite rules do not match the ones " ++
                             " given in the datatypes (check constructor types also)."
      nrOfConstr' (c:cs) i =
          if (getFunName . getCStatement) c == (case conStmt of
                                                  Function n _ -> n
                                                  Atom x -> getIVVariable x)
          then i
          else nrOfConstr' cs (i+1)


-- | Get a constructor just by its name
getConstructorTypeByDatatype     :: (Monad m) => TypedTRS -> Datatype -> m ConstructorType
getConstructorTypeByDatatype t d = case find
                                   (\x -> getCDatatype x == d)
                                   (getConstructors t) of
                                     Nothing -> fail $ "ConstructorType with dataype " ++
                                                show d ++ " not found."
                                     Just x -> return x

-- | Get a constructor just by its name
getConstructorByName     :: (Monad m) => TypedTRS -> String -> m Constructor
getConstructorByName t n = getCtrs (getConstructors t) n
    where
      getCtrs      :: (Monad m) => [ConstructorType] -> String -> m Constructor
      getCtrs [] n = fail $ "Constructor " ++ n ++ " not found."
      getCtrs (ct:cts) n =
                   case find (\(Constructor constr cost ) ->
                                  case constr of
                                    (Function fn _) -> n == fn
                                    (Atom (InternalVariable ivn _ _ )) -> n == ivn
                             ) (getCConstructors ct) of
            Just x -> return x
            Nothing -> getCtrs cts n


pGreaterOrEqualToQ   :: Prove -> Prove
pGreaterOrEqualToQ prove =
    prove { getContexts = pGreaterOrEqualToQ' (getContexts prove)}
    where
      pGreaterOrEqualToQ'                                   :: [Context] -> [Context]
      pGreaterOrEqualToQ' []                                = []
      pGreaterOrEqualToQ' (Context pre (p, q) cond post _:cs) =
          Context pre (p, q) newCond post [] : pGreaterOrEqualToQ' cs

          where
            newCond = cond {getCostConstraints = getCostConstraints cond ++ newCostC  }
            newCostC = [CGeq [CVar 1 $ showCost p] [CVar 1 $ showCost q]]

-- | This function takes as input parameter a prove and a number of variables, which
--   should be generated and returns the new prove along with a list of variables.
getNewVariableName :: Prove -> Int -> (Prove, [String])
getNewVariableName prove num = (newProve, strs start num)
    where
      start = getVarNr prove
      newProve = prove {getVarNr = start + num }
      strs _ 0 = []
      strs nr n = ("ipvar" ++ show nr) : strs (nr+1) (n-1)


--
-- Context.hs ends here
