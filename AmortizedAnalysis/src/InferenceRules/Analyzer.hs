{-# LANGUAGE CPP #-}
-- Analyzer.hs ---
--
-- Filename: Analyzer.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: So Dez  8 19:33:27 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:37:15 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 679
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
--
--
--
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:


module InferenceRules.Analyzer
    ( analyzeTRS
    ) where

import Types.TypeContext
import Types.TypeTypedTRS
import Types.TypeConstraint
import Types.TypeArgumentOptions

import InferenceRules.InferenceRuleImpl
import InferenceRules.Context
import Exceptions.ExceptionHandler

import Control.Exception (throw)
import Data.Char (isNumber)
import Control.Monad (when)
import Debug.Trace (trace)
import Data.Maybe (fromMaybe)


analyzeTRS :: (Monad m) => ArgumentOptions -> TypedTRS -> m LPProblem
analyzeTRS args trs =
#ifdef DEBUG
    if getDebug args then
        trace ("\rDEBUG Starting " ++ show startingProve) $
        trace ("DEBUG: Solution:\n" ++ show solution) $
        return retVal
    else
#endif
    return retVal
    where
      -- The value to be returned. Defined here because of access by
      -- DEBUG/non DEBUG code.
      retVal = LPProblem (getTRS solution) (concatMap getCostConstraints conds)
                          (getVarNr solution)

      -- The prove to start from. This function calls the Context.hs
      -- functions, that creates the contexts from the term rewrite
      -- system
      startingProve :: Prove
      startingProve = case createCtxsFromTRS trs of -- createContexts from TRS
                        Prove [] _ _ _ ->
                            throw $ WarningException $
                                      "No rewrite rules could be parsed. " ++
                                      "There is nothing to do."
                        x -> x

      maxFreeVars = getMaxVariable args     -- number of maximum free variables
      maxTreeHeight = getMaxTreeHeight args -- number of max derivation heigth

      -- This function analyzes the prove using the inference rule in
      -- the InferenceRuleImpl.hs file. If a solution was found it
      -- returns it as a prove. In case no solution can be found, it
      -- throws an exception.
      analyzeProve :: [Prove] -> Prove
      analyzeProve [] = throw $ WarningException $
         "The Term Rewrite System could not be solved using the inference rules :( \n" ++
         "First of all, check if your input has no errors, e.g. wrong signatures. " ++
         "Furthermore, you could try to increase the maximum number " ++
         "of free variables and/or " ++
         "the maximum number of tree derivation height (see help with -h). Sorry about that!"
      analyzeProve (p:ps) =
        -- apply inference rule to the prove
        case applyInferenceRules p args of

          -- TODO: Non-important! Find out which approach is faster:
          --
          -- a) using the number to put the failed context in front or
          -- b) ignore the number and just throw the failed prove away

          Left nr -> analyzeProve ps -- analyzeProve $ putCtxInFront ps nr
          Right prove -> do
              let newProves = removeMaxNo prove maxFreeVars maxTreeHeight
                  -- remove unsolveable proves
                  proves = filter (checkContexts . getContexts) newProves

              if null proves
              then analyzeProve ps                -- try other possibilities
              else
                  -- check for solution - if a we get Just solution, return solution
                  -- otherwise iterate
                  fromMaybe (analyzeProve (proves++ps)) (getSolution proves)


      solution :: Prove                         -- the final solution
      solution = (\x -> x { getProvenContexts =
                         map removeNrConstraintsContext (getProvenContexts x)
                          }) $
                 analyzeProve [startingProve]


      removeNrConstraintsContext :: Context -> Context
      removeNrConstraintsContext ctx =
          ctx { getConditions =
                    (getConditions ctx) {
                  getCostConstraints = removeNrOnlyConstraints
                                       ( getCostConstraints . getConditions $ ctx)
                }

              }


      conds :: [Conditions]                     -- the conditions of the solution
      conds = map getConditions (getProvenContexts solution)


-- | This function takes a list of constraints and
-- removes all constraints which are composed of numbers
-- only. Like: 3 >= 3. This is needed because lp_solve
-- does not like such constraints!
--
-- The that this happens here, is that it is the place
-- where the least number of constraints have to be
-- checked for this occurence.
--
-- Only under the circumstances that the user enters the
-- nested constructor (e.g. L(NAT(0,3), X) ) costs can
-- cause such constraints.
removeNrOnlyConstraints      :: [Constraint] -> [Constraint]
removeNrOnlyConstraints = foldl fun []
    where
      fun            :: [Constraint] -> Constraint -> [Constraint]
      fun acc ctr =
        acc ++
            if allNumbers (left ctr) && allNumbers (right ctr)
            then []
            else [ctr]

      -- This function checks if all input terms are
      -- composed of numbers only
      allNumbers :: [CFact] -> Bool
      allNumbers = all (\x -> case x of
                                CConst _ -> True
                                CVar 1 nr -> all isNumber nr
                                _ -> False)

      left (CLeq a _ ) = a
      left (CEq a _ )  = a
      left (CGeq a _ ) = a
      right (CLeq _ b ) = b
      right (CEq _ b )  = b
      right (CGeq _ b ) = b


-- | This function takes as input parameter a list of proves and an integer value x.
--   It will then iterate over the list putting the context which is at position x in
--   the list of getContexts to the front.
-- putCtxInFront                       :: [Prove] -> Int -> [Prove]
-- putCtxInFront p 0                   = p
-- putCtxInFront [] _                  = []
-- putCtxInFront (Prove c p t v:ps) nr =
--     (if length c <= nr    -- ensure half-solved proves do not break the program
--      then
-- #ifdef DEBUG
--          trace ("TRACE Too big: " ++ show (length c) ++ " <= " ++ show nr) $
-- #endif
--          Prove c p t v
--      else -- Prove (c !! nr : take nr c ++ drop (nr + 1) c) p t v) : putCtxInFront ps nr
--          Prove (drop nr c ++ take nr c) p t v) : putCtxInFront ps nr


-- | This function takes as input a list of proves and the maximum number of free
--   variables and the maximum inference tree height.
--   It returns the list without the proves that exceeded the maximum number of
--   free variables.
removeMaxNo    :: [Prove] -> Int -> Int -> [Prove]
removeMaxNo [] _ _ = []
removeMaxNo (p:ps) m t
    | getVarNr p > m || (not (null ctxs) && not (null his) &&
      fst ((head . getHistory) (head ctxs)) > t )
        = removeMaxNo ps m t
    | otherwise
        = p : removeMaxNo ps m t
    where
      ctxs = getContexts p
      his = getHistory.head $ ctxs

checkContexts :: [Context] -> Bool
checkContexts = all (checkConditions . getConditions)

-- | This function returns True if the unificands are from the same value.
checkConditions :: Conditions -> Bool
checkConditions c = checkUnificands (getVarUnificands c) &&
                    checkUnificands (getTypeUnificands c)

-- | This function takes a list of proves and checks it for the finished and
--   successful proves. It either returns a successful prove, or fails.
getSolution    :: (Monad m) => [Prove] -> m Prove
getSolution [] = fail "No prove was found."
getSolution (p:ps) =
    case p of
      Prove [] _ _ _ -> return p
      _ -> getSolution ps


-- | This function takes a a list of a' tuples and checks if all tuples
--   have the same value. If so it returns true, otherwise false.
checkUnificands :: (Eq a') => [(a', a')] -> Bool
checkUnificands = all (uncurry (==))


--
-- Analyzer.hs ends here
