{-# LANGUAGE CPP #-}
-- InferenceRuleImpl.hs ---
--
-- Filename: InferenceRuleImpl.hs
-- Description:
-- Author: Manuel Schneckenreither
-- Maintainer:
-- Created: Di Dez 10 15:05:15 2013 (+0100)
-- Version:
-- Package-Requires: ()
-- Last-Updated: Di Jun 10 06:41:18 2014 (+0200)
--           By: Manuel Schneckenreither
--     Update #: 5910
-- URL:
-- Doc URL:
-- Keywords:
-- Compatibility:
--
--

-- Commentary:
-- This file implements the inference rules.
-- It is intuitively implemented and should be quite readable.
-- The function names should describe the functionality of the
-- code. Therefore, the names might be quite long for some of them.
--

-- Change Log:
--
--
--
--
-- This program is free software; you can redistribute it and/or
-- modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; see the file COPYING.  If not, write to
-- the Free Software Foundation, Inc., 51 Franklin Street, Fifth
-- Floor, Boston, MA 02110-1301, USA.
--
--

-- Code:

module InferenceRules.InferenceRuleImpl
    ( applyInferenceRules
    ) where


import Exceptions.ExceptionHandler
import Types.TypeContext
import Types.TypeDatatype
import Types.TypeGeneral
import Types.TypeConstraint
import Types.TypeTypedTRS
import Types.TypeArgumentOptions

import Control.Exception (throw, catch)
import Data.Maybe (fromMaybe)
import Control.Monad (liftM)
import Data.List

#ifdef DEBUG
import Debug.Trace (trace)
#endif


-- | Use every single rule on the context. And return the list of contexts
--   (each list item used a different inference rule).
applyInferenceRules                      :: Prove -> ArgumentOptions -> Either Int [Prove]
applyInferenceRules (Prove [] p t v)   _      = return [Prove [] p t v]
applyInferenceRules (Prove (c:cs) p t v) opts =
#ifdef DEBUG
    (if getDebug opts
    then trace (show (getHistory c)) $ id
    else id)
#endif
    (case c of
       (Context [] _ _ CtxEmpty _) -> applyInferenceRules (Prove cs (c:p) t v) opts
       (Context pre cost cond stmt []) ->  -- use share only at first call
           return (if null shared
                   then [Prove (Context pre cost cond stmt [(0, "")]:cs) p t v]
                   else map (\(x,y,z) -> Prove (z++cs) p x y) shared)
       (Context [] _ _ _ _) ->
           -- only one more rule can be applied, either it works, or it doesn't
           case map (\(x,y,z) -> Prove (z++cs) p x y)
                    (applyAllInferenceRules' (t, v, c) opts) of
             [] -> -- prove failed, no more preconditions
                 Left $ nonCompositions p
             x -> return x
       _ -> return $ map (\(x,y,z) ->               -- normally apply all rules
                          Prove (z++cs) p x y) (applyAllInferenceRules' (t, v, c) opts)
    )
    where
      shared = share (t, v, c)

      -- This function finds the number of contexts which were not generated
      -- by a composition. This is the number of succeeded contexts from the starting
      -- prove, when given all succeeded contexts so far.
      nonCompositions        :: [Context] -> Int
      nonCompositions []     = 0
      nonCompositions (con:cons) = if "composition" `elem` map snd (getHistory con)
                                   then nonCompositions cons
                                   else 1 + nonCompositions cons


applyAllInferenceRules'   :: (TypedTRS, Int, Context) -> ArgumentOptions ->
                             [(TypedTRS, Int, [Context])]
applyAllInferenceRules' (t, v, c) opts = concatMap (\x -> x (t, v, c)) rules
        where
          rules =
              [relax | getUseRelaxRule opts]  -- add relax rule if option set
              ++
              [ function    -- list of inference rules (the order is important)
              , constr      -- the higher the rule is named, the earlier it gets
              , identity    -- applied. If the prove can be finished without
                            -- using
              , composition -- rules at the end of the list, the computation
                            -- will
              , weak        -- be faster.
              ]


-- applyAllInferenceRules' :: (TypedTRS, Int, Context) -> ArgumentOptions ->
--                            [(TypedTRS, Int, [Context])]
-- applyAllInferenceRules' (t, v, Context pre cost cond post his) opts =
--     trace ("\n\nBEFORE:\n" ++ show (map (\(a, b, c) -> c !! nr) before) ++
--            "\nAFTER:\n" ++ show (map (\(_,_,a) -> a) $
--                                  concatMap (\(a,b,c) -> constr
--                                                 (a, b, c !! nr)) before))
--               [(t, v, [Context [] cost cond CtxEmpty []])]

--            where
--              before =
--                       -- concatMap (\(a,b,c) -> composition (a, b, c !! 1)) $
--                       -- concatMap (\(a,b,c) -> weak (a, b, head c)) $
--                       -- concatMap (\(a,b,c) -> composition (a, b, c !! 2)) $
--                       -- concatMap (\(a,b,c) -> composition (a, b, c !! 1)) $
--                       concatMap (\(a,b,c) -> composition (a, b, c !! 0)) $
--                       [(t, v, [Context pre cost cond post his])]
--              nr = 1


-- | The identity inference rule.
identity :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
identity (t, v, Context [InternalVariable v1 d1 c1] (p, q) c
         (CtxAtom (InternalVariable v2 d2 c2)) his) =
              [ newCtx | v1 == v2 && d1 == d2 &&
                    case getConstructor t v2 d1 of  -- do only if ctrs cannot be found
                      Nothing -> True
                      _ -> False
              ]
    where
      newCtx = (t, newV, [Context [] (newP, newQ) (newConditions c)
                                  CtxEmpty (((fst.head $ his) + 1, "identity") : his)])
      newConditions cds = Conditions (getVarUnificands cds ++ newVarU)
                          (getTypeUnificands cds ++ newTypeU)
                          (getCostConstraints cds ++ newCostC)

      (newV, newVarStrs) = getNewVariableName v 2
      newP = CostVar $ head newVarStrs
      newQ = CostVar $ newVarStrs !! 1
      newVarU  = [] -- [(v1, v2)] -- already checked in [newCtx | ... ]
      newTypeU = [] -- [(d1, d2)] -- already checked in [newCtx | ... ]
      newCostC = zipWith (\a b -> CGeq [CVar 1 $ showCost a] [CVar 1 $ showCost b]) c1 c2 ++
                 [ CEq [CVar 1 $ showCost newP] [CConst 0]
                 , CEq [CVar 1 $ showCost newQ] [CConst 0]
                 , CGeq [CVar 1 $ showCost p] [CVar 1 $ showCost newP] -- relax included
                 , CGeq [CVar 1 $ showCost p, CVar (-1) $ showCost newP]
                        [CVar 1 $ showCost q, CVar (-1) $ showCost newQ]
                 ]
identity _ = []


relax             :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
relax (t, v, ctx) = [(t, newV, [Context (getPreConditions ctx) (newP, newQ)
                                          newCond (getPostCondition ctx)
                                  (((fst.head $ his) + 1, "relax") : his)])
                             | not (null his) && -- do not allow multiple relaxs
                               snd (head his) /= "relax"] -- in a row
    where
      his = getHistory ctx
      (oldP, oldQ) = getCosts ctx
      (newP, newQ) = (CostVar $ head vars, CostVar $ vars!!1)
      (newV, vars) = getNewVariableName v 2
      cond = getConditions ctx
      newCond = Conditions (getVarUnificands cond) (getTypeUnificands cond)
                           (getCostConstraints cond  ++ newCostC)
      newCostC = [ CGeq [CVar 1 $ showCost oldP] [CVar 1 $ showCost newP]
                 , CGeq [CVar 1 $ showCost oldP, CVar (-1) (showCost newP)]
                        [CVar 1 $ showCost oldQ, CVar (-1) (showCost newQ)]
                 , CGeq [CVar 1 $ showCost newP] [CVar 1 $ showCost newQ]
                 ]


function :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
function (t, v, Context preUnsorted (p, q) cond (CtxFunction f d fc fcost) his) =
    -- trace ("preUnsorted: " ++ show preUnsorted)
    -- trace ("pre (sorted): " ++ show pre)
    -- trace ("fc: " ++ show fc) $
    case getRewriteRuleByName t f of
      Nothing -> []
      Just rule  ->
          [(t, v, [Context [] (p, q) newConditions CtxEmpty
                               (((fst.head $ his) + 1, "function") : his)])
               | length preUnsorted == length fc &&                 -- parameter length for ctx
                 (length fc == 0 ||                         -- an empty function
                  length preUnsorted == (length . getParTypes . getSignature) rule && -- for rewrite r.
                  all (\x -> case x of                  -- all function childs are atoms
                               CtxAtom _ -> True
                               CtxFunction _ _ [] _ -> True
                               _ -> False
                      ) fc &&
                  (getIVDatatype . getRetTypes . getSignature) rule == d)
          ]
          where
            pf = (getCostsP . getSignature) rule
            qf = (getCostsQ . getSignature) rule
            -- (newV, vars) = getNewVariableName v
            newConditions = Conditions (getVarUnificands cond ++ newVarC)
                                    (getTypeUnificands cond ++ newTypeC)
                                    (getCostConstraints cond  ++ newCostC)

            pre :: [InternalVariable]
            pre = sortBy (sortFun fcNames) preUnsorted

            sortFun          :: [String] -> InternalVariable -> InternalVariable -> Ordering
            sortFun strs l r = compare idxL idxR

                where
                  getIdx        :: String -> [String] -> Int
                  getIdx n list = case find ((== n) . fst) (zip list [0..]) of
                                    Nothing -> length list
                                    Just (_, nr) -> nr

                  idxL = getIdx (getIVVariable l) strs
                  idxR = getIdx (getIVVariable r) strs

            fcNames :: [String]
            fcNames = map getCtxAtomName fc

            getCtxAtomName x = case x of
                                 CtxAtom x -> getIVVariable x
                                 CtxFunction n _ _ _ -> n


            newVarC = zip (map getIVVariable pre) (map getCtxAtomName fc)

            newTypeC = zip (map getIVDatatype pre)      -- types from pre and signature
                       (map getIVDatatype ((getParTypes . getSignature) rule))

            newCostC = CGeq [CVar 1 $ showCost p, CVar (-1) $ showCost q]
                            [CVar 1 $ showCost pf, CVar (-1) $ showCost qf]
                       :  -- plus constraints from parameters:
                       map (\(x, y) -> CGeq [CVar 1 $ showCost x]
                                            [CVar 1 $ showCost y])
                               (zip (concatMap getIVCosts pre)
                                    (concatMap getIVCosts  $
                                     (getParTypes . getSignature) rule)
                               ) ++ -- plus cost constrain from return values
                       (concatMap (\(a, b) ->
                                [CLeq [CVar 1 $ showCost a]
                                     [CVar 1 $ showCost b]
                                ]) $
                       zip fcost (getIVCosts $ getRetTypes $ getSignature rule))
                       ++
                       [CGeq [CVar 1 $ showCost p] [CVar 1 $ showCost q]]

function _ = []


constr :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
constr (t, v, Context pre cost cond (CtxAtom (InternalVariable n d postcost)) his ) =
    constr (t, v, Context pre cost cond (CtxFunction n d [] postcost) his)
constr (t, v, Context pre (p, q) cond (CtxFunction n d ch postcost) his) =
    if not $ all (\x -> case x of -- all function childs are atoms and variables
                    CtxAtom _ -> True
                    CtxFunction _ _ []  _ -> True
                    _ -> False        ) ch
    then []
    else
     case getConstructor t n d of
      Nothing -> []
      Just (Constructor conStmt costC) ->
          [(t, v, [Context [] (p, q) newC CtxEmpty
                   (((fst.head $ his) + 1, "constr") : his)])
           | all (\(b,a) ->
                  case getConstructorsType t d of
                    Nothing -> False
                    Just _ ->
                        case b of
                          Atom y ->
                              getIVDatatype a ==
                              if getIVVariable y == "X"
                              then  -- Bi = X, therefore check Ai = C
                                  d
                              else  -- Ai = Bi
                                  getIVDatatype y
                          _ -> throw $ FatalException "Nested contructors are not allowed."
                 ) (zip (getChilds conStmt) pre) &&
             length (getChilds conStmt) == length pre
          ]

          where
            newC = cond { getVarUnificands = getVarUnificands cond ++ newVarU0
                        , getCostConstraints = getCostConstraints cond ++ newCostC
                        }

            -- The constructor type.
            ctrType datatype = fromMaybe (throw $ FatalException
                                 "The author thinks this error is not possible :P") $
                      getConstructorsType t datatype

            variableName x = case x of
                               CtxAtom y -> getIVVariable y
                               CtxFunction y _ _ _ -> y

            newVarU0 =  zipWith (\a b -> (getIVVariable a, variableName b))
                               pre ch

            -- p >= k AND p-k >= q or differently: p-q >= k where k
            -- is the post cost
            newCostC0 =
                [ CGeq [ CVar 1 $ showCost p ]
                               [ CVar 1 $ showCost ctrCost ] -- (postcost !! nrOfConstr) ]
                        , CGeq [ CVar 1 $ showCost p
                               , CVar (-1) $ showCost ctrCost ]  -- (postcost !! nrOfConstr) ]
                               [ CVar 1 $ showCost q ]
                        ]


            ctrCost = case costC of
                        CostInt {} -> costC
                        _ -> postcost !! nrOfConstr


            -- This fucntion holds all internal variables of the
            -- constructer children.
            constrChld :: [InternalVariable]
            constrChld = case conStmt of
                           Function _ x -> foldl fun [] x
                                      where fun acc (Atom iv') = acc ++ [iv']

                           _ -> []

            -- This number represents the position in the list of
            -- constructors
            nrOfConstr :: Int
            nrOfConstr = nrOfConstr' (getCConstructors $ ctrType d) 0
                where
                  nrOfConstr' []   _   = throw $ FatalException
                                         "No more constructors...weired! Programming error!"
                  nrOfConstr' (c:cs) i =
                      if (getFunName . getCStatement) c == getFunName conStmt
                      then i
                      else nrOfConstr' cs (i+1)


            -- This function computes the new cost constraints.
            newCostC = newCostC0 ++
                       -- include Ai = C AND Bi = X constraints
                       (concatMap (\(_, x, y) ->
                                   -- Bi = X
                                   [
                                    CGeq [ CVar 1 $ showCost x ]
                                          [ CVar 1 $ showCost $ postcost !! y
                                          ]
                                   ])

                        -- constraints for all costs
                        (concatMap (filter (\(a,_,c) -> getIVVariable a == "X")
                                   )
                         (map (\x -> zip3
                                (replicate
                                 (length $ getIVCosts $ fst x) (constrChld !! snd x))
                                (getIVCosts $ fst x) [0..])
                           (zip pre [0..]))
                         )) ++

                       (concatMap (\(_, x, y) ->
                                   -- Bi = X
                                   [
                                     CGeq [ CVar 1 $ showCost x ]
                                          [ CVar 1 $ showCost $ postcost !! y ]
                                   ])

                        -- constraint for the right one only
                        (concatMap (filter (\(a,_,c) -> getIVVariable a == "X"
                                                        && c == nrOfConstr)
                                   )
                         (map (\x -> zip3
                                (replicate
                                 (length $ getIVCosts $ fst x) (constrChld !! snd x))
                                (getIVCosts $ fst x) [0..])
                           (zip pre [0..]))
                         )) ++


                       -- include Ai = Bi constraints
                       (map (\(a, x, y) ->
                                 CGeq [ CVar 1 $ showCost x ]
                                      [ CVar 1 $ showCost $
                                        bCosts (getIVDatatype a) (getIVCosts a) !! y])

                        (concatMap (filter (\(a,_,_) -> getIVVariable a /= "X"))
                         (map (\x -> zip3
                                (replicate
                                 (length $ getIVCosts $ fst x) (constrChld !! snd x))
                                (getIVCosts $ fst x) [0..])
                           (zip pre [0..]))
                         ))
                           where
                             bCosts :: Datatype -> [Cost] -> [Cost]
                             bCosts dt csts = snd $ (foldl (\(cs, acc) x ->
                                                            case getCCost x of
                                                              CostInt{} -> (cs, acc ++ [getCCost x])
                                                              _ -> (tail cs, acc ++ [head cs])
                                                           ) (csts, [])
                                                     (getCConstructors . ctrType $ dt))


constr _    = []


weak :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
weak (t, v, Context pre cost cond post his) =
    if null pre  || (snd . head) his == "weak" then []
    else

        [(t, v, [Context newPre
                 cost cond post (((fst.head $ his) + 1, "weak") : his)])

         | length pre > length newPre
        ] ++ constr emptyPreCtx ++ function emptyPreCtx


    where

      newPre = (filter (\x -> case find (== getIVVariable x)
                                        (map getIVVariable atoms) of
                                          Nothing -> False
                                          _ -> True) pre)

      emptyPreCtx = (t, v, Context [] cost cond post
                   (((fst.head $ his) + 1, "weak empty") : his))


      atoms = case post of
                CtxFunction _ _ ch _ -> concatMap varsPost ch
                CtxAtom x -> [x] -- empty, no more iteration possible
                                -- (identity rule will be called in
                                -- same iteration).

      -- Gets the variables from the statement on the right hand side.
      varsPost                        :: CtxStatement -> [InternalVariable]
      varsPost (CtxFunction n d [] c) = [InternalVariable n d c]
      varsPost (CtxFunction _ _ ch _) = concatMap varsPost ch
      varsPost (CtxAtom x)            = [x]


share :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
share (t, v, Context pre cost cond post his) =
    if null vars || elem Nothing (map (\(_, b) -> find (== b) pre ) vars)
    then [] else
        [(t, newV, [Context newPre cost (newCond newCondGroups cond)
                            (foldl' newPost post newVars)
                            ((0, "share") : his)]
         )]

        where
          -- Vars holds all the variables which need to be split up.
          vars :: [([Int], InternalVariable)]
          vars = varsToShare post

          -- This function gets the right amount of variables for the
          -- variables to share.
          (v0, varStrs) = getNewVariableName v (length vars)

          -- This function gets the right amount of variables for
          -- the new cost variables.
          --
          -- cvarStrs: list of list of variable names
          (newV, cvarStrs) = foldl fun (v0, []) (map snd vars)
              where
                fun              :: (Int, [[String]]) -> InternalVariable -> (Int, [[String]])
                fun (v, strs) iv = (\(a,b) -> (a, strs++[b])) $
                                   getNewVariableName v ((length . getIVCosts .
                                                          oldPre . getIVVariable) iv)

          -- This function gets the internal variable from the old
          -- pre conditions using the name of the variable.
          oldPre     :: String -> InternalVariable
          oldPre str = fromMaybe
                     (throw $ FatalException $
                                "Could not find element: " ++ str
                     )
                     (find ((str ==) . getIVVariable) pre)


          -- Location information as a list of integers, variable
          -- to share, new name for variable
          newVars :: [([Int], InternalVariable, InternalVariable)]
          newVars =
              zipWith (\(a, b) (c, d) -> (a, b, InternalVariable c (getDatatypeF b pre)
                                    (map CostVar d))) vars (zip varStrs cvarStrs)


          getDatatypeF     :: InternalVariable -> [InternalVariable] -> Datatype
          getDatatypeF k n = getIVDatatype $ fromMaybe
                            (throw $ FatalException $ "Could not find element " ++
                                   show k ++ " in " ++ show n  ++ ". (share)")
                            (find (== k) n )

          newPre = (pre \\ map snd vars) ++ map (\(_,_,c) -> c) newVars
          newPost                                       :: CtxStatement ->
                                   ([Int], InternalVariable, InternalVariable) ->
                                                           CtxStatement
          newPost (CtxAtom _) (_, _, var)               = CtxAtom var
          newPost (CtxFunction n d ch s)  (pos, u, var) =
              CtxFunction n d chd s
                  where
                    chd = (\(a, b) -> a ++
                            [newPost (ch !! head pos) (tail pos, u, var)]
                            ++ tail b) (splitAt (head pos) ch)

          -- This function returns the list of new variables grouped by the
          -- type. It first needs to sort the list, to ensure proper
          -- functionality of groupBy!
          newCondGroups :: [[([Int], InternalVariable, InternalVariable)]]
          newCondGroups = groupBy cmpFun (sortBy sortFun newVars)

                  where
                    cmpFun (_,a,_) (_,b,_) = getIVVariable a == getIVVariable b
                    sortFun (_,a,_) (_,b,_) = compare (getIVVariable a) (getIVVariable b)

          newCond         :: [[([Int], InternalVariable, InternalVariable)]] ->
                             Conditions -> Conditions
          newCond [] c    = c
          newCond (l:r) c = newCond r (c { getCostConstraints = getCostConstraints c ++ nCost })
                  where
                    -- This function calucates the new cost
                    -- constraints by setting equal the parts of the
                    -- costr constraints from the old signature to the
                    -- new one.
                    nCost = map (\(x, y) -> CEq [CVar 1 . showCost $ x]
                                            (map (CVar 1 . showCost) y)) $
                            (map (\a -> ((getIVCosts oldPre) !! a,
                                         map (!! a) list)))
                            [0..length (getIVCosts oldPre)-1]

                    list :: [[Cost]]
                    list = map (\(_, _, a) -> getIVCosts a) l

                    oldPre = fromMaybe
                             (throw $ FatalException $
                                        "Could not find element: " ++
                                        show ((\ (_, a, _) -> a) $ head l)
                             )
                             (find (== ((\ (_, a, _) -> a) $ head l)) pre)


composition :: (TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
composition (t, v, Context pre (p, q) cond (CtxFunction f d ch c) his) =
    case constr (t, v, Context pre (p, q) cond (CtxFunction f d ch c) his) ++
         function (t, v, Context pre (p, q) cond (CtxFunction f d ch c) his) of
      [] -> -- if constructor or function rule works, composition cannot be done!
            -- otherwise the prove could endlessly do compositions.
          case getRewriteRuleByName t f of
            Just rr -> composition' (rr, t, v, Context pre (p, q) cond
                                           (CtxFunction f d ch c) his)
            Nothing ->
                case getRRfromCtr t f d of
                  [] -> []
                  rr1 -> if null (getParTypes $ getSignature $ head rr1)
                         then []
                         else composition'
                                   (head rr1, t, v,
                                         Context pre (p, q) cond (CtxFunction f d ch c) his)
      _ -> []

    where

      getRRfromCtr :: TypedTRS -> String -> Datatype -> [RewriteRule]
      getRRfromCtr t n d = concatMap (getRRFromCtrType t n) (getConstructorsType t d)

      getRRFromCtrType          :: TypedTRS -> String -> ConstructorType -> [RewriteRule]
      getRRFromCtrType trs n ct =
          concatMap (\c -> case signature c of
                             Nothing -> []
                             Just sig -> [RewriteRule n sig []]) (getCConstructors ct)
              where
                -- This function creates a signature out of a contructor
                signature :: (Monad m) =>  Constructor -> m Signature
                signature c =
                    case getInputVar n (getCConstructors ct) of
                      Nothing -> fail "getInputVar failed"
                      Just inp -> return $ Signature (setDatatypes inp)
                                  (InternalVariable "" (getCDatatype ct)
                                                        (map getCCost (getCConstructors ct)))
                                  CostEmpty CostEmpty

                setDatatypes        :: [InternalVariable] -> [InternalVariable]
                setDatatypes []     = []
                setDatatypes (v:vs) =
                      if getIVVariable v == "X"
                      -- `elem` map getIVVariable (getCConstructorParams ct)
                      then v { getIVVariable = "", getIVDatatype = d
                             , getIVCosts = map getCCost (getCConstructors ct) }
                               : setDatatypes vs
                      else v { getIVVariable = ""
                             , getIVCosts = -- get the constructors costs
                                 map getCCost (getCConstructors $
                                               fromMaybe
                                               (throw $ FatalException
                                                "Sorry..programming error!")
                                               (getConstructorsType trs
                                                (getIVDatatype v)))
                             }
                               : setDatatypes vs


                -- This function iterates over a list of
                -- constructors and tries to find the right one
                -- comparing the string to the constructor name (it's function name)
                getInputVar :: String -> [Constructor] -> Maybe [InternalVariable]
                getInputVar n []     = fail $ "Input variable " ++ n ++ " not declared."
                getInputVar n (c:cs) = if n == getFunName (getCStatement c)
                                       then return $ concatMap getChldr
                                                (getChilds (getCStatement c))
                                       else getInputVar n cs

                -- This function takes as input a statement and
                -- returns a list of all it's atoms which are not
                -- functions.
                getChldr                 :: Statement -> [InternalVariable]
                getChldr (Function _ ch) = concatMap getChldr ch
                getChldr (Atom x)        = [x]

composition _ = []

composition' :: (RewriteRule, TypedTRS, Int, Context) -> [(TypedTRS, Int, [Context])]
composition' (rr, t, v, Context pre (p, q) cond (CtxFunction f d ch c) his) =
    if null (getParTypes $ getSignature rr)
    then []
    else [(t, newV, newCtx)
              | (length . getParTypes . getSignature) rr <= length ch &&
                (getIVDatatype . getRetTypes . getSignature) rr == d &&
                all (\x -> case find (== x) (map getIVVariable
                                             (concatMap getAtoms ch))  of
                             Nothing -> False
                             _ -> True) (map getIVVariable pre)
         ]

    where

      isRR = isRewriteRule f t

      ctrRec = case findCtrByName f t of
                 Nothing -> repeat False
                 Just ctr ->
                     case getCStatement ctr of
                       Atom x -> repeat False
                       Function _ ch -> map (\(Atom x)-> getIVVariable x == "X") ch

      -- variables for new costs (p and q), new variables and new variable costs
      (newV0, varStrs0) = getNewVariableName v (length . getParTypes . getSignature $ rr)
      (newV1, varStrs1) = getNewVariableName newV0 (length . getParTypes . getSignature $ rr)
      (newV, varStrs2) = foldl fun (newV1, []) $ zip ctrRec (getParTypes . getSignature $ rr)
          where
            fun                   :: (Int, [[String]]) -> (Bool, InternalVariable) -> (Int, [[String]])
            fun (v, strs) (r, iv) =
                if isRR || not r -- if it is a rewrite rule, or the constructor
                                 -- child is not recursive
                then (\(a,b) -> (a, strs++[b])) $
                               getNewVariableName v (length . getIVCosts $ iv)
                else (v, strs ++ [map showCost c]) -- use same costs

      getAtoms                 :: CtxStatement -> [InternalVariable]
      getAtoms (CtxAtom x)        = [x]
      getAtoms (CtxFunction _ _ chld _) = concatMap getAtoms chld

      -- First call context parameter, to create parameter contexts, then create
      -- the function context.
      newCtx :: [Context]
      newCtx = ctxFunction (ctxParameter (CtxFunction f d ch c)
                            ((p : map CostVar varStrs0)  ++ [q])
                            (map getIVDatatype $ getParTypes . getSignature $ rr)
                            varStrs2
                           )

      ctxFunction         :: [Context] -> [Context]
      ctxFunction parCtxs = foldl fun (Context [] (CostVar $ last varStrs0, q)
                                       cond { getCostConstraints = getCostConstraints cond ++
                                              [CGeq [CVar 1 $ last varStrs0]
                                                    [CVar 1 $ showCost q]
                                              ]
                                            }
                                       (CtxFunction f d [] c)
                                       (((fst.head $ his) + 1, "composition end") : his))
                                       (zip parCtxs varStrs1)
                            : parCtxs
                where
                  fun :: Context -> (Context, String) -> Context
                  fun (Context apre acst acond (CtxFunction af ad ach ac) ahis)
                      (Context _ _ _ (CtxFunction _ cd _ cc) _, varName) =
                          Context (apre ++ [InternalVariable varName cd cc])
                          acst acond (CtxFunction af ad
                                      (ach ++ [CtxAtom $ InternalVariable varName cd cc])
                                      ac) ahis

                  fun (Context apre acst acond (CtxFunction af ad ach ac) ahis)
                      (Context _ _ _ (CtxAtom x) _, varName) =
                          Context (apre ++ [InternalVariable
                                            varName (getIVDatatype x) (getIVCosts x)])
                          acst acond (CtxFunction af ad
                                      (ach ++ [CtxAtom x { getIVVariable = varName }]) ac
                                     ) ahis

      ctxParameter :: CtxStatement -> [Cost] -> [Datatype] -> [[String]] -> [Context]
      ctxParameter (CtxFunction _ _ [] _)        _   _  _ = []
      ctxParameter (CtxFunction n dtype (c:cs) cost) vars (d:ds) (nc:ncs) =

          case c of
            CtxAtom x ->
                Context (newPre pre c) (head vars, vars !! 1) newCond
                 (CtxAtom (x { getIVDatatype = d, getIVCosts = map CostVar nc
                                               -- map (\(a, b) -> case a of
                                               --                   CostInt {} -> a
                                               --                   _ -> CostVar b) $
                                               --                        zip cost nc
                             }))
                 (((fst.head $ his) + 1, "composition") : his)
                 : ctxParameter (CtxFunction n dtype cs cost) (tail vars) ds ncs
            CtxFunction {} ->
                Context (newPre pre c) (head vars, vars !! 1) newCond
                      (c { getCtxFDatatype = d, getCtxFCost = map CostVar nc
                                             -- map (\(a, b) -> case a of
                                             --                     CostInt {} -> a
                                             --                     _ -> CostVar b) $
                                             --                        zip cost nc
                         })
                      (((fst.head $ his) + 1, "composition") : his)
                      : ctxParameter (CtxFunction n dtype cs cost) (tail vars) ds ncs

          where
                  newPre :: [InternalVariable] -> CtxStatement -> [InternalVariable]
                  newPre oldPre (CtxFunction _ _ chd _) = concatMap (newPre oldPre) chd
                  newPre oldPre (CtxAtom x)             =
                      case find (== x) oldPre of
                        Nothing -> []
                        Just x -> [x]

                  newCond = Conditions [] newTypeC newCostC

                  newTypeC = case typeFromPreConds of
                               Nothing -> []
                               Just x -> [(d, x)]

                  typeFromPreConds :: Maybe Datatype
                  typeFromPreConds = case c of
                         CtxAtom x -> find (\y-> getIVVariable y == getIVVariable x) pre
                                      >>= (\x -> return $ getIVDatatype x)
                         CtxFunction name _ d _ ->
                              case getRewriteRuleByName t name of
                                Nothing ->
                                    case getConstructorTypeByCtrName t name of
                                      Nothing -> throw $ FatalException $
                                                 "Could not find rewrite rule" ++
                                                 " or constructor named " ++ name ++ "."
                                      Just x -> return $ getCDatatype x
                                Just x -> return $
                                          (getIVDatatype . getRetTypes . getSignature) x

                  newCostC = [ CGeq [CVar 1 $ showCost $ head vars]
                                    [CVar 1 $ showCost $ vars !! 1]
                             ]


-----------------------------------------------------------------------------
-- HELPER FUNCTIONS


-- | This function returns the number of same elements in the given list.
--   Input: A element and a list of elements.
instances :: (Eq a) => a -> [a] -> Int
instances _ [] = 0
instances x (y:ys)
    | x==y = 1 + instances x ys
    | otherwise = instances x ys


-- | This function gets the all variables which are used more than once in the given
--   context statement. Furthermore, it adds the location information, as a list of
--   integers, which indicates the branch to be taken, to get to the variable.
varsToShare      :: CtxStatement -> [([Int], InternalVariable)]
varsToShare stmt = filter (\(_, x) -> instances x variables > 1) (vars ([], stmt))
    where
      variables = map snd (vars ([], stmt))
      vars                        :: ([Int], CtxStatement) -> [([Int], InternalVariable)]
      vars (pos, CtxFunction _ _ ch _) =
          concatMap vars (zipWith (\a b -> (pos ++ [a], b)) [0..] ch)
      vars (pos, CtxAtom x)            = [(pos, x)]


-- | This function takes two Integer values as input parameters. The first one
--   is the input parameter for the old value, which needs to be kept track of
--   to ensure the variables are unique. And the second one is the number of
--   variables to generate. The return value is a tuple, containing the new
--   variable counter and the variables.
getNewVariableName :: Int -> Int -> (Int, [String])
getNewVariableName oldV num =
    (oldV + num, strs start num)
    where
      start = oldV
      strs      :: Int -> Int -> [String]
      strs nr n = foldl (\acc x -> acc ++ ["ipvar" ++ show x]) [] [nr..nr+n-1]


-- | This function searches the rewrite rule in the given TRS by its name.
--   It returns the rewrite rule packed in the monad, or fails.
getRewriteRuleByName :: (Monad m) => TypedTRS -> String -> m RewriteRule
getRewriteRuleByName t n = getRR n (getRewriteRules t)
    where
      getRR      :: (Monad m) => String -> [RewriteRule] -> m RewriteRule
      getRR f [] = fail $ "Rewrite rule '" ++ show f ++ "' could not be found."
      getRR f (r:rr) = if f == getName r then return r else getRR f rr

-- | This functions gets the constructorType by the given datatype.
getConstructorsType     :: (Monad m) => TypedTRS -> Datatype -> m ConstructorType
getConstructorsType trs = getConstrType (getConstructors trs)
    where
      getConstrType          :: (Monad m) => [ConstructorType] -> Datatype -> m ConstructorType
      getConstrType [] n     = fail $ "Constructor " ++ show n ++ " not found."
      getConstrType (c:cs) n = if n == getCDatatype c
                               then return c
                               else getConstrType cs n


getConstructorTypeByCtrName       :: (Monad m) => TypedTRS -> String -> m ConstructorType
getConstructorTypeByCtrName trs n = getConstrType n (getConstructors trs)
    where
      getConstrType           :: (Monad m) => String -> [ConstructorType] -> m ConstructorType
      getConstrType _ []      = fail "Constructor was not found."
      getConstrType n1 (c:cs) =
          case getConstructorByName n1 (getCConstructors c) of
            [] -> getConstrType n1 cs
            _ -> return c

      getConstructorByName           :: String -> [Constructor] -> [Constructor]
      getConstructorByName []    _   = []
      getConstructorByName _ []      = []
      getConstructorByName n0 (c:cs) = if n0 == getFunName (getCStatement c)
                                 then [c]
                                 else getConstructorByName n0 cs


-- | This function returns a constructor by its name and type.
getConstructor :: (Monad m) => TypedTRS -> String -> Datatype -> m Constructor
getConstructor trs name dtype = getCtr name dtype (getConstructors trs)
    where
      getCtr :: (Monad m) => String -> Datatype -> [ConstructorType] -> m Constructor
      getCtr n _ []     = fail $ "Constructor " ++ n ++ " was not found."
      getCtr n d (c:cs) = if d == getCDatatype c
                          then
                              case findCtr n (getCConstructors c) of
                                Just x -> return x
                                Nothing -> getCtr n d cs
                            else getCtr n d cs

      findCtr :: String -> [Constructor] -> Maybe Constructor
      findCtr n [] = fail $ "Constructor " ++ n ++ " was not found."
      findCtr n (Constructor (Function n1 ch1) c:cs) =
                     if n == n1
                     then return $ Constructor (Function n1 ch1) c
                     else findCtr n cs

--
-- InferenceRuleImpl.hs ends here
